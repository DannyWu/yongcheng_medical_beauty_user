// pages/user-my-follow/user-my-follow.js
var app = getApp()
Page({
  data: {
    currentTab: 0,
    pageNum: 1, //页数
    pageSize: 10, //每页数量
    follow: "", //关注数据
    imgUrl: "https://tyc-bj.cn/yc/api/attach/",
  },
  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数

    this.follow(); //关注列表
    this.fans();
  },
  //关注列表
  follow: function() {
    var that = this;
    var userid = wx.getStorageSync('userid');
    wx.request({
      url: app.globalData.baseUrl + 'doctor/followApi/getFollowList',
      data: {
        userId: userid, //用户id
        pageNum: that.data.pageNum, //页数
        pageSize: that.data.pageSize, //每页数量
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log("关注列表", res.data)
        that.setData({
          follow: res.data.data
        })
      }
    })
  },
  //粉丝列表
  fans: function() {
    var that = this;
    var userid = wx.getStorageSync('userid');
    wx.request({
      url: app.globalData.baseUrl + 'doctor/followApi/getFansList',
      data: {
        userId: userid, //用户id
        pageNum: that.data.pageNum, //页数
        pageSize: that.data.pageSize, //每页数量
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log("粉丝列表", res.data)
        that.setData({
          fans: res.data.data
        })
      }
    })
  }, 

  //删除关注
  delfollow: function (e) {
    console.log("删除ID", e)
    var that = this;
    var followId = e.currentTarget.dataset.id;
    console.log("删除ID1", followId)
    
    wx.request({
      url: app.globalData.baseUrl + 'doctor/followApi/deleteFollow',
                                   
      data: {
        followId: followId,//关注ID

      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log("删除关注", res.data)
        that.onLoad();
      }
    })
  },

  //滑动切换
  swiperTab: function(e) {
    var that = this;
    that.setData({
      currentTab: e.detail.current
    });
  },
  //点击切换
  clickTab: function(e) {
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
    }

    var type = e.target.dataset.current;
    if (type == 0) {
      that.follow(); //关注列表
    } else {
      that.fans(); //粉丝列表
    }
  },
  
})