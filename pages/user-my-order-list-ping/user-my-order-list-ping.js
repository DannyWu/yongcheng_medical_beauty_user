// pages/user-my-order-details/user-my-order-details.js
var app = getApp();
Page({

  /**总价格
   * 页面的初始数据 
   */
  data: {
    orderid: "",//订单ID
    operation: "",//术前后详情数据//医美的详情数据
    // orderdetails:"",
    imgUrl: "https://tyc-bj.cn/yc/api/attach/",//拼接图片路径
    orderType: "",//订单类型2医美1术前术后
    options: "",
  },
 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("用订单id", options)
    var orderid = options.id;//订单ID
    var orderType = options.orderType;//订单类型2医美1术前术后
    this.setData({
      orderid: orderid,
      orderType: orderType,
      options: options,
    })
    //获取订单详情判断
    if (orderType == 1) {
      this.operation();// 获取用户订单详情  术前术后的
    } else {
      this.orderdetails();// 获取用户订单详情 医美的
    }
  },

  // 获取用户订单详情医美的
  orderdetails: function () {
    var that = this;
    // 获取用户订单详情
    wx.request({
      url: app.globalData.baseUrl + 'shopp/yimeiGoodsOrderApi/getOrderDetail',
      data: {
        orderid: that.data.orderid,//订单ID
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {

        console.log("医美的订单详情", res.data.data)
        that.setData({
          operation: res.data.data
        })
      }
    })
  },

  // 获取用户订单详情医术前术后的
  operation: function () {
    var that = this;
    console.log("123qweqwe", that.data.orderid)
    // 获取用户订单详情
    wx.request({
      url: app.globalData.baseUrl + '/shopp/goodsOrderApi/getOrderDetail/' + that.data.orderid,
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        // 'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
        'content-type': 'application/json'
      },
      success(res) {
        console.log("术前术后订单详情", res.data.data)
        var operation = res.data.data
        var znum = 0;
        for (var i = 0; i < res.data.data.goodsArray.length; i++) {
          znum = znum + operation.goodsArray[i].number;
          // console.log('znum', znum);
        }
        operation.znum = znum;
        // console.log('aaaaaaaaaaaaaaaaaaaa',operation);
        that.setData({
          operation: operation
        })
      }
    })
  },

  //跳转到我的（个人中心）
  jop:function(){
    wx.switchTab({
      url: '../user-my/user-my',
    })
  },
  
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})