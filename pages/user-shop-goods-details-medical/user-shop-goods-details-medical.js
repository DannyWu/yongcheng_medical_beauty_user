var app = getApp();
var WxParse = require('../../wxParse/wxParse.js');
Page({
  data: {
    imgUrls: [
      'https://tyc-bj.cn/yc/api/attach/images/shop-goods-details-1.jpg',
      'https://tyc-bj.cn/yc/api/attach/images/shop-goods-details-1.jpg',
      'https://tyc-bj.cn/yc/api/attach/images/shop-goods-details-1.jpg'
    ],
    //是否采用衔接滑动  
    circular: true,
    //是否显示画板指示点  
    indicatorDots: true,
    //选中点的颜色  
    indicatorcolor: "#000",
    //是否竖直  
    vertical: false,
    //是否自动切换  
    autoplay: true,
    //自动切换的间隔
    interval: 2500,
    //滑动动画时长毫秒
    duration: 100,
    //所有图片的高度  （必须）
    imgheights: [],
    //图片宽度 
    imgwidth: 750,
    //默认  （必须）
    current: 0,
    currentTab: 0,
    info: [],
    imgUrl: 'https://tyc-bj.cn/yc/api/attach/',
    goodsid: 1,
    goodsmoney: 0, // 预约金
    storemoney: 0, // 到点再付
  },
  imageLoad: function(e) {
    //获取图片真实宽度
    var imgwidth = e.detail.width,
      imgheight = e.detail.height,
      //宽高比
      ratio = imgwidth / imgheight;
    // console.log(imgwidth, imgheight)
    //计算的高度值
    var viewHeight = 750 / ratio;
    var imgheight = viewHeight
    var imgheights = this.data.imgheights
    //把每一张图片的高度记录到数组里
    imgheights.push(imgheight)
    this.setData({
      imgheights: imgheights,
    })
  },
  bindchange: function(e) {
    // console.log(e.detail.current)
    this.setData({
      current: e.detail.current
    })
  },
  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数
    this.loadData(options);
  },
  //滑动切换
  swiperTab: function(e) {
    var that = this;
    that.setData({
      currentTab: e.detail.current
    });
  },

  // 加载数据
  loadData: function(options) {
    
    var that = this;
    var url = '';
    console.log('商品参数:', options)
    this.setData({
      goodsid: options.id
    })
    if (options.etype == 1) {
      url = app.globalData.baseUrl + 'shopp/yimeiGoodsApi/getDetail/';
      that.setData({
        'etype': 1,
      })
    } else {
      url = app.globalData.baseUrl + 'shopp/goodsApi/goodsDetail/';
      that.setData({
        'etype': 2,
      })

    }
    console.log('url:', url+options.id);
    wx.request({
      url: url + options.id,
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/json'
      },
      success(res) {
        console.log('详情', res);
        that.setData({
          info: res.data.data,
          goodsId: options.id,
          goodsmoney: res.data.data.goodsDetail.reservations,
          storemoney: res.data.data.goodsDetail.storePayment,
        })
      }
    })
  },

  //点击切换
  clickTab: function(e) {
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
      if (e.target.dataset.current == 1) {
        that.loadDetail();
      }
    }
  },

  loadDetail: function(e) {
    var goodsId = this.data.goodsId;
    var that = this;
    var etype = this.data.etype;
    var url = '';
    if (etype == 1) {
      url = app.globalData.baseUrl + 'shopp/yimeiGoodsApi/getGoodsDetailsHtml/' + goodsId;
    } else {
      url = app.globalData.baseUrl + 'shopp/goodsApi/getGoodsDetailsHtml/' + goodsId;
    }
    console.log('详情url:', url);
    wx.request({
      url: url,
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/json'
      },
      success(res) {
        console.log('详情html:', res);
        var detail = res.data;

        that.setData({
          goodsDetailsHtml: res.data,
        })

        WxParse.wxParse('detail', 'html', detail, that, 5);
      }
    })
  },

  //去订单
  gobuy: function(e) {
    console.log(e);
    var goodsmoney = e.currentTarget.dataset.money;
    var goodsid = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../user-shop-goods-order-1/user-shop-goods-order-1?money=' + goodsmoney,
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },

  //去质询
  goconsult: function() {
    var isLogin = wx.getStorageSync('isLogin');
    if (isLogin != 2) {
      wx.navigateTo({
        url: '../user-my-kf/user-my-kf',
      })
    } else if (isLogin == 1) {
      wx.redirectTo({
        url: '/pages/user-login/user-login',
      });
    }

  },

  // 关注按钮
  bindFollow: function(e) {
    var userid = e.currentTarget.dataset.userid;
    console.log('关注:userid', userid);
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/member/attention',
      data: {
        userId: userid,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('关注结果:', res);
        var url = '';
        if (that.data.etype == 1) {
          url = app.globalData.baseUrl + 'shopp/yimeiGoodsApi/getDetail/';
        } else {
          url = app.globalData.baseUrl + 'shopp/goodsApi/goodsDetail/';
        }
        console.log('url:', url);
        wx.request({
          url: url + that.data.goodsid,
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/json'
          },
          success(res) {
            console.log('详情:', res);
            console.log('预约金:',res.data.data.goodsDetail.reservations);
            that.setData({
              info: res.data.data,
              goodsId: that.data.goodsid,
              goodsmoney: res.data.data.goodsDetail.reservations,
              storemoney: res.data.data.goodsDetail.storePayment,
            })
          }
        })

      }
    })
  },
})