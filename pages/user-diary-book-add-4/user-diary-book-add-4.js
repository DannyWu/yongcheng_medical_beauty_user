// pages/user-diary-book-add-4/user-diary-book-add-4.js
var app = getApp();
Page({
 
  /**
   * 页面的初始数据 
   */
  data: {
    choosed: [],// 已选标签
    choosedid: [],// 已选id
    images: [],
    images1: [],
    images2: [],
    route1: "",//上传成功的图片地址
    route2: "",//上传成功的图片地址
    route3: "",//上传成功的图片地址
    orderid: "",//传来的orderId 
    goodId: "",//医美产品ID
    orderdetails: "",//订单详情
    userinfo:"",
    name: '', //商品名字名字  
    doctor:'' ,//医生名字
    place: '',//价格
  }, 

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("传值数据", options)
    //user-diary-book-add-3传过来的值
    this.setData({
      choosed: options.choosed,// 已选标签
      choosedid: options.choosedid,// 已选id
      images: options.images,
      images1: options.images1,
      images2: options.images2,
      orderid: options.orderid,
      goodId: options.goodId,
   
      name: options.name, //商品名字名字  
      doctor: options.doctor,//医生名字
      place: options.place,//价格

    })
    console.log('orderID赋值结果:', this.data.orderid);
    var ccc = app.globalData.route1//获取数据
    // this.orderxqym();//订单详情医美
    // this.orderxqsq();//订单详情术前
    console.log('缓存传值', ccc);
    this.photo();

  },
  //订单详情医美
  // orderxqym: function () {
  //   var that = this;
  //   wx.request({
  //     url: app.globalData.baseUrl + 'shopp/yimeiGoodsOrderApi/getOrderDetail',
  //     data: {
  //       orderid: that.data.orderid,
  //     },
  //     header: {
  //       'sign': app.globalData.sign,
  //       'token': app.globalData.token,
  //       'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
  //     },
  //     method: 'POST',
  //     success(res) {
  //       console.log('医美', res)
  //       that.setData({
  //         orderdetails:res.data.data,
  //       })
        
  //     }
  //   }) 
    
  // },
  
  
  //提交
  complete:function(){
    var that = this;
    var choo = this.data.choosedid;//转成数组
    var choosedid = choo.split(",")
   
    console.log('orderID::::', that.data.orderid);
    
    wx.request({
      url: app.globalData.baseUrl + 'doctor/diaryBook/create',
      data: {
        oneType: choosedid[0],
        twoType: choosedid[1],
        threeType: choosedid[2],

        orderId: that.data.orderid,//	订单ID
        goodId: that.data.goodId, //医美产品ID

        front: app.globalData.route1[0].fileId,//正面照
        sideFour: app.globalData.route2[0].fileId,//侧面45
        side: app.globalData.route3[0].fileId,//侧面
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('提交日记本返回', res);
      
        wx.navigateTo({
          url:"../user-diary-book-add-5/user-diary-book-add-5",
          
        })

      }
    })  
   
  },
  //获取头像
  photo: function () {
    var userinfo = wx.getStorageSync('userInfo')
    this.setData({
      userinfo: userinfo
    })
    // console.log("asdas", this.data.userinfo)
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})