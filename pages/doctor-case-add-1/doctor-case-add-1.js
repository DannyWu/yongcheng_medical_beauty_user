var app = getApp();
Page({
  data: {
    currentTab: 0,
    choosed: [1, 456, 789],
    firsttype: 1, // 一级
    secondtype: 1, // 二级
    thirdtype: 1, // 三级
    firsttypelist: [], // 一级分类列表
    choosed: [], // 已选标签列表
    choosedid: [], // 已选id列表
    childtypelist: [],
    where: ''
  },

  onLoad: function (options) {

    this.setData({
      where: options.where
    })
    this.oneTypeServiceList();
  },

  // 一级服务产品类别列表
  oneTypeServiceList: function (e) {
    var that = this;
    app.request({
      url: 'shopp/goodsTypeApi/getPage',
      data: {
        'type': 2,
      },
      success: function (res) {
        var firstName = res.data[1].name;
        var firstTypeId = res.data[1].id;
        var choosed = that.data.choosed;
        var choosedid = that.data.choosedid;
        choosed[0] = firstName;
        choosedid[0] = firstTypeId;
        that.setData({
          'firsttypelist': res.data.slice(1),
          'choosed': choosed,
          'choosedid': choosedid,
          'firsttype': firstTypeId,
        });
        that.loadTwoAndThreeType(firstTypeId);
      }
    })
  },

  // 加载二三级分类
  loadTwoAndThreeType: function (pid) {
    var that = this;
    app.request({
      url: 'shopp/goodsTypeApi/getAllSonsByParent',
      data: {
        'parentid': pid,
      },
      success: function (res) {
        var choosed = that.data.choosed;
        var choosedid = that.data.choosedid;
        choosed[1] = res.data[0].name;
        choosedid[1] = res.data[0].id;
        // 目前为必定有三级分类
        choosed[2] = res.data[0].sons[0].name; // 名字赋值
        choosedid[2] = res.data[0].sons[0].id; // id存储
        that.setData({
          'childtypelist': res.data,
          'secondtype': res.data[0].id,
          'thirdtype': res.data[0].sons[0].id,
          'choosed': choosed,
          'choosedid': choosedid,
        })
      }
    })
  },

  // 点击一级分类
  clickTabOne: function (e) {
    var firsttype = e.currentTarget.dataset.firsttype;
    var firsttypename = e.currentTarget.dataset.firsttypename;
    if (firsttype != this.data.firsttype) {
      var choosed = this.data.choosed;
      var choosedid = this.data.choosedid;
      choosed[0] = firsttypename; // 标签赋值
      choosedid[0] = firsttype; // id存储
      this.setData({
        'firsttype': firsttype,
        'choosed': choosed,
        'choosedid': choosedid,
      });
      this.loadTwoAndThreeType(firsttype);
    } else {
      return false;
    }
  },

  // 点击二级分类
  clickTabTwo: function (e) {
    var secondtype = e.currentTarget.dataset.secondtype;
    if (secondtype != this.data.secondtype) {
      var choosed = this.data.choosed;
      choosed[1] = secondtype;
      this.setData({
        'secondtype': secondtype,
        'choosed': choosed,
      });
    } else {
      return false;
    }
  },

  // 点击三级分类
  clickTabThree: function (e) {
    var secondtype = e.currentTarget.dataset.secondtype;
    var thirdtype = e.currentTarget.dataset.thirdtype;
    var secondtypename = e.currentTarget.dataset.secondtypename;
    var thirdtypename = e.currentTarget.dataset.thirdtypename;
    if (thirdtype != this.data.thirdtype) {
      var choosed = this.data.choosed;
      var choosedid = this.data.choosedid;
      choosed[1] = secondtypename;
      choosed[2] = thirdtypename; // 赋值名字
      choosedid[1] = secondtype;
      choosedid[2] = thirdtype; // 存储id
      this.setData({
        'thirdtype': thirdtype,
        'choosed': choosed,
        'choosedid': choosedid,
      });
    } else {
      return false;
    }
  },
  deleteType: function () {

  },
  // 下一页
  nextPage: function (e) {
    if (this.data.where == 'addreview') {
      wx.navigateTo({
        url: '../doctor-add-review/doctor-add-review?choosed=' + this.data.choosed + '&choosedid=' + this.data.choosedid
      })
    } else {
      wx.navigateTo({
        url: '../doctor-case-add-2/doctor-case-add-2?choosed=' + this.data.choosed + '&choosedid=' + this.data.choosedid
      })
    }
  },
})