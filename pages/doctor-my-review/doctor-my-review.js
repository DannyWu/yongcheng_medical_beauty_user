// pages/doctor-my-review/doctor-my-review.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgBase: app.globalData.imgUrl,          // 线上图片
    localImg: app.globalData.staticimgUrl,   // 本地图片
    pageNum: 1,
    pageSize: 10,
    listData: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.review();
  },
  // 评论数据
  review() {
    const { pageNum, pageSize } = this.data;
    const that = this;
    const param = {
      pageNum,
      pageSize,
      userId: wx.getStorageSync('userid')
    };
    app.request({
      url: "system/commentApi/pageList",
      data: param,
      success: function (res) {
        let listData = res.data;
        if (param.pageNum != 1) {
          listData = that.data.listData.concat(listData);
        }
        that.setData({
          listData
        })
      },
      fail: function (res) {
        // app.requestFail(res);
      }
    });
  },

  // 进入详情
  entry(e) {
    const id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/doctor-review-details/doctor-review-details?id=' + id
    });
  },

  // 删除点评
  deleteCase(e) {
    const that = this;
    const commentOnId = e.currentTarget.dataset.id;
    wx.showModal({
      title: '提示',
      content: '确定要删除该信息么？',
      success(res) {
        if (res.confirm) {
          app.request({
            url: "system/commentApi/deleteCommentOn",
            data: { commentOnId },
            success: function (res) {
              that.review();
            },
            fail: function (res) {
              // app.requestFail(res);
            }
          });
        } else if (res.cancel) {

        }
      }
    })



  },
  moreInfo() {
    let pageNum = this.data.pageNum + 1;
    this.setData({
      pageNum
    });
    this.review();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})