// pages/user-message/user-message.js
var app = getApp()
Page({
  data: {
    currentTab: 0,
    pageSize: 100,//每页数量
    pageNum: 1,//页数
    doctorid: "",//医生id
    userId: wx.getStorageSync('userid'),//互动用户id
    type: 0,
    xiaoxi: "",
    msgId: "",// 互动提醒消息id
    userIdd: "",//uonghu
    name: "",
    token: "",
    ordernews: "",//订单提醒数据
    imgUrl: 'https://tyc-bj.cn/yc/api/attach/',
  },

  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数  
    this.loadlogin();
    this.xianshi();//进页面默认显示互动消息
  },
  //滑动切换
  swiperTab: function (e) {
    var that = this;
    // that.setData({
    //   currentTab: e.detail.current,

    // });
    var a=[];
    var dataset=[]
    dataset['dataset']= e.detail;

    a['target'] = dataset;

    console.log('kan',a);
    that.clickTab(a);
  },
  //点击切换
  clickTab: function (e) {
    console.log('切换:', e);
    var that = this;

    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
    }

    var type = e.target.dataset.current;

    if (type == 0) {
      console.log("0");
      // 互动提醒
      wx.request({
        url: app.globalData.baseUrl + 'doctor/usermsg/getMsgByUser',
        data: {
          pageSize: that.data.pageSize, //每页数量
          pageNum: that.data.pageNum, //页数
          userId: that.data.userId, //互动用户id
        },
        header: {
          'sign': app.globalData.sign,
          // 'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
        },
        method: 'POST',
        success: function (t) {
          console.log("互动消息", t);
          that.setData({
            xiaoxi: t.data.data
          })

        }

      })

    } else if (type == 2) {
      console.log('订单提醒:', app.globalData.token)
      var tokened = wx.getStorageSync('token');

      //订单提醒
      wx.request({
        url: app.globalData.baseUrl + 'shopp/orderNews/getOrderNewsPage?pagenum=' + that.data.pageNum + '&pagesize=' + that.data.pageSize,
        data: {

        },
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/json'
        },
        success: function (res) {
          console.log("消息订单提醒", res);
          that.setData({
            ordernews: res.data.data.orderNews,
          })
        }
      })

    } else if (type == 3) {
      console.log("3");
      // 平台新闻
      wx.request({
        url: app.globalData.baseUrl + 'system/newsapi/getnewspage',
        data: {
          pageSize: that.data.pageSize, //每页数量
          pageNum: that.data.pageNum, //页数
        },
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
        },
        method: 'POST',
        success: function (t) {
          console.log("用平台新闻", t);
          that.setData({
            xiaoxi: t.data.data

          })
        }
      })

    } else if (type == 4) {
      //我的咨询
      wx.request({
        url: app.globalData.baseUrl + 'consult/talkapi/gettalkpage',
        data: {
          pageSize: that.data.pageSize, //每页数量
          pageNum: that.data.pageNum, //页数
          // userId: wx.getStorageSync('userid'),
          type: 1,//1用户端 2医生端
          // userId: that.data.userIdd,
          // name: that.data.name,
        },
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
        },
        method: 'POST',
        success: function (t) {
          console.log("我的咨询", t);
          that.setData({
            xiaoxi: t.data.data

          })

        }
      })
    } else {
      return
    }

  },
  //互动跳转
  jopriji: function (e) {
    console.log('---sha---', e.currentTarget.dataset)
    var bookid = e.currentTarget.dataset.bookid;
    var diaryid = e.currentTarget.dataset.diaryid;
    var msgtype = e.currentTarget.dataset.msgtype;
    if (msgtype == 3) {
      console.log(1)
      wx.navigateTo({
        url: "../user-my-follow/user-my-follow",
      })
    } else {
      console.log(2)

    }
    if (bookid == null) {
      console.log(1)
      wx.navigateTo({
        url: "../doctor-my-case-details/doctor-my-case-details?id=" + diaryid,
        // url: "../user-my-follow/user-my-follow",
      })
    } else {
      console.log(2)
      wx.navigateTo({
        url: '../user-diary-details/user-diary-details?diaryId=' + diaryid + '&bookid=' + bookid,
      })
    }

  },

  //订单跳转 1是术前术后  2是医美
  jop: function (e) {
    console.log("zhi3", e.currentTarget.dataset)
    var that = this;
    var orderType = e.currentTarget.dataset.ordertype;
    var orderId = e.currentTarget.dataset.orderid;
    console.log('orderId', orderId);
    console.log('orderType', orderType);
    if (orderType == 1) {
      wx.navigateTo({
        url: '../user-my-order-details/user-my-order-details?id=' + orderId + '&orderType=' + orderType,
      })
    } else if (orderType == 2) {
      wx.navigateTo({
        url: '../user-my-order-details-medical-1/user-my-order-details-medical-1?id=' + orderId + '&orderType=' + orderType,

      })
    } else if (orderType == 3) {
      wx.navigateTo({
        url: '../user-my-recharge-record/user-my-recharge-record',

      })
    }
  },

  //删除互动提醒
  deleteClick: function (event) {
    console.log("俩id", event)
    var that = this;
    var userId = event.currentTarget.dataset.userid
    var msgId = event.currentTarget.dataset.id
    wx.request({
      url: app.globalData.baseUrl + 'doctor/usermsg/deleteMsg',
      data: {
        userId: userId, //互动用户id
        msgId: msgId,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },
      method: 'POST',
      success: function (res) {
        console.log("删除互动消息", res);
        if (res.data.status == 0) {
          wx.showToast({
            title: "失败",
            icon: 'loading',
            duration: 1500
          })
        } else {
          wx.showToast({
            title: "成功",
            icon: 'success',
            duration: 1000
          })
          that.onLoad();
        }
      },
      fail: function () {
        wx.showToast({
          title: '服务器网络错误!',
          icon: 'loading',
          duration: 1500
        })
      }
    })
  },

  //删除订单提醒id
  deleteorder: function (e) {
    console.log("订单提醒id", e.currentTarget.dataset)
    var that = this;
    var id = e.currentTarget.dataset.id
    var url = app.globalData.baseUrl + 'shopp/orderNews/deleteOrderNews/' + id;
    console.log("sad12", url)
    wx.request({
      url: url,
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        // 'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
        'content-type': 'application/json'
      },
      method: 'GET',
      success: function (res) {
        console.log("删除订单提醒", res);
        wx.showToast({
          title: "成功",
          icon: 'success',
          duration: 1000
        })
        that.onLoad();
      },
      fail: function () {
        wx.showToast({
          title: '服务器网络错误!',
          icon: 'loading',
          duration: 1500
        })
      }
    })
  },
  // 进页面默认显示
  xianshi: function () {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/usermsg/getMsgByUser',
      data: {
        pageSize: that.data.pageSize, //每页数量
        pageNum: that.data.pageNum, //页数
        userId: that.data.userId, //互动用户id
      },
      header: {
        'sign': app.globalData.sign,
        // 'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },
      method: 'POST',
      success: function (t) {
        console.log("互动消息1", t.data);
        that.setData({
          xiaoxi: t.data.data
        })
      }
    })
  },
  loadlogin: function () {
    var isLogin = wx.getStorageSync('isLogin');
    var roleType = wx.getStorageSync('roleType');
    console.log('是否登陆isLogin', isLogin);
    if (isLogin == 2) {
      wx.redirectTo({
        url: '/pages/user-login/user-login',
      });
    }
  },

})