// pages/user-my-task/user-my-task.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // userId:"",//用户id
    // beautyId:"",//美丽币id 
    touxiang: "",
    paiming:"",
    bizi:"",
    imgUrl: 'https://tyc-bj.cn/yc/api/attach/',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  
  onLoad: function(options) {
    console.log("renwu", options.id)
    this.setData({
      id: options.id
    });
    var that = this;
    wx.request({                             
      url: app.globalData.baseUrl + 'doctor/memberTaskApi/getMemberTaskByUser', //仅为示例，并非真实的接口地址
      data: {
        userId: options.id ,
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        // 'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      success(res) {
        console.log("任务列表信息",res.data);
        var guanzhu=res.data.data.taskNew[1].rewardValue*10;
        var fenxiang=res.data.data.taskNew[2].rewardValue*10;
        that.setData({
          // info:res.data,
          // touxiang:res.data.userInfo.photo,
          jinri: res.data.data.taskNew,
          qita: res.data.data.taskOther,
          guanzhu: guanzhu,
          fenxiang: fenxiang,
          touxiang: res.data.data.userInfo.photo,
          paiming: res.data.data.currUser.rownum,
          bizi: res.data.data.currUser.beautyCoin,
        })
        
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.onLoad(this.data.id);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})