// pages/user-my-collect/user-my-collect.js
var app = getApp();
Page({

  /** 
   * 页面的初始数据
   */
  data: {
    collec: "", //收藏列表数据
    imgUrl: "https://tyc-bj.cn/yc/api/attach/",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.collection();
  },
  //收藏列表
  collection: function() {
    var id = wx.getStorageSync('userid');
    var that = this;
    //收藏
    wx.request({
      url: app.globalData.baseUrl + 'doctor/member/getMemberCollects',
      data: {
        id: id,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log("收藏", res)
        that.setData({
          collec: res.data.data
        })
      }
    })
  },

  //删除收藏
  deleteClick: function(e) {
    console.log('-asdasd-', e.currentTarget.dataset)
    var that = this;
    var goodsid = e.currentTarget.dataset.goodsid;
    var goodstype = e.currentTarget.dataset.goodstype;
    if (goodstype == 1){
      wx.request({
        url: app.globalData.baseUrl + 'doctor/member/collectGoods',
        data: {
          userId: wx.getStorageSync('userid'),
          goodsId: goodsid,
          goodsType: 1,
        },
        method: 'POST',
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
        },
        success: function (res) {
          console.log("取消收藏", res.data);
          wx.showToast({
            title: "取消收藏",
            icon: 'success',
            duration: 1000
          })
          that.collection();
        }
      })
    }else{
      wx.request({
        url: app.globalData.baseUrl + 'doctor/member/collectGoods',
        data: {
          userId: wx.getStorageSync('userid'),
          goodsId: goodsid,
          goodsType: 2,
        },
        method: 'POST',
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
        },
        success: function (res) {
          console.log("取消收藏", res.data);
          wx.showToast({
            title: "取消收藏",
            icon: 'success',
            duration: 1000
          })
          that.collection();
        }
      })
    }
    

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})