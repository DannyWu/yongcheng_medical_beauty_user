// pages/user-doctor-video/user-doctor-video.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgBase: app.globalData.imgUrl,          // 线上图片
    localImg: app.globalData.staticimgUrl,   // 本地图片
    pageNum: 1,                              // 当前页数
    pageSize: 10,                            // 每页条数
    liveList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getLiveList(options.doctorid)
  },
  // 获取直播列表
  getLiveList(doctorid) {
    const that = this;
    const param = {
      doctorId: doctorid,
      pageNum: this.data.pageNum,
      pageSize: this.data.pageSize
    };
    app.request({
      url: "train/onlineVideo/doctorLiveList",
      data: param,
      success: function (res) {
        console.log(res)
        // that.setData({
        //   liveList: data
        // })
      },
      fail: function (res) {
        // app.requestFail(res);
      }
    });
  },
})