// pages/user-my-photo/user-my-photo.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrl: "https://tyc-bj.cn/yc/api/attach/", //请求接口图片路径
    photo: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.loginphtop(); //加载图片
  },
  //加载图片
  loginphtop: function() {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/member/getMemberAlbum',
      data: {
        userId: wx.getStorageSync('userid'),
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log("相册", res.data)

        that.setData({
          photo: res.data.data,

        })
        console.log("-123123---", that.data.photo)
      }
    })
  },

  //图片放大
  imgYu: function(event) {
    console.log('assacsa', event.currentTarget.dataset)
    var src = event.currentTarget.dataset.src; //获取data-src
    var imgList = event.currentTarget.dataset.list; //获取data-list
    console.log('imgList', imgList[1].attachUrl)

    var imgUrl = app.globalData.imgUrl;
   
    var a = [];
    //获取下标
    var o = event.currentTarget.dataset.inde;
    // 循环 list数组
    for (var s in imgList) a.push(imgUrl + imgList[s].attachUrl);

    // a.push(imgUrl + imgLists);
    //图片预览
    console.log('yaokan1', imgUrl + imgList[o].attachUrl);
    console.log('yaokan2',a);
    wx.previewImage({
      current: imgUrl + imgList[o].attachUrl, // 当前显示图片的http链接
      urls: a// 需要预览的图片http链接列表
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})