// components/searchHeader/searchHeader.js
var app = getApp();
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    rightImageType: {
      type: Number,
      value: 0,
    },
    showCity: {
      type: Boolean,
      value: false,
      observer: function(newVal, oldVal, changedPath) {
        this.setData({
          isShowCity: newVal
        });
      }
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    areaName: '定位中..',
    provinceId: wx.getStorageSync('provinceid') || 1, // 省id
    provinceList: [], //省列表
    cityId: wx.getStorageSync('cityid') || 2, // 市id
    cityList: [], //城市列表
    isShowCity: false,
    currentTab: 0,
    keyword: '',
  },

  ready: function() {
    //获取省列表
    this.getProvince();
    if (wx.getStorageSync('provinceid')) {
      this.getCity();
    } else {
      this.loadAddress();
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 获取所有省列表
    getProvince: function() {
      var that = this;
      wx.request({
        url: app.globalData.baseUrl + 'doctor/city/getCityList',
        data: {
          id: '',
          level: '',
          currentLevel: '',
        },
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
        },
        method: 'POST',
        success(res) {
          that.setData({
            provinceList: res.data.data,
          })
        }
      })
    },
    //获取当前位置(定位)
    loadAddress: function() {
      var that = this;
      wx.getLocation({
        type: 'wgs84',
        success(res) {
          var latitude = res.latitude;
          var longitude = res.longitude;
          wx.setStorageSync('latitude', latitude);
          wx.setStorageSync('longitude', longitude);
          
          console.log('存储经纬度');
          var location = latitude + "," + longitude;
          wx.request({
            url: app.globalData.baseUrl + 'doctor/city/getAreaByLocation',
            data: {
              'location': location
            },
            header: {
              'sign': app.globalData.sign,
              'token': app.globalData.token,
              'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
            },
            method: 'POST',
            success(res) {
              that.setData({
                provinceId: res.data.data.proId,
                cityId: res.data.data.cityId,
              })
              wx.setStorageSync('provinceid', res.data.data.proId);
              wx.setStorageSync('cityid', res.data.data.cityId);
              that.getCity();
            }
          })
        }
      })
    },
    // 获取市列表
    getCity: function() {
      var that = this;
      let {
        provinceId
      } = this.data;
      wx.request({
        url: app.globalData.baseUrl + 'doctor/city/getCityList',
        data: {
          id: provinceId,
          level: 2,
          currentLevel: 1,
        },
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
        },
        method: 'POST',
        success(res) {
          that.setData({
            cityList: res.data.data,
          })
          let {
            cityId,
            cityList
          } = that.data;
          console.log(cityId);
          let findItem = cityList.find((item) => {
            return item.cityId == cityId;
          });
          if (findItem) {
            that.setData({
              areaName: findItem.cityName,
            });
          }
        }
      })
    },
    // 显示弹出框
    showCity: function(e) {
      var that = this;
      let isShowCity = !that.data.isShowCity
      this.setData({
        showCity: isShowCity
      });
      this.triggerEvent('changeShowCity', {
        isShowCity: isShowCity
      });
    },
    // 点击省
    bindProvince: function(e) {
      var provinceId = e.currentTarget.dataset.provinceid;
      if (provinceId != this.data.provinceId) {
        this.setData({
          provinceId: provinceId
        });
        this.getCity();
      } else {
        return false;
      }
    },
    // 点击市
    bindCity: function(e) {
      var cityId = e.currentTarget.dataset.cityid;
      var cityName = e.currentTarget.dataset.cityname;
      this.setData({
        isShowCity: false
      });
      if (cityId != this.data.cityId) {
        this.setData({
          cityId: cityId,
          areaName: cityName
        });
        wx.setStorageSync('cityid', cityId);
        wx.setStorageSync('provinceid', this.data.provinceId);
        this.triggerEvent('changeCity');
      }
    },
    //滑动切换
    swiperTab: function(e) {
      this.setData({
        currentTab: e.detail.current
      });
    },
    // 搜索框输入
    changeKeyword: function(e) {
      var keyword = e.detail.value;
    
      this.setData({
        keyword: keyword,
      })
      return e.detail.value;
    },
    confirmSearch: function(e) {
      this.triggerEvent('indexSearch', {
        keyword: e.detail.value
      });
      this.setData({
        keyword: '',
      })
    },
    rightButtonClick: function() {
      this.triggerEvent('rightClick');
    }
  }
})