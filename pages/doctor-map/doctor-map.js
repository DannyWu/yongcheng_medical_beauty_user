const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    latitude: '',
    longitude: '',
    allMarkes: []       // 医生&城市中心信息
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      latitude: options.latitude,
      longitude: options.longitude
    });
    this.getDoctorByMap();
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.mapCtx = wx.createMapContext('myMap')
  },
  //按地图搜索医生
  getDoctorByMap() {
    const t = this;
    const { longitude, latitude } = t.data;
    console.log('--多少人--', t.data.latitude)
    console.log('--多少人2--', t.data.longitude)
    
    app.request({
      url: 'doctor/doctor/getDoctorByMap',
      data: {
        longitude,
        latitude,
        distance: 1000
      }, 
      success: function (res) {
        console.log('--多少3--',res)
        const {data} = res;
        let marker = [];
        for (let i = 0; i < data.length; i++) {
          let doctorItem = data[i];
          marker.push({
            id: `doctor_${doctorItem.doctorId}`,
            iconPath: "https://tyc-bj.cn/yc/api/attach/images/doctor_icon.png",
            latitude: Number(doctorItem.latitude),
            longitude: Number(doctorItem.longitude),
            callout: {
              content: ` ${doctorItem.doctorName} | ${doctorItem.position}\n预约${doctorItem.assesNums}次`,
              color: '#ffffff',
              fontSize: 14,
              borderWidth:6,
              borderColor: '#9d46fa',
              display: 'BYCLICK',
              textAlign: 'center',
              borderRadius: 4,
              bgColor: '#9d46fa'
            },
            width: 25,
            height: 30
          });
        }
        t.setData({
          allMarkes: marker
        });
        t.getCityCenterByMap();
      }
    });
  },
  getCityCenterByMap(){
    const t = this;
    const { longitude, latitude } = t.data;
    app.request({
      url: 'doctor/city/getCityCoreByLocation',
      data: {
        longitude,
        latitude,
        distance: 1000
      },
      success: function (res) {
        const { data } = res;
        let marker = t.data.allMarkes;
        for (let i = 0; i < data.length; i++) {
          let cityItem = data[i];
          marker.push({
            id: `city_${cityItem.id}`,
            iconPath: "https://tyc-bj.cn/yc/api/attach/images/hospital_icon.png",
            latitude: Number(cityItem.latitude),
            longitude: Number(cityItem.longitude),
            callout: {
              content: `${cityItem.name}`,
              color: '#ffffff',
              fontSize: 14,
              borderWidth: 6,
              borderColor: '#9d46fa',
              display: 'BYCLICK',
              textAlign: 'center',
              borderRadius: 4,
              bgColor: '#9d46fa'
            },
            width: 25,
            height: 30
          });
        }
        t.setData({
          allMarkes: marker
        });
      }
    });
  },
  markerTap: function(e){
    
  },
  calloutTap: function(e) {
    let type = e.markerId.split('_')[0];
    let id = e.markerId.split('_')[1];
    if(type == 'doctor'){
      wx.navigateTo({
        url: `../user-doctor-details/user-doctor-details?id=${id}`
      });
    }
    else {
      wx.navigateTo({
        url: `../hospitalDetail/hospitalDetail?type=1&siteId=${id}`
      });
    }
  }
})