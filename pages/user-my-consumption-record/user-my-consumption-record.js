// pages/user-my-consumption-record/user-my-consumption-record.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

    pageSize: 10,
    pageNum: 1,
    record: [],    
    more: '0',
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    
    this.select();
    console.log('kan', this.data.record.length);
  },
  //查询消费记录
  select: function () {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'shopp/financeApi/queryPayList', 
      data: {
        'pagesize': that.data.pageSize,
        'pagenum': that.data.pageNum,
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log("消费记录", res)
        console.log("消费记录条数", res.data.data.length)
        if (res.data.data) {
          if (that.data.more == 0) {
            var array = that.data.record.concat(res.data.data);
            that.setData({
              record: array
            })
            console.log("--消费记录1", array);
          } else {
            that.setData({
              more: 0,
            })
            if (res.data.data.length) {


              var array = that.data.record.concat(res.data.data);
              that.setData({
                record: array
              })
              console.log("消费记录2", array);
            } else {
              wx.showToast({
                title: '已无更多...',
                icon: 'none',
                duration: 2000,
              });
            }
          }
        }
        // var array = that.data.record.concat(res.data.data);
        // that.setData({
        //   record: array
        // })
        wx.hideLoading();
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
    // this.select();

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  // onReachBottomss: function () {
  //   wx.showLoading({
  //     title: '加载中...',
  //   })
  //   this.setData({
  //     'pageNo': this.data.pageNo + 1,
  //   });
  //   this.select();
  // },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
    /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

    wx.showLoading({
      title: '加载中...',
    })
    this.setData({
      'more': 1,
      'pageNum': this.data.pageNum + 1,
    });
    console.log('页码:', this.data.pageNum)
    this.select();
  },

})