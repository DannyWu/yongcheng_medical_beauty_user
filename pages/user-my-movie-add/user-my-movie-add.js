// pages/user-my-movie-add/user-my-movie-add.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    beautyCoin:'',
    sheetPrice:'',
    sheetId:'',
    userId: 1,
    changeNumber: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this;
    this.setData({
      sheetId: options.sheetId,
      sheetPrice: options.sheetPrice
    });
    app.request({
      url: 'doctor/member/detail',
      success: function (res) {
        console.log("个人中心1", res);
        that.setData({
          beautyCoin: res.data[0]['beautyCoin']
        });
      },
      fail: function (res) {
        app.requestFail(res);
      }
    })
  },

  payForSheet: function() {
    const t = this;
    let { beautyCoin, sheetPrice, sheetId } = this.data;
    if (Number(beautyCoin) < Number(sheetPrice)) {
      wx.showToast({
        title: '您的美丽币不足，请充值',
        icon: 'none',
        duration: 2000
      });
      return;
    }
    else{
      app.request({
        url: 'doctor/memberchange/addSheetChange',
        data: {
          userId:wx.getStorageSync('userid'),
          sheetId
        },
        success(res) {
          wx.navigateBack();
        },
        fail: function (res) {
          app.requestFail(res);
        }
      })
    }
  }
  
})