// pages/doctor-review-details/doctor-review-details.js
const app = getApp();
Page({
  data: {
    imgBase: app.globalData.imgUrl, // 线上图片
    localImg: app.globalData.staticimgUrl, // 本地图片
    data: {},
    pageNum: 1,
    pageSize: 10,
    id: '',
    inputValue: '',           // 编写评论
    commentedCount: '',      // 点评数
    commentOn: {},           // 评论三条
    commentData: [],          // 全部评论
    commentid: '',            // 所评论用户id
    beforeArray: [],
    afterArray: [],
    otherArray: [],
    str: ''                   // 回复前缀               
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      id: options.id
    })
    this.getDetail();
    this.getCommentData();
  },
  getDetail() {
    const that = this;
    app.request({
      url: "system/commentApi/getDetail",
      data: {
        commentOnId: that.data.id
      },
      success: function (res) {
        that.setData({
          commentedCount: res.data.commentedCount,
          commentOn: res.data.commentOn,
          data: res.data,
          beforeArray: res.data.beforeArray,
          afterArray: res.data.afterArray,
          otherArray: res.data.otherArray
        })
      },
      fail: function (res) {
        // app.requestFail(res);
      }
    });
  },
  // 客服
  handleContact(e) {
    console.log('客服消息')
    console.log(e);
  },
  // 获取点评数据
  getCommentData() {
    const that = this;
    const { pageNum, pageSize, id } = this.data;
    const param = {
      commentOnId: id,
      pageNum,
      pageSize
    };
    app.request({
      url: "system/commentApi/commentPage",
      data: param,
      success: function (res) {
        that.setData({
          commentData: res.data
        })
      },
      fail: function (res) {
        // app.requestFail(res);
      }
    });
  },
  // 获取点评用户信息
  getInfor(e) {
    const commentid = e.currentTarget.dataset.commentid;
    const name = e.currentTarget.dataset.name;
    const str = '回复' + name + ':';
    this.setData({
      commentid,
      inputValue: '回复' + name + ':',
      str
    })
  },

  // 编写评论
  addComment(e) {
    this.setData({
      inputValue: e.detail.value
    })
  },
  // 发布评论
  publish() {
    const that = this;
    const { inputValue, id, commentid, str } = this.data;
    const content = inputValue.replace(str, "");
    const param = {
      commentOnId: id,
      content,
      replyId: commentid
    };
    if (!app.validate(content, 'required')) {
      app.showModal('请填写评论');
      return;
    }
    app.request({
      url: "system/commentApi/comment",
      data: param,
      success: function (res) {
        that.setData({
          inputValue: ''
        })
        that.getCommentData();
      },
      fail: function (res) {
        // app.requestFail(res);
      }
    });
  },
  // 预览大图
  previewImage(e) {
    const { beforeArray, afterArray, otherArray, imgBase } = this.data;
    const urls = [];
    const current = e.currentTarget.dataset.src;

    for (var i = 0; i < beforeArray.length; i++) {
      urls.push(imgBase + beforeArray[i])
    }
    for (var j = 0; j < afterArray.length; j++) {
      urls.push(imgBase + afterArray[j])
    }
    if (beforeArray.length) {
      for (var k = 0; k < otherArray.length; k++) {
        urls.push(imgBase + otherArray[k])
      }
    }
    wx.previewImage({
      urls,
      current
    })
  },
  // 新加代码
  goDoctorDetails(e) {
    var doctorId = e.currentTarget.dataset.doctorid

    wx.navigateTo({
      url: '../user-doctor-details/user-doctor-details?id=' + doctorId,
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
  }
})