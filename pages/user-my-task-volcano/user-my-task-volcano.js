// pages/user-my-task-volcano/user-my-task-volcano.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info: '',       // 
    beautyId: '',   // 美丽币id
    top: [],
    left: [],
    image: [
      "https://tyc-bj.cn/yc/api/attach/images/my-task-volcano-1.png",
      "https://tyc-bj.cn/yc/api/attach/images/my-task-volcano-2.png",
      "https://tyc-bj.cn/yc/api/attach/images/my-task-volcano-3.png",
    ],
    coin: '',
    beautyprice:'',
    num: ''        //钻石图标数据
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getList();
  },
  //美丽币数值，列表获取
  getList(){
    var that = this;
    const param = {
      userId: wx.getStorageSync('userid')
    };
    app.request({
      url: 'doctor/beautyCoin/getbeautyCoinByUser',
      data: param,
      success: function (res) {
        console.log("res:",res);
        that.setData({
          info: res.data.UserAccount,
          coin: res.data.currUser.beautyCoin,
          num: res.data.beautyList
        })
        that.iconPostion();
      },
      fail: function (res) {
        // app.requestFail(res);
      }
    });
  },
  // 图标的随机布局
  iconPostion(){
    const item = this.data.num;
    let top = [];
    let left = [];
    for (let i = 0; i < item.length; i++) {
      top[i] = Math.round(Math.random() * 70);
      left[i] = Math.round(Math.random() * 75);
    }
    this.setData({
      top,
      left
    })
  },
  // 收获美丽币
  getCoin(e) {
    const that = this;
    const userId = e.currentTarget.dataset.userid;
    const beautyId = e.currentTarget.dataset.beautyid;
    const beautyprice = e.currentTarget.dataset.beautyprice;
    const param = {
      userId: userId,
      beautyId: beautyId
    };
    app.request({
      url: 'doctor/beautyCoin/updateBeauty',
      data: param,
      success: function (res) {
        console.log("getCoin",res)
        app.showModal("获取" + beautyprice +"美丽币");
        that.onShow();
      },
      fail: function (res) {
        // app.requestFail(res);
      }
    });
    that.onShow();
  },
  // 收获美丽币
  getCoins(e) {
    const that = this;
    const userId = e.currentTarget.dataset.userid;
    // const beautyId = e.currentTarget.dataset.beautyid;
    const beautyprice = e.currentTarget.dataset.beautyprice;
    const param = {
      userId: userId,
      beautyId: 1
    };
    app.request({
      url: 'doctor/beautyCoin/updateBeauty',
      data: param,
      success: function (res) {
        console.log("getCoin", res)
        app.showModal("获取" + beautyprice + "美丽币");
      },
      fail: function (res) {
        // app.requestFail(res);
      }
    });
    that.onShow();
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getList();
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    this.getList();
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    this.getList();
  },
})