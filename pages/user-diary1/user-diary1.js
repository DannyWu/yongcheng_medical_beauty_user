// pages/diary/diary.js
var app= getApp();
Page({
  
  /** 
   * 页面的初始数据
   */
  data: {
    pageSize: 10,//每页条数
    pageNum: 1,// 页数
    diaryBookId:"",//日记id
    arraydata:"",
    imgUrl: "https://tyc-bj.cn/yc/api/attach/", 
    BookId:"",
    userdata:"",
  },
  

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log("book", options)
    var that = this;
    var bookid = options.bookid
    var diaryId = options.diaryId
    // that.setData({
    //   BookId: BookId,
    //   diaryBookId:diaryBookId,
    // })

    //美丽日记列表上边内容
    wx.request({
      url: app.globalData.baseUrl + 'doctor/diaryBook/detail', //仅为示例，并非真实的接口地址
      data: {
        // bookId:"cb449041c1f446ddaecb62dc1dd60319",//日记本id
        bookId: bookid,//日记本id
        // bookId: that.data.BookId ,//日记本id
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log('日记列表上边用户数据', res.data.data)
        that.setData({
          userdata: res.data.data
        })
      }
    })
 
    //美丽日记列表
    wx.request({                            
      url: app.globalData.baseUrl + 'doctor/diary/getDiaryList', //仅为示例，并非真实的接口地址
      data: {
        // diaryBookId: "cb449041c1f446ddaecb62dc1dd60319",//日记本id
        diaryBookId: bookid,//日记本id
        // diaryBookId: that.data.diaryBookId,//日记本id
        pageSize: that.data.pageSize,//每页条数
        pageNum: that.data.pageNum,// 页数

      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log('日记列表2', res.data.data)
        that.setData({
          arraydata: res.data.data
        })
      }
    }) 
  },
  
   //点赞
  dianzan:function(){
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/diary/praise', //仅为示例，并非真实的接口地址
      data: {
        diaryBookId: that.data.diaryBookId,//日记本id
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log('点赞', res.data.data)
        // that.setData({
        //   arraydata: res.data.data
        // }),
      }
    }) 
    
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})