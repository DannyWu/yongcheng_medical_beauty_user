// pages/user-my-order-evaluate-medical/user-my-order-evaluate-medical.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    commentcontent: "", //评价内容
    goodsid: "", //商品ID
    images: [], //图片
    orderid: "", //订单ID
    fs: 5, //打分
    route1: "", //上传后的图片
    imagesurl: [],
    orderType: '',//订单类型1术前术后  2医美
    max:200,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log('-俩ID-', options)
    this.setData({
      goodsid: options.id, //商品ID
      orderid: options.orderId, //订单id
      orderType: options.orderType, //订单类型
    })
  },
  //图片放大
  imgYu: function (event) {
    console.log('assacsa', event.currentTarget.dataset)
    var src = event.currentTarget.dataset.src; //获取data-src
    var imgList = event.currentTarget.dataset.list; //获取data-list
 
    console.log('imgList2', imgList[0])

    var imgUrl = app.globalData.imgUrl;

    var a = [];
    //获取下标
    var o = event.currentTarget.dataset.inde;
    // 循环 list数组
    for (var s in imgList) a.push( imgList[s]);

    // a.push(imgUrl + imgLists);
    //图片预览
    console.log('yaokan1',  imgList[o]);
    console.log('yaokan2', a);
    wx.previewImage({
      current:  imgList[o], // 当前显示图片的http链接
      urls: a// 需要预览的图片http链接列表
    })
  },

  //打分
  choose: function(e) {
    console.log("--fen---", e);

    this.setData({ 
      fs: e.currentTarget.dataset.current,
    });


  },
  //保存内容
  save: function(e) {
    var that = this
    console.log('点击:', e);
    //内容
    var value = e.detail.value;
    // 获取输入框内容的长度
    var len = parseInt(value.length);
    //最多字数限制
    if (len > this.data.max) return;
    // 当输入框内容的长度大于最大长度限制（max)时，终止setData()的执行
    this.setData({
      currentWordNumber: len //当前字数  
    });
    this.setData({
      commentcontent: e.detail.value
    });
    console.log("内容:", that.data.commentcontent)
  },

  //选择图片待上传
  chooseImg: function(e) {
    var that = this;
    wx.chooseImage({
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: function(res) {
        // 将现有和新上传的合并
        var images = that.data.images.concat(res.tempFilePaths);
        images = images.length <= 6 ? images : images.slice(0, 6);
        that.setData({
          images: images,
        })
        that.uploadimages(); // 选择图片后 上传到服务器上
      },
    })
  },

  // 上传图片
  uploadimages: function(e) {
    var that = this;
    wx.showToast({
      icon: "loading",
      title: "正在上传"
    });
    var images = this.data.images;
    var imagesArr = [];
    for (var i = 0; i < images.length; i++) {
      console.log("--图片i--", i, images[i])
      var aa = images[i];
      wx.uploadFile({
        url: app.globalData.baseUrl + 'shopp/goodsCommentApi/uploadCommentImage',
        filePath: images[i],
        name: 'file',
        method: 'POST',
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          "Content-Type": "multipart/form-data"
        },
        success: function(res) {
          console.log('---1---', res)

          // var path = JSON.parse(res.data).data;
          var path = JSON.parse(res.data).data;

          // var str = JSON.stringify(path);
          // var arr = "[" + str + "]";
          var str = JSON.stringify(path);
          console.log('arr:', str)
          imagesArr.push(str);
          console.log('图片数组:', imagesArr)
          console.log('图片数组:', String(imagesArr));
          var newimagesArr = String(imagesArr);
          var newimagesArred = "[" + newimagesArr + "]";
          that.setData({
            route1: newimagesArred
          })
          // [{ image: "comment/2018/11/2cdf4bf19fa34f128c531bb292fe877d.jpg", thumbnail: "comment/2018/11/thumbnail_2cdf4bf19fa34f128c531bb292fe877d.jpg" }]
        }
      })
    };
  },

  // 删除图片
  deleteimg: function(e) {
    var index = e.target.dataset.index;
    var imagesurl = this.data.imagesurl;
    var images = this.data.images;
    imagesurl.splice(index, 1);
    images.splice(index, 1);
    this.setData({
      'imagesurl': imagesurl,
      'images': images,
    });
    console.log(this.data.imagesurl);
  },

  //提交评价 
  evaluate: function() {
    var that = this;
    var orderType = that.data.orderType;
    console.log('commen1', that.data.commentcontent)
    console.log('goodsid', that.data.goodsid)
    console.log('images3', that.data.route1)
    console.log('orderid4', that.data.orderid)
    console.log('score5', that.data.fs)
    wx.request({
      url: app.globalData.baseUrl + 'shopp/goodsCommentApi/addComment',
      data: {
        commentcontent: that.data.commentcontent, //评价内容
        goodsid: that.data.goodsid, //商品ID
        images: that.data.route1, //图片
        orderid: that.data.orderid, //订单ID
        score: that.data.fs //打分
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log("发表评分", res.data)

        if (that.data.orderType == 1){
          
          wx.redirectTo({
            url: '../user-my-order-list/user-my-order-list',
          })

          wx.showToast({
            title: '商品评价成功',
            icon: 'success',
            duration: 3000,
          })
        }else{
          wx.redirectTo({
            url: '../user-my-order-list/user-my-order-list',
          })
          wx.showToast({
            title: '商品评价成功',
            icon: 'success',
            duration: 3000,
          })
        }
      }
    })
  },
  /**
  * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})