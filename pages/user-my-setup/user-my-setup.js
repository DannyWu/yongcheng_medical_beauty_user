// pages/user-my-setup/user-my-setup.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    flag: !0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(e) {
    var mobile = e.mobile;
    this.setData({
      mobile: mobile
    })
  },
  //用户指南
  guide: function() {
    var id = 8;
    // this.loadings(id);
    wx.navigateTo({
      url: '../doctor-news-system/doctor-news-system?id=' + id,
    })
  },
  //法律条款和隐私政策
  policy: function() {
    var id = 1;
    // this.loadings(id);
    wx.navigateTo({
      url: '../doctor-news-system/doctor-news-system?id='+ id,
    })
  },
  //关于平台
  terrace: function() {
    var id = 2;
    // this.loadings(id);
    wx.navigateTo({
      url: '../doctor-news-system/doctor-news-system?id=' + id,
    })

  },
  //消失
  checked: function() {
    this.setData({
      flag: !0
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  signout(){
    var tokened = wx.getStorageSync('token');
    wx.request({
      url: app.globalData.baseUrl + 'doctor/member/logout',
      data: {},
      header: {
        'sign': app.globalData.sign,
        'token': tokened,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },
      method: 'POST',
      success: function (t) {
        console.log("退出成功", t);
        wx.switchTab({
          url: '/pages/index/index',
        });
      }
    })
  }
})