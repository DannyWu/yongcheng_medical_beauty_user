// pages/user-my-recharge/user-my-recharge.js
var app = getApp();
Page({
  /**
   * 页面的初始数据 
   */
  data: {
    currentTab:0,
    clist: "", //充值列表数据
    givemoney: "", //附送金额
    godmoney: "", //应冲金额
    remark: "", //充值说明
    state: "", //支付方式
    id: "", //id
  },

  //点击切换
  clickTab: function(e) {
    console.log('---id2----', e.target.dataset)
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current,
      })
    }

    that.setData({
      givemoney: e.target.dataset.givemoney, //附送金额
      godmoney: e.target.dataset.goodsmoney, //应冲金额
      state: e.target.dataset.state, //支付方式
      createuserid: e.target.dataset.createuserid, //ID
    })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getpage(); //充值列表
  },
  //充值列表
  getpage: function() {
    var that = this
    wx.request({
      url: app.globalData.baseUrl + 'shopp/rechrageApi/getPage', //仅为示例，并非真实的接口地址
      data: {

      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log("充值列表", res.data.data.list)
        that.setData({
          clist: res.data.data.list
        })
      }

    })
  },

  //生成订单
  generate: function() {
    const that = this;
    const param = {
      userId: wx.getStorageSync('userid'), // 会员id
      payType: 3,            // 支付方式
      shouldMoney: that.data.godmoney,     // 应冲金额
      orderMoney: that.data.givemoney      // 订单金额 
    };

    console.log('---leixing--', that.data.state)
    console.log('---leixing--', param)
    app.request({
      url: "shopp/rechrageApi/insertRechargeOrder",
      data: param,
      success: function (res) {
        console.log("充值订单", res.data)
        var payType = 3;         // 支付类型；1=商品订单支付；2=课程支付；3=充值支付
        var orderId = res.data.orderId;
        var money = that.data.money;
        wx.navigateTo({
          url: '../user-shop-goods-order-pay/user-shop-goods-order-pay?res=' + orderId + '&orderType=' + payType + '&totalMoney=' + that.data.godmoney,
        })
      },
      fail: function (res) {
        // app.requestFail(res);
      }
    });
  }
})