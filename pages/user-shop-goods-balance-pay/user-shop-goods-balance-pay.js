// pages/user-shop-goods-balance-pay/user-shop-goods-balance-pay.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderId: '',
    balance: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('使用余额开发票',options);
    this.setData({
      orderId: options.res,//订单id
      payType:options.payType,
      balance: options.balance,
    });
  
  },

  pay: function () {
    const t = this;
    app.request({
      url: "shopp/payment/balancePayment",
      data: {
        orderid: this.data.orderId
      },
      success: function (res) {
        console.log("res", res);
        wx.navigateTo({
          url: '../user-shop-goods-order-2/user-shop-goods-order-2',
        });
      },
      fail: function (res) {
        wx.showToast({
          title: '余额支付失败',
          icon: 'none',
          duration: 1000
        });
      }
    })
  },
  

  
})