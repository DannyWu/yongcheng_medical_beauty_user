// pages/user-my-movie-details/user-my-movie-details.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    videoUrl: 'https://tyc-bj.cn/yc/api/attach/',
    imgUrl: 'https://tyc-bj.cn/yc/api/attach/',    
    changeNumber:'',
    changeVideo:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      changeNumber: options.changeNumber
    });
    this.getSheetChangeUrl();
  },

  getSheetChangeUrl: function() {
    const t = this;
    app.request({
      url: 'doctor/memberchange/getSheetChangeUrl',
      data: {
        userId: wx.getStorageSync('userid'),
        changeNumber: t.data.changeNumber
      },
      success: function (res) {
        console.log('-----',res)
        t.setData({
          changeVideo: res.data
        });
      },
      fail: function (res) {
        app.requestFail(res);
      }
    });
  }
})