// pages/user-diary-book-add-3/user-diary-book-add-3.js
var app = getApp();
Page({ 
  /**
   * 页面的初始数据
   */
  data: {
    choosed: [], // 已选标签
    choosedid: [], // 已选id
    images: [],//显示图片
    images1: [],//显示图片
    images2: [],//显示图片
    route1: "",//上传成功的图片地址
    route2: "",//上传成功的图片地址
    route3: "",//上传成功的图片地址
    pagenum: 1,// 页码
    pagesize: 10, //行数
    book: "",// 新建日记本中获取订单数据
    imgUrl: "https://tyc-bj.cn/yc/api/attach/",//图片拼接路径
  },
  
  /** 
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('---add2传参数---', options)
    var that = this;
    var options = options;
    //user-diary-book-add-2传过来的值
    that.setData({
      choosed: options.choosed,
      choosedid: options.choosedid,
    })
    
    

    if (that.userorder == null){
      wx.showToast({
        title: '没有订单',
        
        duration: 2000
      })
    }else{

      that.userorder();//新建日记本中获取订单
    }
  },
  
  

  //新建日记本中获取订单
  userorder:function(){
    var that = this;
    var choo = that.data.choosedid; //转成数组
    var choosedid = choo.split(",")
    console.log('---onetypeid---', choosedid[0]);
    console.log('---twotypeid---', choosedid[1]);
    console.log('---threetypeid---', choosedid[2]);
    //获取订单
    wx.request({
      url: app.globalData.baseUrl + 'shopp/yimeiGoodsOrderApi/getCompleteOrder', //仅为示例，并非真实的接口地址
      data: {
        pagenum: that.data.pagenum,// 页码
        pagesize: that.data.pagesize, //行数
        onetypeid: choosedid[0],
        twotypeid: choosedid[1],
        threetypeid: choosedid[2],
        
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },

      success(res) {
        console.log("日记本获取订单", res)
        // if(res.data.data == ''){
        //   wx.showToast({
        //     title: "没有订单跳转到商城",
        //     icon: 'success',
        //     duration: 2000
        //   })
        //   wx.navigateTo({
        //     url: '../user-shop/user-shop',
        //   })
        // }else{
          that.setData({
            book: res.data.data,
          })
        // }
        
        
      }
    })  
  },

  // 返回上一级
  nextPage: function (e) {
    // var choosed = JSON.stringify(this.data.choosed);
    console.log(e);
    // var choosedid = this.data.choosedid;
    var orderId = e.currentTarget.dataset.orderid;
    var goodsId = e.currentTarget.dataset.goodsid;
    var doctor = e.currentTarget.dataset.doctor;
    var name = e.currentTarget.dataset.name;
    var place = e.currentTarget.dataset.place;    
    console.log('orderId:', orderId);
    console.log('goodsId:', goodsId);
    console.log('doctor:', doctor);
    console.log('name:', name);
    console.log('place:', place);    
    var pages = getCurrentPages();
    var currPage = pages[pages.length - 1]; //当前页面
    var prevPage = pages[pages.length - 2]; //上一个页面
    //直接调用上一个页面的setData()方法，把数据存到上一个页面中去
    prevPage.setData({
      orderId: orderId,
      goodsId: goodsId,
      doctor: doctor,
      name: name,
      place: place,      
    })
    wx.navigateBack({
      delta: 1,
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})