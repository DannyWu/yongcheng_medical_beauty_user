// pages/diary-details/diary-details.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // diaryId:"",//日记id
    diaryId: 1, //日记id
    pageSize: 10,
    pageNum: 1,
    // content: '', //内容
    // replyId: '', // 评论指定id
    arraydata: "", //下面赋的数据
    userdata: "", //下面赋的数据
    bookid: "", //日记本id
    imgUrl: "https://tyc-bj.cn/yc/api/attach/",
    pinglun: "", //评论数据
    time: '', //转的时间错
    kong: '', //发布后清空
    isshow: 0, //默认点赞
    options: '', //默认
    usid: wx.getStorageSync('userid'),

    isreply: false, // 是否为回复 false.直接发布 true.回复
    replyContent: '', // 回复内容
    replyPeople: '', // 回复的人(拼接后的) 格式:回复 ***:
    replyid: '', // 评论指定id
    replyname: '', // 回复的人的昵称
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    console.log("q1q11w1w", options)
    that.setData({
      bookid: options.bookid,
      diaryId: options.diaryId,
      options: options,
    })

    //美丽日记列表上边内容
    wx.request({
      url: app.globalData.baseUrl + 'doctor/diaryBook/detail', //仅为示例，并非真实的接口地址
      data: {
        // bookId: that.data.bookId ,//日记本id
        bookId: that.data.bookid, //日记本id
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log('日记列表上边用户数据', res.data.data)
        that.setData({
          userdata: res.data.data,
          time: app.timeData(res.data.data.diaryBook.addTime),
        })
      }
    })


    this.details(); //日记详情
    this.obtain(); //获取日记评论
    this.browse(); //+浏览量
  },
  //美丽日记详情 
  details: function() {
    var that = this;
    //美丽日记详情
    wx.request({
      url: app.globalData.baseUrl + 'doctor/diary/detail', //仅为示例，并非真实的接口地址
      data: {
        diaryId: that.data.diaryId
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log('日记详情', res.data)
        that.setData({
          arraydata: res.data.data
        })
      }
    })
  },
  //获取日记评论
  obtain: function() {
    //获取日记评论
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/diary/commentPage', //仅为示例，并非真实的接口地址
      data: {
        diaryId: that.data.diaryId, //日记id
        pageSize: that.data.pageSize, //每页条数
        pageNum: that.data.pageNum, //页数
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,

        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log('获取日记评论123', res.data);
        that.setData({
          pinglun: res.data.data
        })
      }
    })
  },
  //浏览量增加
  browse: function() {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/diary/addDiaryViews',
      data: {
        diaryId: that.data.diaryId,
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log('增加浏览数', res.data)
      }
    })
  },


  // 处理点赞按钮
  handlePoint: function(e) {
    var that = this;
    var isLogin = wx.getStorageSync('isLogin');
    if (!isLogin || isLogin == 2) {
      wx.showToast({
        title: '请登录!',
        icon: 'loading',
      })
      // 跳转到登入界面
      wx.redirectTo({
        url: '/pages/user-login/user-login',
      });
    } else {
      var that = this;
      console.log("sssss", e);
      if (e.currentTarget.dataset.isshow == 0 && e.currentTarget.dataset.id == that.data.diaryId) {
        console.log('传ID了么', e);
        // var id = e.target.dataset.exampleid;

        wx.request({
          url: app.globalData.baseUrl + 'doctor/diary/praise',
          data: {
            diaryId: that.data.diaryId,
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('点赞结果', res);
            that.details();
            that.setData({
              id: that.data.diaryId,

            });
          }
        })
      }
    }
  },
  // 内容
  save: function(e) {
    this.setData({
      replyContent: e.detail.value,
    })
  },

  // 回复指定人
  replyPeople: function(e) {
    console.log('回复指定人:', e);
    var commentid = e.currentTarget.dataset.commentid; // 回复的评论id
    var nickname = e.currentTarget.dataset.nickname; // 回复人的昵称
    var replyPeople = '回复 ' + nickname + ':';
    this.setData({
      replyid: commentid,
      replyname: nickname,
      replyPeople: replyPeople,
    })
  },

  // 处理回复
  bindReply: function(e) {
    console.log('--发布处理结果---', e);
    var isLogin = wx.getStorageSync('isLogin');
    console.log('登录状态:', isLogin)
    if (isLogin == 2 || !isLogin) {
      wx.showModal({
        title: '提示',
        content: '请先登陆!',
        success: function(res) {
          if (res.confirm) {
            wx.redirectTo({
              url: '/pages/user-login/user-login',
            });
          } else {}
        }
      })
    } else {

      var exampleId = this.data.diaryId; // 日记本id
      var replyContent = this.data.replyContent; // 回复内容
      var replyPeople = this.data.replyPeople; // 回复的人 拼接后的
      if (!replyContent) {
        wx.showToast({
          title: '请输入内容',
          icon: 'none',
        })
        return false;
      }

      console.log('回复内容:', replyContent);

      console.log('拼接后的回复人:', replyPeople);
      var isReply = replyContent.indexOf(replyPeople); // 是否为回复人或者为直接发布
      console.log('是否在:', replyContent.indexOf(replyPeople));
      var replyId = ''; // 评论指定id
      if (isReply == -1) {
        console.log('普通回复2');
      } else {
        console.log('回复指定id1', this.data.replyid);
        replyId = this.data.replyid;

        replyContent = replyContent.replace(replyPeople, '');
      }

      console.log('exampleId:', exampleId);
      console.log('content:', replyContent);
      console.log('replyId:', replyId);
      var that = this;
      wx.request({
        url: app.globalData.baseUrl + 'doctor/diary/comment',
        data: {
          diaryId: exampleId,
          content: replyContent,
          replyId: replyId,
        },
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
        },
        method: 'POST',
        success(res) {
          console.log('回复发布结果:', res)
          if (res.data.status_code == 200) {
            that.setData({
              'pageNum': 1,
            })
            that.obtain();
            // that.pageScrollToBottom();
            that.details();
            that.setData({
              'replyPeople': '',
              'replyid': '',
              'replyContent': '',
            })
          }
        }
      })


    }
  },
  // 使页面滚动到底部
  pageScrollToBottom: function() {
    wx.createSelectorQuery().select('#j_page').boundingClientRect(function(rect) {
      console.log('底部：', rect);
      // 使页面滚动到底部
      wx.pageScrollTo({
        scrollTop: rect.height
      })
    }).exec();
  },

  // 关注按钮
  bindFollow: function(e) {
    console.log('---dd--', e.currentTarget.dataset)
    var isLogin = wx.getStorageSync('isLogin');
    if (isLogin == 2) {
      wx.showModal({
        title: '提示',
        content: '请先登陆!',
        success: function(res) {
          if (res.confirm) {
            wx.redirectTo({
              url: '/pages/user-login/user-login',
            });
          } else {}
        }
      })
    } else {
      var userid = e.currentTarget.dataset.userid;
      var isfollow = e.currentTarget.dataset.isfollow;
      console.log('关注:userid', userid);
      var that = this;
      var ziji = wx.getStorageSync('userid');
      console.log('关注:userid', userid);
      console.log('关注:isfollow', isfollow);

      if (userid == ziji) {
        wx.showToast({
          title: '不能关注自己',
          icon: 'success',
          duration: 1000
        })

      } else if (isfollow == 1) {
        wx.request({
          url: app.globalData.baseUrl + 'doctor/member/attention',
          data: {
            userId: userid,
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('关注结果:', res)
            wx.showToast({
              title: '取消关注',
              icon: 'success',
              duration: 1000
            })
            that.onLoad(that.data.options);
          }
        })
      } else {
        wx.request({
          url: app.globalData.baseUrl + 'doctor/member/attention',
          data: {
            userId: userid,
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('关注结果:', res)
            wx.showToast({
              title: '关注成功',
              icon: 'success',
              duration: 1000
            })
            that.onLoad(that.data.options);
          }
        })
      }

    }
  },

  /**
   * 用户点击右上角分享
   */
  // 关注
  onShareAppMessage: function() {
    return {
      title: '分享好友获取美丽币！',
      path: '/pages/index/index',
      // imageUrl: '/pages/images/aa.jpg',
      success: function() {
        wx.request({
          url: app.globalData.baseUrl + 'doctor/memberTaskApi/addBeauty',
          data: {
            userId: wx.getStorageSync('userid'),
            type: 3
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('分享给美丽币给美丽值', res);
          }
        })
      }
    }
  },


})