// pages/user-my-rank/user-my-rank.js
var WxParse = require('../../wxParse/wxParse.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    wallet:"",//等级数据
    percentage:"",
    imgUrl: "https://tyc-bj.cn/yc/api/attach/", //请求接口图片路径
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.live();
    this.grade();    
  },

  live:function(){
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/wallet/getUserLevel',
      data: {
        userId: wx.getStorageSync('userid'),
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log("等级", res.data.data)
        var nowScoreEnd = res.data.data.userAccount.userLevelInfo.nowScoreEnd;
        var beautyValue = res.data.data.userAccount.beautyValue;
        var percentage = beautyValue / nowScoreEnd * 100;
        that.setData({
          wallet: res.data.data.userAccount,
          percentage: percentage
        })
      }
    })
  },

  grade: function () {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'system/officialapi/officialdetail',
      data: {
        id:4,
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log("等级特权", res.data.data);
        var article = res.data.data.content;
         WxParse.wxParse('article', 'html', article, that, 5);
       
        // res.data.data["content"] = WxParse.html2json(res.data.data["content"], 'returnData');
        that.setData({
          // contentList: res.data.data.contentList["0"],
          contentList: res.data.data.contentList,          
        })
      }
    })
  },
  //会员规则
  terrace: function () {
    var id = 6;
    wx.navigateTo({
      url: '../doctor-news-system/doctor-news-system?id=' + id,
    })

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})