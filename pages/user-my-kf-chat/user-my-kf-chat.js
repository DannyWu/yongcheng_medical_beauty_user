// pages/user-my-kf-chat/user-my-kf-chat.js
var app = getApp();
Page({

  /** 
   * 页面的初始数据
   */
  data: {
    doctorId:'',//医生id
    pageSize:10,
    pageNum:1,
    loadLoading: false,
    loadComplete: false,
    talkDetail:[],//咨询详情信息
    content:"",//提问的内容
    postLoading: false,
    imgUrl: "https://tyc-bj.cn/yc/api/attach/"
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      doctorId: options.doctorId
    });
    this.getTalkDetail();
  },

  getTalkDetail:function(){
    const t = this;
    let { loadLoading, talkDetail } = t.data;
    app.request({
      url: 'consult/talkapi/getTalkListByUserAndDoctor',
      data: {
        pageSize: t.data.pageSize,
        pageNum: t.data.pageNum,
        userId: t.data.doctorId,//医生id
      },
      success: function (res) {
        const { data, page } = res;
        if (page.current_page >= page.total_page) {
          //没有更多了
          t.setData({
            loadComplete: true
          });
        }
        if (loadLoading) {
          //上拉加载
          t.setData({
            talkDetail: talkDetail.concat(data)
          });
        }
        else {
          t.setData({
            talkDetail: data,
          });
        }
        t.setData({
          loadLoading: false
        });
      },
      fail: function (res) {}
    })
    
  },

  // 保存信息
  changeContent: function (e) {
    //内容
    this.setData({
      content: e.detail.value
    });
  },

  //用户提问
  askQuestions: function () {
    const t = this;
    let { postLoading, content, doctorId} = t.data;
    if (!postLoading){
      if(content.length > 0) {
        t.setData({
          postLoading: true
        });
        app.request({
          url: 'consult/talkapi/addtalk',
          data: {
            content: content,//内容
            userId: doctorId,//医生id
            pId: '',//回复id
          },
          success: function (res) {
            t.getTalkDetail();
            t.setData({
              content: '',
              postLoading: false
            })
          },
          fail: function (res) {
          }
        });
      }
      else{
        wx.showToast({
          title: '请输入提问内容',
          icon:"none",
          duration: 2000
        });
      }
    }
    else{
      wx.showToast({
        title: '操作繁忙请稍后再试',
        icon: "none",
        duration: 2000
      });
    }
  },

  //上拉加载
  loadMoreData: function() {
    const t = this;
    let { pageNum, loadLoading, loadComplete } = t.data;
    if (!loadLoading && !loadComplete) {
      t.setData({
        pageNum: pageNum + 1,
        loadLoading: true
      });
      t.getTalkDetail();
    }
  }
})