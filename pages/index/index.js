var app = getApp()
Page({
  data: {
    isShowCity: false,
    isShowVideo: true,
    imgUrl: 'https://tyc-bj.cn/yc/api/attach/',
    userid: wx.getStorageSync('userid'),
    videotypelist: [], // 视频类型列表
    currentVideoTab: 0,
    videoadinfo: [], // 视频广告
    onetypeservicelist: [], // 一级服务类产品列表
    currentDiaryTab: 0,
    diaryPageNum: 1,
    pageSize: 50,
    more: 0, // 上拉加载判断
    diaryinfo: [], // 首页美丽日记
    imgadinfo: [], // 图片广告
    specialgoodslist: [], // 特卖商品列表
    yimeigoodsnum: 3, // 首页特卖数量
    adtype: 1, // 广告类型 0:视频 1:图片
    searchPlaceholder: '请输入医生/日记/商品名称',
    hidecity: true, // 是否隐藏城市列表
    hidearea: true, // 是否隐藏区域列表
    hidesort: true, // 是否隐藏只能排序
    hidetype: false,
    isBindCity: false, // 是否点过城市
    isBindSory: false, // 是否点过智能排序
    isAddTopCss: false, // 是否加top 样式
    url: 'https://tyc-bj.cn/yc/api/attach/images/top-jia.png',//搜索框右侧图片路径
  },

  showCity: function (e) {
    this.setData({
      isShowCity: !e.detail.isShowCity
    });
    this.setData({
      'isShowVideo': false,
    });
    this.getProvince();
    this.loadaddress();
  },

  // 首页搜索
  indexSearch: function(e) {
    var keyword = e.detail.value;
    console.log("商城首页搜索input:", e.detail.value)
    wx.navigateTo({
       url: '../user-search/user-search?keyword=' + keyword,
    });
  },
  
  //发布日记
  adddiary: function() {
    var isLogin = wx.getStorageSync('isLogin');
    if (isLogin != 2) {
      wx.navigateTo({
        url: '../user-diary-list/user-diary-list',
      })
    } else {
      wx.redirectTo({
        url: '/pages/user-login/user-login',
      });
    }
    // 
  },
  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数
    this.setData({

    });
    this.videoTypeList();
    this.loadaddress();
    console.log('视频显示:',this.data.isShowVideo);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    app.loadings();
    this.setData({
      "isShowVideo":true,
      diaryPageNum: 1,
    });
    this.loadOneTypeServiceList(); // 获取一级分类
    this.getIngandlat();
    this.loadaddress();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    wx.showLoading({
      title: '加载中...',
    })
    this.setData({
      'more': 1,
      'diaryPageNum': this.data.diaryPageNum + 1,
    });
    console.log('页码:', this.data.diaryPageNum)
    this.loadDiary();
  },

  onShareAppMessage: function() {
    return {
      title: '分享好友获取美丽币！',
      path: '/pages/index/index',
      // imageUrl: '/pages/images/aa.jpg',
      success: function() {
        wx.request({
          url: app.globalData.baseUrl + 'doctor/memberTaskApi/addBeauty',
          data: {
            userId: wx.getStorageSync('userid'),
            type: 3
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('分享给美丽币给美丽值', res);
          }
        })
      }
    }
  },
  // 首页视频分类列表
  videoTypeList: function() {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'system/advertise/getHomeVideoType',
      data: {},
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },
      method: 'POST',
      success: function(res) {
        console.log("首页视频类型列表:", res);
        that.setData({
          'videotypelist': res.data.data
        })
        that.loadVideoAd();
      }
    })
  },
  // 加载视频广告
  loadVideoAd: function() {
    var that = this;
    let {
      currentVideoTab,
      videotypelist
    } = this.data;
    wx.request({
      url: app.globalData.baseUrl + 'system/advertise/getHomeVideo',
      data: {
        'id': videotypelist[currentVideoTab]['id'],
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('视频广告:', res.data.data)
        that.setData({
          'videoadinfo': res.data.data,
        })
      }
    })
  },
  // 切换首页视频广告类别
  changeVideoTab: function(e) {
    console.log(e);
    this.setData({
      'currentVideoTab': e.detail.index,
    });
    this.loadVideoAd();
  },
  loadAllData: function() {
    this.loadImgAd(); // 图片广告
    this.specialGoods(); // 推荐(特卖)商品
    this.loadDiary();
    wx.showToast({
      title: '加载中...',
      icon: 'loading',
      duration: 1500
    });
  },
  // 一级服务产品类别列表
  loadOneTypeServiceList: function(e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'shopp/goodsTypeApi/getPage',
      data: {
        'type': 2,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },
      method: 'POST',
      success: function(res) {
        console.log("一级服务产品:", res);
        that.setData({
          'onetypeservicelist': res.data.data
        })
        that.loadAllData(); // 分类列表加载完成 请求页面全部数据
      }
    })
  },
  // 首页特卖推荐区
  specialGoods: function(e) {
    var that = this;
    var cityid = wx.getStorageSync('cityid');
    wx.request({
      url: app.globalData.baseUrl + 'shopp/mallApi/serviceGood?cityid=' + cityid + '&yimeigoodsnum=' + that.data.yimeigoodsnum,
      data: {},
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/json',
      },
      success(res) {
        console.log('首页特卖:', res)
        that.setData({
          'specialgoodslist': res.data.data.serviceGoods,
        })
      }
    })
  },
  // 加载图片广告
  loadImgAd: function(e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'system/advertise/getAdvertise',
      data: {
        'type': 0,
        'adType': 'index',
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('图片广告:', res.data.data)
        var tempData = res.data.data;
        for (var i = 0; i < tempData.length; i++) {
          if (tempData[i].srcType == 1) {
            tempData[i].srcUrl = '../user-shop-goods-details/user-shop-goods-details?etype=1&id=' + tempData[i].srcId;
          } else if (tempData[i].srcType == 2) {
            tempData[i].srcUrl = '../user-shop-goods-details/user-shop-goods-details?etype=2&id=' + tempData[i].srcId;
          }
        }
        that.setData({
          'imgadinfo': tempData,
        });
      }
    })
  },
  // 首页美丽日记数据
  loadDiary: function() {
    var that = this;
    let {
      currentDiaryTab,
      onetypeservicelist,
      pageSize,
      diaryPageNum
    } = this.data;
    let currentDiaryType = onetypeservicelist[currentDiaryTab]['id'];
    wx.request({
      url: app.globalData.baseUrl + 'doctor/diary/getPage',
      data: {
        'cityId': wx.getStorageSync('cityid'),
        'oneType': currentDiaryType,
        'pageSize': pageSize,
        'pageNum': diaryPageNum,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('日记列表:', res)
        if (res.data.data) {
          if (that.data.more == 0) {
            that.setData({
              'diaryinfo': res.data.data,
            })
          } else {
            wx.hideLoading();
            that.setData({
              more: 0,
            })
            if (res.data.data.length) {
              var info = that.data.diaryinfo.concat(res.data.data);
              that.setData({
                'diaryinfo': info,
              })
            } else {
              wx.showToast({
                title: '已无更多...',
                icon: 'none',
                duration: 2000,
              });
            }
          }
        }
      }
    })
  },
  changeDiaryTab: function(e) {
    this.setData({
      'currentDiaryTab': e.detail.index,
      'diaryPageNum': 1
    });
    this.loadDiary();
  },
  // 关注按钮
  bindFollow: function(e) {
    console.log('---dd--', e.currentTarget.dataset)
    var isLogin = wx.getStorageSync('isLogin');
    if (isLogin == 2) {
      wx.showModal({
        title: '提示',
        content: '请先登陆!',
        success: function(res) {
          if (res.confirm) {
            wx.redirectTo({
              url: '../user-login/user-login',
            });
          } else {}
        }
      })
    } else {
      var userid = e.currentTarget.dataset.userid;
      var isfollow = e.currentTarget.dataset.isfollow;
      console.log('关注:userid', userid);
      var that = this;
      if (isfollow == 1) {
        wx.request({
          url: app.globalData.baseUrl + 'doctor/member/attention',
          data: {
            userId: userid,
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('关注结果:', res)
            wx.showToast({
              title: '取消关注',
              icon: 'success',
              duration: 1000
            })
            that.setData({
              diaryPageNum: 1,
            });
            that.loadDiary();
            wx.request({
              url: app.globalData.baseUrl + 'doctor/memberTaskApi/addBeauty',
              data: {
                userId: wx.getStorageSync('userid'),
                type: 5,
              },
              header: {
                'sign': app.globalData.sign,
                'token': app.globalData.token,
                'content-type': 'application/x-www-form-urlencoded; charset=utf - 8',
              },
              method: 'POST',
              success(res) {
                console.log('给美丽币给美丽值', res);
              }
            })
          }
        })
      } else {
        wx.request({
          url: app.globalData.baseUrl + 'doctor/member/attention',
          data: {
            userId: userid,
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('关注结果:', res)
            wx.showToast({
              title: res.data.message,
              icon: 'success',
              duration: 1000
            });

            that.setData({
              diaryPageNum: 1,
            });
            that.loadDiary();
          }
        })
      }

    }
  },

  //发布评论
  pinglun: function() {
    var isLogin = wx.getStorageSync('isLogin');
    if (isLogin != 2) {

    } else {
      wx.redirectTo({
        url: '/pages/user-login/user-login',
      });
    }
  },
  //点赞
  dianzan: function() {
    var isLogin = wx.getStorageSync('isLogin');
    if (isLogin != 2) {} else {
      wx.redirectTo({
        url: '/pages/user-login/user-login',
      });
    }
  },

  // 关注
  onShareAppMessage: function() {
    return {
      title: '分享好友获取美丽币！',
      path: '/pages/index/index',
      // imageUrl: '/pages/images/aa.jpg',
      success: function() {
        wx.request({
          url: app.globalData.baseUrl + 'doctor/memberTaskApi/addBeauty',
          data: {
            userId: wx.getStorageSync('userid'),
            type: 3
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('分享给美丽币给美丽值', res);
          }
        })
      }
    }
  },

  // 获取经纬度
  getIngandlat: function() {
    var that = this;
    wx.getLocation({
      type: 'wgs84',
      success(res) {
        var latitude = res.latitude;
        var longitude = res.longitude;
        wx.setStorageSync('latitude', latitude);
        wx.setStorageSync('longitude', longitude);
        console.log('存储经纬度');
      }
    })
  },
  // 离开页面关闭城市列表
  onHide: function () {
    this.setData({
      isShowCity: false,
      hidecity: true,
      hidearea: true,
      hidesort: true,
      hidetype: false,
    })
  },

  // 点击省
  bindProvince: function (e) {
    var provinceid = e.currentTarget.dataset.provinceid;
    var provincename = e.currentTarget.dataset.provincename;
    if (provinceid != this.data.provinceid) {
      this.setData({
        'provinceid': provinceid,
        'provincename': provincename,
      });
      console.log('省id', provinceid)
      var area = [provinceid, 2, 1];
      this.getCity(area);

    } else {
      return false;
    }
  },

  // 点击市
  bindCity: function (e) {
    var cityid = e.currentTarget.dataset.cityid;
    var cityname = e.currentTarget.dataset.cityname;
    if (cityid != this.data.cityid) {
      this.setData({
        'cityid': cityid,
        'areaname': cityname,
      });
      wx.setStorageSync('cityid', cityid);
      wx.setStorageSync('cityname', cityname);
      wx.setStorageSync('provinceid', this.data.provinceid);
      wx.setStorageSync('provincename', this.data.provincename);
      this.setData({
        isShowCity: false,
        'hidetype': false,
        "isShowVideo":true,
      })
    } else {
      this.setData({
        isShowCity: false,
        'hidetype': false,
        "isShowVideo":true,
      })
      return false;
    }
  },
  // 获取省
  getProvince: function (e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/city/getCityList',
      data: {
        id: '',
        level: '',
        currentLevel: '',
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('省列表:', res)
        that.setData({
          'provincelist': res.data.data,
        })
      }
    })
  },

  // 获取市
  getCity: function (e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/city/getCityList',
      data: {
        id: e[0],
        level: e[1],
        currentLevel: e[2],
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('当前省的所有市列表:', res)
        var citylist = res.data.data;
        that.setData({
          'citylist': citylist,
        })
        var cityid = wx.getStorageSync('cityid');
        if (cityid) {
          for (var i = 0; i < citylist.length; i++) {
            if (citylist[i]["cityId"] == cityid) {
              that.setData({
                areaname: citylist[i]["cityName"],
              })
            }
          }
          wx.setStorageSync('cityname', that.data.areaname);
        }
      }
    })
  },
  //获取当前位置
  loadaddress: function () {
    var that = this;
    wx.getLocation({
      type: 'wgs84',
      success(res) {
        console.log("定位", res)
        var latitude = res.latitude;
        var longitude = res.longitude;
        wx.setStorageSync('latitude', latitude)
        wx.setStorageSync('longitude', longitude)
        var location = latitude + "," + longitude;
        console.log("location", location)
        wx.request({
          url: app.globalData.baseUrl + 'doctor/city/getAreaByLocation',
          data: {
            'location': location
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('当前位置:', res);
            console.log('当前位置省id:', res.data.data.proId);
            console.log('当前位置市id:', res.data.data.cityId);
            var proId = res.data.data.proId;
            var cityId = res.data.data.cityId;
            that.setData({
              provinceid: proId,
              cityid: cityId,
            })
            wx.setStorageSync('provinceid', proId);
            wx.setStorageSync('cityid', cityId);
            var area = [proId, 2, 1];
            that.getCity(area);
          }
        })
      }
    })
  },


})