// pages/diary/diary.js
var app = getApp();
Page({

  /** 
   * 页面的初始数据
   */
  data: {
    pageSize: 10, //每页条数
    pageNum: 1, // 页数
    diaryBookId: "", //日记id
    arraydata: "",
    imgUrl: "https://tyc-bj.cn/yc/api/attach/",
    BookId: "",
    userdata: "",
    userId: "", //关注用的  用户ID
    time: '', //转的时间
    more: 0, // 是否为上拉加载
    isshow:0,//点赞默认
    options:'',//默认
    usid: wx.getStorageSync('userid'),
  }, 



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log("book", options)
    this.setData({
      options: options
    }) 

  },
  //美丽日记列表上边内容
  rijishang: function(options) {
    console.log('--3322---', options)
    var bookid = options.bookid
    // var diaryId = options.diaryId
    var that = this;
    //美丽日记列表上边内容
    wx.request({
      url: app.globalData.baseUrl + 'doctor/diaryBook/detail', //仅为示例，并非真实的接口地址
      data: {
        bookId: bookid, //日记本id   
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log('日记列表上边用户数据', res.data.data)
        that.setData({
          userdata: res.data.data,
          // userId: res.data.data.diaryBook.userId,
          time: app.timeData(res.data.data.diaryBook.addTime),
        })
      }
    })
  },
  //美丽日记列表
  riji: function(options) {
    console.log("--测试---", options)
    var bookid = options.bookid
    this.setData({
      bookid:bookid,
    });
    // var diaryId = options.diaryId
    var that = this;
    //美丽日记列表
    wx.request({
      url: app.globalData.baseUrl + 'doctor/diary/getDiaryList', //仅为示例，并非真实的接口地址
      data: {
        diaryBookId: bookid, //日记本id
        pageSize: that.data.pageSize, //每页条数
        pageNum: that.data.pageNum, // 页数

      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log('日记列表2', res.data.data)
        wx.hideLoading();
        if (res.data.data) {
          if (that.data.more == 0) {
            that.setData({
              'arraydata': res.data.data,
            })
          } else {
            that.setData({
              more: 0,
            })
            if (res.data.data.length) {

              var info = that.data.arraydata.concat(res.data.data);
              that.setData({
                'arraydata': info,
              })
            } else {
              wx.showToast({
                title: '已无更多...',
                icon: 'none',
                duration: 2000,
              });
            }
          }
        }
        
      }
    })
  },

  //关注按钮
  bindFollow: function (e) {
    var that=this;
    console.log('---ceshi--', e.currentTarget.dataset)
    var isLogin = wx.getStorageSync('isLogin');
    if (isLogin == 2) {
      wx.showModal({
        title: '提示',
        content: '请先登陆!',
        success: function (res) {
          if (res.confirm) {
            wx.redirectTo({
              url: '/pages/user-login/user-login',
            });
          } else { }
        }
      })
    } else {
      var userid = e.currentTarget.dataset.userid;
      var isfollow = e.currentTarget.dataset.isfollow;
      var ziji = wx.getStorageSync('userid');
      console.log('关注:userid', userid);
      console.log('关注:isfollow', isfollow);
     
      if (userid == ziji) {
        wx.showToast({
          title: '不能关注自己',
          icon: 'success',
          duration: 1000
        })

      } else if (isfollow == 1) {

        wx.request({
          url: app.globalData.baseUrl + 'doctor/member/attention',
          data: {
            userId: userid,
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('关注结果:', res)
            wx.showToast({
              title: '取消关注',
              icon: 'success',
              duration: 1000
            })
            that.onShow(that.data.options);
          }
        })
       
      } else if (isfollow == 2) {
        wx.request({
          url: app.globalData.baseUrl + 'doctor/member/attention',
          data: {
            userId: userid,
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('关注结果:', res)
            wx.showToast({
              title: '关注成功',
              icon: 'success',
              duration: 1000
            })
            that.onShow(that.data.options);
          }
        })
      
      }


    }
  },
  // 处理点赞按钮
  handlePoint: function(e) {
    var that = this;
    var isLogin = wx.getStorageSync('isLogin');
    if (!isLogin || isLogin == 2) {
      wx.showToast({
        title: '请登录!',
        icon: 'loading',
      })
      // 跳转到登入界面
      wx.redirectTo({
        url: '/pages/user-login/user-login',
      });
    } else {
      var that = this;
      console.log("sssss", e.currentTarget.dataset);
      if (e.currentTarget.dataset.isshow == 0) {
        console.log('传ID了么', e);
        // var id = e.target.dataset.exampleid;

        wx.request({
          url: app.globalData.baseUrl + 'doctor/diary/praise',
          data: {
            diaryId: e.currentTarget.dataset.id,
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('点赞结果', res);

            that.setData({
              id: that.data.diaryId,
              isshow: 1,
            });
            that.onLoad(that.data.options);
          }
        })
      }
    }
  },
  // 日记浏览量
  browse: function() {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/diary/addDiaryViews', //仅为示例，并非真实的接口地址
      data: {
        userId: that.data.userId, //用户id
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log('浏览量增加', res)
        // that.setData({
        //   arraydata: res.data.data
        // }),
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var options = this.data.options;
    this.rijishang(options)
    this.riji(options)
    this.setData({
      'more': 0,
      'pageNum': 1,
    })
   
  },
  

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    wx.showLoading({
      title: '加载中...',
    })
    this.setData({
      'more': 1,
      'pageNum': this.data.pageNum + 1,
    });
    console.log('页码:', this.data.pageNum)
    this.riji(this.data);
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    return {
      title: '分享好友获取美丽币！',
      path: '/pages/index/index',
      // imageUrl: '/pages/images/aa.jpg',
      success: function() {
        wx.request({
          url: app.globalData.baseUrl + 'doctor/memberTaskApi/addBeauty',
          data: {
            userId: wx.getStorageSync('userid'),
            type: 3
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('分享给美丽币给美丽值', res);
          }
        })
      }
    }
  },
  //单张图片放大
  imgYu: function (event) {
    console.log('assacsa', event.currentTarget.dataset)
    var src = event.currentTarget.dataset.src; //获取data-src
    var imgUrl = app.globalData.imgUrl;
    var a = [];
    a.push(imgUrl+src);
    // a.push(imgUrl + imgLists);
    //图片预览
    wx.previewImage({
      current: imgUrl + src, // 当前显示图片的http链接
      urls: a
    })
  }, 
  //数组图片放大
  imgYu1: function (event) {
    console.log('assacsa', event.currentTarget.dataset)
    var src = event.currentTarget.dataset.src; //获取data-src
    var imgList = event.currentTarget.dataset.list; //获取data-list
    console.log('imgList', imgList[0])
    var imgUrl = app.globalData.imgUrl;
    var a = [];
    //获取下标
    var o = event.currentTarget.dataset.inde;
    // 循环 list数组
    for (var s in imgList) a.push(imgUrl + imgList[s]);

    // a.push(imgUrl + imgLists);
    //图片预览
    console.log('yaokan1', imgUrl + imgList[o]);
    console.log('yaokan2', a);
    wx.previewImage({
      current: imgUrl + imgList[o], // 当前显示图片的http链接
      urls: a// 需要预览的图片http链接列表
    })
    this.onShow();
  },

})