// pages/user-service-order-pay/user-service-order-pay.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderId:'',
    totalMoney: '',
    maxCoin: '', // 美丽币
    maxPrice: '', // 抵用钱数
    balance: '', // 余额
    calculateTotalMoney: '',
    checkedBalance: false,
    checkedcoin: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      orderId: options.orderId,
      totalMoney: options.storePayment,
      calculateTotalMoney: options.storePayment
    });
    this.beautifulCoin();
  },
  // 获取美丽币
  beautifulCoin() {
    const that = this;
    app.request({
      url: "doctor/beautyCoin/getBeautyCoin",
      success: function (res) {
        that.setData({
          maxCoin: res.data.maxCoin,
          maxPrice: res.data.maxPrice,
          balance: res.data.currUser.totalMoney
        })
      },
      fail: function (res) {
        // app.requestFail(res);
      }
    });
  },

  changeBanlanceChecked: function (e) {
    let { balance } = this.data;
    if (Number(balance) > 0) {
      let checked = !this.data.checkedBalance;
      this.setData({
        checkedBalance: checked
      });
      this.calculateTotalPrice();
    } else {
      wx.showToast({
        icon: 'none',
        title: '您的余额不足',
        duration: 1000
      });
    }
  },
  changeCoinChecked: function (e) {
    let { maxCoin } = this.data;
    if (Number(maxCoin) > 0) {
      let checked = !this.data.checkedcoin;
      this.setData({
        checkedcoin: checked
      });
      this.calculateTotalPrice();
    } else {
      wx.showToast({
        icon: 'none',
        title: '您的美丽币不足',
        duration: 1000
      });
    }
  },
  calculateTotalPrice: function () {
    let {
      totalMoney,
      balance,
      maxPrice,
      checkedcoin,
      checkedBalance
    } = this.data;
    let total = Number(totalMoney);
    if (checkedcoin) {
      total = total - Number(maxPrice);
      if (checkedBalance) {
        if (total - Number(balance) > 0) {
          total = total - Number(balance);
        }
      }
    } else {
      if (checkedBalance) {
        if (total - Number(balance) > 0) {
          total = total - Number(balance);
        }
      }
    }
    this.setData({
      calculateTotalMoney: total
    });
  }, 

  payTail: function(e){
    const { orderId, checkedBalance, checkedcoin, balance, maxPrice, calculateTotalMoney} = this.data;
    const that = this;
    app.request({
      url: "shopp/payment/serviceOrderTailmoney",
      data:{
        orderid: orderId,
        usebalance: checkedBalance ? balance : 0,
        usecoin: checkedcoin ? maxPrice : 0,
      },
      success: function (res) {
        let { ifPayment } = res.data;
        if (ifPayment) {
          wx.navigateTo({
            url: `../user-shop-goods-order-pay/user-shop-goods-order-pay?res=${orderId}&totalMoney=${calculateTotalMoney}&payType=1&orderType=1`,
          });
        } else {
          wx.navigateTo({
            url: `../user-shop-goods-balance-pay/user-shop-goods-balance-pay?res=${orderId}&balance=${calculateTotalMoney}`,
          });
        }
      },
      fail: function (res) {
        
      }
    });
  }
})