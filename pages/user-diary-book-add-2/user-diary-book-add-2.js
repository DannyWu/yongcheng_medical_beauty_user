 // pages/user-diary-book-add-2/user-diary-book-add-2.js
 var app = getApp();
 Page({

   /**
    * 页面的初始数据
    */
   data: {
     choosed: [], // 已选标签
     choosedid: [], // 已选id
     productPic: '', // 医美产品图片
     images: [],
     images1: [],
     images2: [],
     imagesurl: [], // 
     route1: [], //上传成功的图片地址
     route2: [], //上传成功的图片地址
     route3: [], //上传成功的图片地址
     userinfo: "",
     orderId: "",
     goodsId: "",
     doctor: "",
     name: "",
     place: "",
     take: !1,
   },

   /**
    * 生命周期函数--监听页面加载
    */
   onLoad: function(options) {
     console.log("---数据---", options)
     var choosed = options.choosed;
     var choosedid = options.choosedid;
     var choosed = choosed.split(",")
     this.setData({
       choosed: choosed,
       choosedid: choosedid,
     })
     this.photo(); //获取头像
   },
   //选择订单
   selectorder: function() {
     wx.navigateTo({
       url: '../user-diary-book-add-3/user-diary-book-add-3',
      
     })
   },
   //协议内容
   agreement: function() {
     var id = 10;
     wx.navigateTo({
       url: '../doctor-news-system/doctor-news-system?id=' + id,
     })
   },

   // 上传图片
   chooseImg: function(e) {
     var that = this;
     wx.chooseImage({
       sizeType: ['compressed'],
       sourceType: ['album', 'camera'],
       success: function(res) {
         // 将现有和新上传的合并 
         var images = that.data.images.concat(res.tempFilePaths);
         images = images.length <= 6 ? images : images.slice(0, 6);
         that.setData({
           images: images,
         })
         that.uploadimages(); // 选择图片后 上传到服务器上
       },
     })
   },
   //选择侧面45
   chooseImg1: function(e) {
     var that = this;
     wx.chooseImage({
       sizeType: ['compressed'],
       sourceType: ['album', 'camera'],
       success: function(res) {
         // 将现有和新上传的合并
         var images = that.data.images1.concat(res.tempFilePaths); // 合并原有数组和现传照片
         images = images.length <= 1 ? images : images.slice(0, 1); // 取前5张
         that.setData({
           'images1': images,
         })
         // console.log("123qweqwe", that.data.images)
         that.uploadimages1() //上传到服务器
       },
     })
   },
   //选择侧面
   chooseImg2: function(e) {
     var that = this;
     wx.chooseImage({
       sizeType: ['compressed'],
       sourceType: ['album', 'camera'],
       success: function(res) {
         // 将现有和新上传的合并
         var images = that.data.images2.concat(res.tempFilePaths); // 合并原有数组和现传照片
         images = images.length <= 1 ? images : images.slice(0, 1); // 取前5张
         that.setData({
           'images2': images,
         })
         // console.log("123qweqwe", that.data.images)
         that.uploadimages2() //上传到服务器
       },
     })
   },


   // 上传图片正面
   uploadimages: function(e) {
     var that = this;
     wx.showToast({
       icon: "loading",
       title: "正在上传"
     });
     var images = this.data.images;
     var imagesArr = [];
     for (var i = 0; i < images.length; i++) {
       wx.uploadFile({
         // url: app.globalData.baseUrl + 'doctor/product/uploadFile',
         url: app.globalData.baseUrl + 'doctor/member/uploadFile',
         formData: {

           type: 1,
         },
         filePath: images[i],
         name: 'file',
         header: {
           'sign': app.globalData.sign,
           'token': app.globalData.token,
           "Content-Type": "multipart/form-data"
         },
         success: function(res) {
           var path = JSON.parse(res.data).data[0];
           imagesArr.push(path);
           console.log('图片数组:', imagesArr)
           that.setData({
             'route1': imagesArr,
           })
           app.globalData.route1 = imagesArr
           // that.data.imagesurl.push(path);
           console.log("返回路径信息", that.data.route1)
           // wx.setStorageSync(that.data.route1)
         }
       })
     };
   },

   // 上传图片侧面45
   uploadimages1: function(e) {
     var that = this;
     wx.showToast({
       icon: "loading",
       title: "正在上传"
     });
     var images = this.data.images1;
     var imagesArr = [];
     for (var i = 0; i < images.length; i++) {
       wx.uploadFile({
         // url: app.globalData.baseUrl + 'doctor/product/uploadFile',
         url: app.globalData.baseUrl + 'doctor/member/uploadFile',
         formData: {

           type: 1,
         },
         filePath: images[i],
         name: 'file',
         header: {
           'sign': app.globalData.sign,
           'token': app.globalData.token,
           "Content-Type": "multipart/form-data"
         },
         success: function(res) {
           var path = JSON.parse(res.data).data[0];
           imagesArr.push(path);
           console.log('图片数组:', imagesArr)
           // that.data.imagesurl.push(path);
           that.setData({
             'route2': imagesArr
           })
           app.globalData.route2 = imagesArr
         }
       })
     };
   },

   // 上传图片侧面
   uploadimages2: function(e) {
     var that = this;
     wx.showToast({
       icon: "loading",
       title: "正在上传"
     });
     var images = this.data.images2;
     var imagesArr = [];
     for (var i = 0; i < images.length; i++) {
       wx.uploadFile({
         // url: app.globalData.baseUrl + 'doctor/product/uploadFile',
         url: app.globalData.baseUrl + 'doctor/member/uploadFile',
         formData: {

           type: 1,
         },
         filePath: images[i],
         name: 'file',
         header: {
           'sign': app.globalData.sign,
           'token': app.globalData.token,
           "Content-Type": "multipart/form-data"
         },
         success: function(res) {
           var path = JSON.parse(res.data).data[0];
           imagesArr.push(path);
           console.log('图片数组:', imagesArr)
           that.setData({
             'route3': imagesArr
           })
           app.globalData.route3 = imagesArr
         }
       })
     };
   },

   // 删除图片
   handleDeleteImage: function(e) {
     var index = e.target.dataset.index;
     var images = this.data.images;
     images.splice(index, 1);
     console.log(images);
     this.setData({
       'images': images,
     })
   },

   selectorder: function() {
     wx.navigateTo({
       url: '../user-diary-book-add-3/user-diary-book-add-3?choosed=' + this.data.choosed + '&choosedid=' + this.data.choosedid,
     })
   },
   // 完成
   nextPage: function(e) {
     var that = this;
     var front = that.data.route1;
     var sideFour = that.data.route2;
     var side = that.data.route3;
     if (that.data.orderId == "") {
       wx.showToast({
         title: '请选择订单',
         icon: 'none',
         duration: 2000
       })
       return false;
     }
     if (front == "") {
       wx.showToast({
         title: '请上传正面照片',
         icon: 'none',
         duration: 2000
       })
       return false;
     }
     if (sideFour == "") {
       wx.showToast({
         title: '请上传侧面45°照片',
         icon: 'none',
         duration: 2000
       })
       return false;
     }
     if (side == "") {
       wx.showToast({
         title: '请上传侧面照片',
         icon: 'none',
         duration: 2000
       })
       return false;
     }
     var choo = this.data.choosedid; //转成数组
     var choosedid = choo.split(",")
     console.log("---321--", that.data.orderId)
     console.log("---456--", that.data.goodsId)

     wx.request({
       url: app.globalData.baseUrl + 'doctor/diaryBook/create',
       data: {
         oneType: choosedid[0],
         twoType: choosedid[1],
         threeType: choosedid[2],
         orderId: that.data.orderId, //	订单ID
         goodId: that.data.goodsId, //医美产品ID
         front: app.globalData.route1[0].fileId, //正面照
         sideFour: app.globalData.route2[0].fileId, //侧面45
         side: app.globalData.route3[0].fileId, //侧面
       },
       header: {
         'sign': app.globalData.sign,
         'token': app.globalData.token,
         'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
       },
       method: 'POST',
       success(res) {
         console.log('提交日记本返回', res);
         wx.reLaunch({
           url: "../user-diary-book-add-5/user-diary-book-add-5",
         })
       }
     })
   },

   //获取头像
   photo: function() {
     var userinfo = wx.getStorageSync('userInfo')
     this.setData({
       userinfo: userinfo
     })
     console.log("asdas", this.data.userinfo)
   },
   /**
    * 生命周期函数--监听页面初次渲染完成
    */
   onReady: function() {

   },

   /**
    * 生命周期函数--监听页面显示
    */
   onShow: function() {
     var that = this;
     if (that.data.orderId == "") {
       that.setData({
         take: !1
       })
     } else {
       that.setData({
         take: !0
       })
     }
   },

   /**
    * 生命周期函数--监听页面隐藏
    */
   onHide: function() {

   },

   /**
    * 生命周期函数--监听页面卸载
    */
   onUnload: function() {

   },

   /**
    * 页面相关事件处理函数--监听用户下拉动作
    */
   onPullDownRefresh: function() {

   },

   /**
    * 页面上拉触底事件的处理函数
    */
   onReachBottom: function() {

   },

   /**
    * 用户点击右上角分享
    */
   onShareAppMessage: function() {

   }
 })