// pages/user-my-order-details-medical-1/user-my-order-details-medical-1.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderid: "",//订单ID
    operation: "",//术前后详情数据//医美的详情数据
    imgUrl: "https://tyc-bj.cn/yc/api/attach/",//拼接图片路径
    options:"",
  },
 
  /** 
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("医美123", options)
    var orderid = options.id;//订单ID
    var orderType = options.orderType;//订单类型2医美1术前术后
    this.setData({
      orderid: orderid,
      orderType: orderType,
      options:options,
    })
    this.orderdetails();
  },

  // 获取用户订单详情医美的
  orderdetails: function () {
    var that = this;
    console.log("------医美123", that.data.orderid)
    // 获取用户订单详情
    wx.request({
      url: app.globalData.baseUrl + 'shopp/yimeiGoodsOrderApi/getOrderDetail',
      data: {
        orderid: that.data.orderid,//订单ID
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {

        console.log("医美的订单详情", res.data.data)
        that.setData({
          operation: res.data.data
        })
      }
    })
  },

  //取消订单按钮 1是术前术后  2是医美
  qxorder: function () {
    // console.log('取消id', e.currentTarget.dataset)
    var that = this;
    // var id = e.currentTarget.dataset.id;
    // that.setData({
    //   id: e.currentTarget.dataset.sid
    // })
    
    var ordertype = that.data.orderType;
    if (ordertype == 2) {
      //删除医美订单
      wx.request({
        url: app.globalData.baseUrl + 'shopp/yimeiGoodsOrderApi/cancelOrder',
        data: {
          id: that.data.orderid,
        },
        method: 'POST',
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
        },
        success(res) {
          console.log("删除", res)
          wx.showToast({
            title: "成功",
            icon: 'success',
            duration: 1000
          })
          wx.navigateTo({
            url: '../user-my-order-list/user-my-order-list',
          })
        }
      })
    } else if (ordertype == 1) {
      var that = this;
      console.log("---", that.data.id)
      //删除术前术后订单
      wx.request({
        url: app.globalData.baseUrl + 'shopp/goodsOrderApi/cancelOrder',
        data: {
          id: that.data.id,
        },
        method: 'POST',
        // method: 'GET', 
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
          // 'content-type': 'application/json'
        },
        success(res) {
          console.log("删除术前", res)
          wx.showToast({
            title: "成功",
            icon: 'success',
            duration: 1000
          })
          
          wx.navigateTo({
            url: '../user-my-order-list/user-my-order-list',
          })

        }
      })
    } else {

    }

  },
  //发起支付请求
  fkorder: function (e) {
    const { operation, orderid } = this.data;
    if (operation.orderStateCode == 2) {
      //支付尾款
      wx.navigateTo({
        url: `../user-service-order-pay/user-service-order-pay?orderId=${orderid}&storePayment=${operation.storePayment}`,
      })
    }
    else if (operation.orderStateCode == 0) {
      //支付预付款
      if (operation.ifPayment) {
        wx.navigateTo({
          url: `../user-shop-goods-order-pay/user-shop-goods-order-pay?res=${orderid}&totalMoney=${operation.paymentMoney}&payType=1&orderType=1`,
        });
      } else {
        wx.navigateTo({
          url: `../user-shop-goods-balance-pay/user-shop-goods-balance-pay?res=${orderid}&balance=${operation.useBalance}`,
        });
      }
    }
  },
  //待安排查看日程
  see: function () {
    wx.showModal({
      title: '提示',
      content: '暂无安排，请耐心等待，我们会尽快联系您给您安排',
      success: function (res) {
        if (res.confirm) {
          console.log('用户点击确定')
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  //代发货 待收货
  receipt: function (e) {
    console.log('确认收货id', e.currentTarget.dataset)

    wx.request({
      url: app.globalData.baseUrl + 'shopp/goodsOrderApi/confirmCollectGoods',
      data: {
        id: e.currentTarget.dataset.orderid
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log("确认收货结果", res)
        that.onLoad(that.data.type);

      }
    })
  }
})