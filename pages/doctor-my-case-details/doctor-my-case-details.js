// pages/doctor-my-case-details/doctor-my-case-details.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrl: "https://tyc-bj.cn/yc/api/attach/",
    exampleId: '',
    content: "",
    replyId: "",
    pageNum: 1,
    pageSize: 5,
    caseinfo: [],
    casecommentinfo: [],
    caseid: '', // 案例id
    isreply: false, // 是否为回复 false.直接发布 true.回复
    replyContent: '', // 回复内容
    replyPeople: '', // 回复的人(拼接后的) 格式:回复 ***:
    replyid: '', // 评论指定id
    replyname: '', // 回复的人的昵称
    more: 0, // 上拉加载判断

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    wx.getUserInfo({
      success: function(res) {
        console.log(res.userInfo)
        var userInfo = res.userInfo;
        that.setData({
          nickName: userInfo.nickName,
          avatarUrl: userInfo.avatarUrl,
        })
      }
    })

    var caseid = options.id;
    var isshowadd = options.isshowadd;
    that.setData({
      'caseid': caseid,
      'isshowadd': isshowadd,
    })
    this.loadCaseDetail(caseid);
    this.getCaseComment(caseid);
    this.browse(caseid); //+浏览量
    

  },



  //获取案例评论
  getCaseComment: function(e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/docExample/commentPage',
      data: {
        exampleId: e,
        pageSize: that.data.pageSize,
        pageNum: that.data.pageNum
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('获取案例评论', res)
        if (that.data.more == 0) {
          that.setData({
            'casecommentinfo': res.data.data,
          })
        } else {
          wx.hideLoading();
          that.setData({
            more: 0,
          })
          if (res.data.data.length) {

            var info = that.data.casecommentinfo.concat(res.data.data);
            that.setData({
              'casecommentinfo': info,
            })
          } else {
            wx.showToast({
              title: '已无更多...',
              icon: 'none',
              duration: 2000,
            });
          }
        }
      }
    })
  },


  // 案例详情
  loadCaseDetail: function(e) {
    console.log('案例id:', e);
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/docExample/detail',
      data: {
        exampleId: e,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('案例详情', res);
        that.setData({
          'caseinfo': res.data.data[0],
          'exampleId': res.data.data[0].exampleId,


        })
        console.log('案例详情iddd', that.data.exampleId)
      }
    })
  },

  //浏览量增加
  browse: function(e) {
    var that = this;
    var exampleId = e;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/docExample/updateView',
      data: {
        exampleId: exampleId,
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log('增加浏览数', res.data)
      }
    })
  },

  // 处理点赞按钮
  handlePoint: function(e) {
    var that = this;
    var isLogin = wx.getStorageSync('isLogin');
    if (!isLogin || isLogin == 2) {
      wx.showToast({
        title: '请登录!',
        icon: 'loading',
      })
      // 跳转到登入界面
      wx.redirectTo({
        url: '/pages/user-login/user-login',
      })
    } else {


      console.log(e);
      var id = e.currentTarget.dataset.exampleid;
      wx.request({
        url: app.globalData.baseUrl + 'doctor/docExample/updatePoint',
        data: {
          exampleId: id,
        },
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
        },
        method: 'POST',
        success(res) {
          console.log('点赞结果', res);
          if (res.statusCode == 200) {
            that.loadCaseDetail(that.data.caseid);
            // that.setData({
            //   'caseinfo': res.data.data[0],
            // })
          }
        }
      })
    }
  },

  // 关注按钮
  bindFollow: function(e) {
    var isLogin = wx.getStorageSync('isLogin');
    if (isLogin == 2) {
      wx.showModal({
        title: '提示',
        content: '请先登陆!',
        success: function(res) {
          if (res.confirm) {
            wx.redirectTo({
              url: '/pages/user-login/user-login',
            });
          } else {}
        }
      })
    } else {
      var doctorid = e.currentTarget.dataset.doctorid;
      console.log('关注:doctorid', doctorid);
      var that = this;
      wx.request({
        url: app.globalData.baseUrl + 'doctor/member/attention',
        data: {
          userId: doctorid,
        },
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
        },
        method: 'POST',
        success(res) {
          console.log('关注医生结果:', res)
          that.loadCaseDetail(that.data.caseid);
        }
      })
    }
  },

  // 回复指定人
  replyPeople: function(e) {
    console.log('回复指定人:', e);
    var commentid = e.currentTarget.dataset.commentid; // 回复的案例id
    var nickname = e.currentTarget.dataset.nickname; // 回复人的昵称
    var replyPeople = '回复 ' + nickname + ':';
    this.setData({
      'replyid': commentid,
      'replyname': nickname,
      'replyPeople': replyPeople,
    })
  },

  // 回复内容
  replyContent: function(e) {
    this.setData({
      'replyContent': e.detail.value,
    })
  },

  // 处理回复
  bindReply: function(e) {
    console.log(e);
    var isLogin = wx.getStorageSync('isLogin');
    console.log('登录状态:', isLogin)
    if (isLogin == 2 || !isLogin) {
      wx.showModal({
        title: '提示',
        content: '请先登陆!',
        success: function(res) {
          if (res.confirm) {
            wx.redirectTo({
              url: '/pages/user-login/user-login',
            });
          } else {}
        }
      })
    } else {

      var exampleId = this.data.caseid; // 案例id 
      var replyContent = this.data.replyContent; // 回复内容
      var replyPeople = this.data.replyPeople; // 回复的人 拼接后的
      if (!replyContent) {
        wx.showToast({
          title: '请输入内容',
          icon: 'none',
        })
        return false;
      }

      console.log('回复内容:', replyContent);

      console.log('拼接后的回复人:', replyPeople);
      var isReply = replyContent.indexOf(replyPeople); // 是否为回复人或者为直接发布
      console.log('是否在:', replyContent.indexOf(replyPeople));
      var replyId = ''; // 评论指定id
      if (isReply == -1) {
        console.log('普通回复');
      } else {
        console.log('回复指定id', this.data.replyid);
        replyId = this.data.replyid;

        replyContent = replyContent.replace(replyPeople, '');
      }

      console.log('exampleId:', exampleId);
      console.log('content:', replyContent);
      console.log('replyId:', replyId);

      var that = this;
      wx.request({
        url: app.globalData.baseUrl + 'doctor/docExample/comment',
        data: {
          exampleId: exampleId,
          content: replyContent,
          replyId: replyId,
        },
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
        },
        method: 'POST',
        success(res) {
          console.log('回复结果:', res)
          if (res.data.status_code == 200) {
            that.setData({
              'pageNum': 1,
            })
            that.getCaseComment(that.data.caseid);
            that.loadCaseDetail(that.data.caseid);
            // that.pageScrollToBottom(); // 页面滚动到底部
            that.setData({
              'replyPeople': '',
              'replyid': '',
              'replyContent': '',
            })
          }
        }
      })
    }
  },



  // 使页面滚动到底部
  pageScrollToBottom: function() {
    wx.createSelectorQuery().select('#j_page').boundingClientRect(function(rect) {
      console.log('底部：', rect);
      // 使页面滚动到底部
      wx.pageScrollTo({
        scrollTop: rect.height
      })
    }).exec();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    wx.showLoading({
      title: '加载中...',
    })
    this.setData({
      pageNum: this.data.pageNum + 1,
      'more': 1,
    })
    console.log('页码:', this.data.pageNum)
    this.getCaseComment(this.data.caseid);
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    return {
      title: '分享好友获取美丽币！',
      path: '/pages/index/index',
      // imageUrl: '/pages/images/aa.jpg',
      success: function() {
        wx.request({
          url: app.globalData.baseUrl + 'doctor/memberTaskApi/addBeauty',
          data: {
            userId: wx.getStorageSync('userid'),
            type: 3
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('分享给美丽币给美丽值', res);
          }
        })
      }
    }
  },
  //数组图片放大
  imgYu: function (event) {
    console.log('assacsa', event.currentTarget.dataset)
    var src = event.currentTarget.dataset.src; //获取data-src
    var imgList = event.currentTarget.dataset.list; //获取data-list
    console.log('imgList', imgList[0].imgPath)

    var imgUrl = app.globalData.imgUrl;

    var a = [];
    //获取下标
    var o = event.currentTarget.dataset.inde;
    // 循环 list数组
    for (var s in imgList) a.push(imgUrl + imgList[s].imgPath);

    // a.push(imgUrl + imgLists);
    //图片预览
    console.log('yaokan1', imgUrl + imgList[o].imgPath);
    console.log('yaokan2', a);
    wx.previewImage({
      current: imgUrl + imgList[o].imgPath, // 当前显示图片的http链接
      urls: a// 需要预览的图片http链接列表
    })
  },
})