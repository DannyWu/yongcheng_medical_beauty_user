// pages/user-my-kf/user-my-kf.js
//获取应用实例
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    doctorId:'',
    problemType:[],
    currentType:'',
    problemList:[],
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      doctorId: options.doctorId
    });
    this.getProblemTypes();
    this.getProblemByType();

  },

  //点击切换
  changeType: function (e) {
    var that = this;
    if (this.data.currentType === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentType: e.target.dataset.current 
      });
      that.getProblemByType();
    }
  },
  //获取问题类型
  getProblemTypes:function(){
    const t = this;
    app.request({
      url:'consult/problemapi/getProblemType',
      success: function (res) {
        const { data } = res;
        t.setData({
          problemType: JSON.parse(JSON.stringify(data))
        });
      },
      fail: function (res) {
      }
    })
  },

  getProblemByType: function () {
    const t = this;
    app.request({
      url: 'consult/problemapi/getProblemByTypeId',
      data:{
        typeId:t.data.currentType
      },
      success: function (res) {
        const { data } = res;
        t.setData({
          problemList: JSON.parse(JSON.stringify(data))
        });
      },
      fail: function (res) {
      }
    })
  }
})