var app = getApp();
Page({
  data: {
    pagenum: 1,
    pagesize: 5,
    more: 0,
  },

  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数
    this.setData({
      'goodsid': options.id,
    })

    this.loadData(this.data.goodsid);
  },

  // 加载数据
  loadData: function(goodsid) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'shopp/goodsCommentApi/getServiceGoodsCommentPage',
      data: {
        goodsid: goodsid, // 产品id
        pagenum: that.data.pagenum, // 页码
        pagesize: that.data.pagesize, // 每页个数
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },
      method: 'POST',
      success: function(res) {
        console.log(res);

        if (res.data.data) {
          if (that.data.more == 0) {
            that.setData({
              'info': res.data.data,
            })
          } else {
            wx.hideLoading();
            that.setData({
              more: 0,
            })
            if (res.data.data.length) {
              var info = that.data.diaryinfo.concat(res.data.data);
              that.setData({
                'info': info,
              })
            } else {
              wx.showToast({
                title: '已无更多...',
                icon: 'none',
                duration: 2000,
              });
            }
          }
        }


      }
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    wx.showLoading({
      title: '加载中...',
    })
    this.setData({
      'more': 1,
      'pagenum': this.data.pagenum + 1,
    });
    console.log('页码:', this.data.pagenum)
    this.loadData(this.data.goodsid);
  },


})