// pages/user-shop/user-shop.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isShowCity: false, // 是否展开城市列表
    productInfo: [],
    imgUrl: 'https://tyc-bj.cn/yc/api/attach/',
    imgadinfo: [],
    hottype: [],// 热门分类
    searchPlaceholder: '请输入商品信息',
    operationgoodsnum: 6,
    yimeigoodsnum: 3,
    hidecity: true, // 是否隐藏城市列表
    hidearea: true, // 是否隐藏区域列表
    hidesort: true, // 是否隐藏只能排序
    hidetype: false,
    isBindCity: false, // 是否点过城市
    isBindSory: false, // 是否点过智能排序
    isAddTopCss: false, // 是否加top 样式
    url: 'https://tyc-bj.cn/yc/api/attach/images/top-cart.png',//搜索框右侧图片路径
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
  //所在位置
    var provinceid = wx.getStorageSync('provinceid');
    var provincename = wx.getStorageSync('provincename');
    var cityid = wx.getStorageSync('cityid');
    var cityname = wx.getStorageSync('cityname');
    console.log('已选省和市id:', provinceid, cityid);
    var opencityid = wx.getStorageSync('opencityid');
    var opencityname = wx.getStorageSync('opencityname');
    var openareaid = wx.getStorageSync('openareaid');
    var openareaname = wx.getStorageSync('openareaname');
    console.log('已选区名:', openareaname);
    console.log('已选市id:', openareaid);
    if (openareaname) {
      this.setData({
        'areaname': cityname,
        'cityid': cityid,
        'opencityid': openareaid,
        'opencityname': openareaname,
      })
    } else {
      this.setData({
        'areaname': cityname,
        'cityid': cityid,
        'opencityid': cityid,
        'opencityname': cityname,
      })
    };
    this.loadProduct();
    this.loadImgAd();
    this.shopHotTypes();
  },
  /**
     * 生命周期函数--监听页面隐藏
     */
  // 离开页面关闭城市列表
  onHide: function () {
    
  },
//搜索右侧购物车按钮路径
  adddiary:function(){
    wx.navigateTo({
      url: '../user-my-cart/user-my-cart',
    })
  },
  // 加载术前术后,医美产品
  loadProduct: function() {
    var that = this;
    var cityid = wx.getStorageSync('cityid');
    wx.request({
      url: app.globalData.baseUrl + 'shopp/mallApi/indexShow?operationgoodsnum=' + that.data.operationgoodsnum + '&yimeigoodsnum=' + that.data.yimeigoodsnum + '&cityid=' + cityid, //仅为示例，并非真实的接口地址
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      success(res) {
        console.log('商城首页数据:', res.data.data)
        that.setData({
          productInfo: res.data.data,
        })
      }
    })
  },
  // 加载图片广告
  loadImgAd: function (e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'system/advertise/getAdvertise',
      data: {
        'type': 0,
        'adType': 'shop',
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('图片广告:', res.data.data)
        var tempData = res.data.data;
        for (var i = 0; i < tempData.length; i++) {
          if (tempData[i].srcType == 1) {
            tempData[i].srcUrl = '../user-shop-goods-details/user-shop-goods-details?etype=1&id=' + tempData[i].srcId;
          } else if (tempData[i].srcType == 2) {
            tempData[i].srcUrl = '../user-shop-goods-details/user-shop-goods-details?etype=2&id=' + tempData[i].srcId;
          }
        }
        that.setData({
          'imgadinfo': res.data.data,
        })
      }
    })
  },
  // 商城热门分类
  shopHotTypes: function (e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'shopp/goodsTypeApi/getHotTypes?type=2',
      data: {},
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/json'
      },
      method: 'GET',
      success(res) {
        console.log("商城热门分类", res.data);
        that.setData({
          'hottype': res.data.data,
        })
      }
    })
  },
  // 商城首页搜索
  indexSearch: function (e) {
    console.log("商城首页搜索input:", e.detail.value)
    var keyword = e.detail.value;
    wx.navigateTo({
      url: '../user-search-goods/user-search-goods?keyword=' + keyword,
    });
  },
  showCity: function (e) {
    this.setData({
      isShowCity: !e.detail.isShowCity
    
    });
    this.getProvince();
    this.loadaddress();
  },
  


  // 离开页面关闭城市列表
  onHide: function () {
    this.setData({
      isShowCity: false,
      hidecity: true,
      hidearea: true,
      hidesort: true,
      hidetype: false,
    })
  },

  // 点击省
  bindProvince: function (e) {
    var provinceid = e.currentTarget.dataset.provinceid;
    var provincename = e.currentTarget.dataset.provincename;
    if (provinceid != this.data.provinceid) {
      this.setData({
        'provinceid': provinceid,
        'provincename': provincename,
      });
      console.log('省id', provinceid)
      var area = [provinceid, 2, 1];
      this.getCity(area);

    } else {
      return false;
    }
  },

  // 点击市
  bindCity: function (e) {
    var cityid = e.currentTarget.dataset.cityid;
    var cityname = e.currentTarget.dataset.cityname;
    if (cityid != this.data.cityid) {
      this.setData({
        'cityid': cityid,
        'areaname': cityname,
      });
      wx.setStorageSync('cityid', cityid);
      wx.setStorageSync('cityname', cityname);
      wx.setStorageSync('provinceid', this.data.provinceid);
      wx.setStorageSync('provincename', this.data.provincename);
      this.setData({
        isShowCity: false,
        'hidetype': false,
      })
    } else {
      this.setData({
        isShowCity: false,
        'hidetype': false,
      })
      return false;
    }
  },
  // 获取省
  getProvince: function (e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/city/getCityList',
      data: {
        id: '',
        level: '',
        currentLevel:'',
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('省列表:', res)
        that.setData({
          'provincelist': res.data.data,
        })
      }
    })
  },

  // 获取市
  getCity: function (e) {
    console.log('要看',e);
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/city/getCityList',
      data: {
        id: e[0],
        level: e[1],
        currentLevel: e[2],
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('当前省的所有市列表:', res)
        var citylist = res.data.data;
        that.setData({
          'citylist': citylist,
        })
        var cityid = wx.getStorageSync('cityid');
        if (cityid) {
          for (var i = 0; i < citylist.length; i++) {
            if (citylist[i]["cityId"] == cityid) {
              that.setData({
                areaname: citylist[i]["cityName"],
              })
            }
          }
          wx.setStorageSync('cityname', that.data.areaname);
        }
      }
    })
  },
  //获取当前位置
  loadaddress: function () {
    var that = this;
    wx.getLocation({
      type: 'wgs84',
      success(res) {
        console.log("定位", res)
        var latitude = res.latitude;
        var longitude = res.longitude;
        wx.setStorageSync('latitude', latitude)
        wx.setStorageSync('longitude', longitude)
        var location = latitude + "," + longitude;
        console.log("location", location)
        wx.request({
          url: app.globalData.baseUrl + 'doctor/city/getAreaByLocation',
          data: {
            'location': location
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('当前位置省id:', res.data.data.proId);
            console.log('当前位置市id:', res.data.data.cityId);
            var proId = res.data.data.proId;
            var cityId = res.data.data.cityId;
            that.setData({
              provinceid: proId,
              cityid: cityId,
            })
            wx.setStorageSync('provinceid', proId);
            wx.setStorageSync('cityid', cityId);
            var area = [proId, 2, 1];
            that.getCity(area);
          }
        })
      }
    })
  },

  tapIcon: function(e) {
    wx.navigateTo({
      url: '/pages/doctor-city-map/doctor-city-map'
    });
  }
})