// pages/user-my-order-details-medical-3/user-my-order-details-medical-3.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: { 
    richeng:'',//日程数据
    time:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    this.service(options)
  },
  //待服务查看日程
  service: function (e) {
    console.log('===qwe', e)
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'citycore/orderSchedule/getOrderSchedule',
      data: {
        orderId: e.orderId
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log("查看日程", res.data)

        that.setData({
          richeng:res.data.data,
          time: app.timeData(res.data.data.useTime),
        })

      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})