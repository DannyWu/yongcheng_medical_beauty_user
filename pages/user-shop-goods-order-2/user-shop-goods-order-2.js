// pages/user-shop-goods-order-2/user-shop-goods-order-2.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    prePage:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let pages = getCurrentPages();
    let pageIndex = pages.findIndex(function(item,index){
      if (item.route == "pages/user-shop-goods-medical/user-shop-goods-medical" || item.route == "pages/user-shop-goods/user-shop-goods"){
        return true;
      }
    });
    this.setData({
      prePage: pages.length - 1 - pageIndex
    });
  },
  //f返回商城
  returnstore: function() {

    var that = this;
 
    wx.navigateBack({
      delta: this.data.prePage
    });

  }, 
  
  returnorder: function() {
    wx.setStorageSync('orderJump', true);
    wx.switchTab({
      url: '/pages/user-my/user-my',
    });
  }
})