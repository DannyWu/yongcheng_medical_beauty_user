64 // pages/my/my.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    core: '', //信息
    id: "",
    imgUrl: 'https://tyc-bj.cn/yc/api/attach/',
    take: !0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    
  },
  loadinds: function() {
    var isLogin = wx.getStorageSync('isLogin');
    var roleType = wx.getStorageSync('roleType');
    console.log('是否登陆isLogin', isLogin);
    if (isLogin != 1) {
      wx.redirectTo({
        url: '/pages/user-login/user-login',
      });
    }
    var userInfo = wx.getStorageSync('userInfo');
    console.log('456', userInfo);
    this.setData({
      url: app.globalData.imgUrl,
      userInfo: userInfo
    })
    var that = this;
    //获取用户id
    var userid = wx.getStorageSync('userid');
    that.setData({
      id: userid
    })
    wx.request({
      url: app.globalData.baseUrl + 'doctor/member/detail',
      data: {},
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },
      method: 'POST',
      success: function(t) {
        console.log("个人中心1", t);
        if (t.data.status_code == 200002) {
          return false;
        } else {
          wx.hideLoading();
          var headPic = t.data.data[0].headPic;
          if (headPic.substr(0, 4) == 'http') {
            console.log("111:", 111);
            that.setData({
              take: !0
            })
          } else {
            console.log("222:", 222);
            that.setData({
              take: !1
            })
          }
          that.setData({
            core: t.data.data[0],
            name: t.data.data[0].name,
            headPic: t.data.data[0].headPic,
            mobile: t.data.data[0].mobile,
          })
        }
      }
    })
  },

  gosystem: function() {
    var mobile = this.data.mobile;
    console.log('mobile', mobile);
    wx.navigateTo({
      url: '../user-my-setup/user-my-setup?mobile=' + mobile,
    })
  },

  //收藏
  collect: function() {
    var id = this.data.id
    var isLogin = wx.getStorageSync('isLogin');
    if (isLogin != 2) {
      wx.navigateTo({
        url: '../user-my-collect/user-my-collect?id=' + id,
      })
    } else {
      wx.redirectTo({
        url: '/pages/user-login/user-login',
      });
    }
  },
  // 客服
  service: function() {
    var isLogin = wx.getStorageSync('isLogin');
    if (isLogin != 2) {
      wx.navigateTo({
        // url: '../doctor-authentication-1/doctor-authentication-1',
        url: '../user-my-kf/user-my-kf',
      })
    } else {
      wx.redirectTo({
        url: '/pages/user-login/user-login',
      });
    }
  },

  //跳转
  jop1: function() {
    wx.navigateTo({
     url: '../user-my-rank/user-my-rank',
      
    });
  },
  jop2: function() {
    var userid = wx.getStorageSync('userid');
    wx.navigateTo({
      url: '../user-my-task/user-my-task?id=' + userid,
    })
  },

  handleContact(e) {
    console.log('客服消息')
    console.log(e);
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (wx.getStorageSync('orderJump') == true) {
      wx.removeStorageSync('orderJump');
      wx.navigateTo({
        url: '/pages/user-my-order-list/user-my-order-list?type=2',
      });
    }
    this.loadinds();
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '分享好友获取美丽币！',
      path: '/pages/index/index',
      // imageUrl: '/pages/images/aa.jpg',
      success: function () {
        wx.request({
          url: app.globalData.baseUrl + 'doctor/memberTaskApi/addBeauty',
          data: {
            userId: wx.getStorageSync('userid'),
            type: 3
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('分享给美丽币给美丽值', res);
          }
        })
      }
    }
  }
})