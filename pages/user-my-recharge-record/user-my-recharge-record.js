// pages/user-my-recharge-record/user-my-recharge-record.js
var app= getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageSize:20,//每页
    pageNum:1, //页
    record:"",//记录数据
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
    this.select();//查询充值记录
  },
  //查询充值记录
  select:function(){
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'shopp/rechrageApi/queryRechargeOrder', //仅为示例，并非真实的接口地址
      data: {
        userId:wx.getStorageSync('userid'),
        pageSize: that.data.pageSize,
        pageNum: that.data.pageNum,
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log("充值记录", res)
        that.setData({
          record:res.data.data
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})