// pages/user-diary-book-add-1/user-diary-book-add-1.js
var app = getApp()
Page({  
  data: {
    currentTab: 0,
    firsttype: 1, // 一级分类id
    firsttypelist: [], // 一级分类列表
    secondtype: 1, // 二级分类
    thirdtype: 1, // 三级分类
    currentTab: 0,
    choosed: [], // 已选标签列表
    choosedid: [], // 已选id列表
    childtypelist: [],
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    this.oneTypeServiceList();
  },
 
  // 一级服务产品类别列表
  oneTypeServiceList: function (e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'shopp/goodsTypeApi/getPage',
      data: {
        'type': 2,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },
      method: 'POST',
      success: function (res) {
        console.log('服务类:', res)
        var firstName = res.data.data[1].name;
        var firstTypeId = res.data.data[1].id;
        var choosed = that.data.choosed;
        var choosedid = that.data.choosedid;
        choosed[0] = firstName;
        choosedid[0] = firstTypeId;
        that.setData({
          'firsttypelist': res.data.data.slice(1),
          'choosed': choosed,
          'choosedid': choosedid,
          'firsttype': firstTypeId,
        });
        that.loadTwoAndThreeType(firstTypeId);
      }
    })
  },

  // 加载二三级分类
  loadTwoAndThreeType: function (pid) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'shopp/goodsTypeApi/getAllSonsByParent',
      data: {
        'parentid': pid,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },
      method: 'POST',
      success: function (res) {
        console.log('二级三级分类:', res)
        var choosed = that.data.choosed;
        var choosedid = that.data.choosedid;
        choosed[1] = res.data.data[0].name;
        choosed[2] = res.data.data[0].sons[0].name; // 名字赋值
        choosedid[1] = res.data.data[0].id;
        choosedid[2] = res.data.data[0].sons[0].id; // id存储
        that.setData({
          'childtypelist': res.data.data,
          'secondtype': res.data.data[0].id,
          'thirdtype': res.data.data[0].sons[0].id,
          'choosed': choosed,
          'choosedid': choosedid,
        })
      }
    })
  },

  // 点击一级分类
  clickTabOne: function (e) {
    var firsttype = e.currentTarget.dataset.firsttype;
    var firsttypename = e.currentTarget.dataset.firsttypename;
    if (firsttype != this.data.firsttype) {
      var choosed = this.data.choosed;
      var choosedid = this.data.choosedid;
      choosed[0] = firsttypename; // 标签赋值
      choosedid[0] = firsttype; // id存储
      this.setData({
        'firsttype': firsttype,
        // 'currentTab': firsttype - 1,
        'choosed': choosed,
        'choosedid': choosedid,
      });
      this.loadTwoAndThreeType(firsttype);
    } else {
      return false;
    }
  },

  // 点击二级分类
  clickTabTwo: function (e) {
    var secondtype = e.currentTarget.dataset.secondtype;
    if (secondtype != this.data.secondtype) {
      var choosed = this.data.choosed;
      choosed[1] = secondtype;
      this.setData({
        'secondtype': secondtype,
        'choosed': choosed,
      });
    } else {
      return false;
    }
  },

  // 点击三级分类
  clickTabThree: function (e) {
    var secondtype = e.currentTarget.dataset.secondtype;
    var thirdtype = e.currentTarget.dataset.thirdtype;
    var secondtypename = e.currentTarget.dataset.secondtypename;
    var thirdtypename = e.currentTarget.dataset.thirdtypename;
    if (thirdtype != this.data.thirdtype) {
      var choosed = this.data.choosed;
      var choosedid = this.data.choosedid;
      choosed[1] = secondtypename;
      choosed[2] = thirdtypename; // 赋值名字
      choosedid[1] = secondtype;
      choosedid[2] = thirdtype; // 存储id
      this.setData({
        'thirdtype': thirdtype,
        'choosed': choosed,
        'choosedid': choosedid,
      });
    } else {
      return false;
    }
  },

  // 下一页
  nextPage: function (e) {
    // var choosed = JSON.stringify(this.data.choosed);
    wx.navigateTo({
           
      url: '../user-diary-book-add-2/user-diary-book-add-2?choosed=' + this.data.choosed + '&choosedid=' + this.data.choosedid,
    })
  },

  //滑动切换
  swiperTab: function (e) {
    var that = this;
    that.setData({
      currentTab: e.detail.current
    });
  },
  //点击切换
  clickTab: function (e) {
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  }
})
