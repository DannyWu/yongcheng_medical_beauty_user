 // pages/user-diary-index/user-diary-index.js
var app = getApp();
Page({
  /** 
   * 页面的初始数据
   */
  data: {

    pageSize: 30, //每页条数
    pageNum: 1, // 页数 
    sort: 0,
    currentTab: 0, 
    iscity: true, //城市
    xiangmu: true, // 选择项目
    riji: true, //日记情况
    arraydata: "",
    diaryinfo: "",
    imgUrl: "https://tyc-bj.cn/yc/api/attach/",

    firsttype: '4799b11d4ca749d0a712aba0053c591d', // 一级
    secondtype: '', // 二级
    thirdtype: 1, // 三级
    firsttypelist: [], // 一级分类列表
    choosed: [], // 已选标签列表
    choosedid: [], // 已选id列表
    opencitylist: [],
    openarealist: [],
    openareaid: '',
    opencityname: '全部城市', // 已开通的城市名称  
    areaid: '',
    opencityid: "", //城市ID
    noteType: 1, //日记类型(1最热,2最新回复,3最新日记)
    diary: "最热日记",
    project: "全部项目",
    cityId: '',
    more:'0',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    that.loadDiary();
    that.oneTypeServiceList(); //一级类
    that.getAllCity();
  },


  //列表
  loadDiary: function() {
    var that = this;
    if (that.data.secondtype == "") {
      that.setData({
        firsttype: "4799b11d4ca749d0a712aba0053c591d",
      })
    }
    console.log('--cityId--', that.data.opencityid, ) //城市ID
    console.log('--oneType--', that.data.firsttype, ) //一级类型ID
    console.log('--twoType--', that.data.secondtype, ) //二级类型ID    
    console.log('--noteType--', that.data.noteType, ) //日记类型
    console.log('--pageSize--', that.data.pageSize, ) //日记类型    
    console.log('--pageNum--', that.data.pageNum, ) //日记类型
 
    wx.request({
      url: app.globalData.baseUrl + 'doctor/diary/getPage',
      data: {
        'cityId': that.data.opencityid, //城市ID
        'oneType': that.data.firsttype, //一级类型ID
        'twoType': that.data.secondtype, //二级类型ID
        'noteType': that.data.noteType, //日记类型
        'pageSize': that.data.pageSize,
        'pageNum': that.data.pageNum,
      },
      header: { 
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('首页进入日记列表:', res.data)
        wx.hideLoading();
        if (res.data.data) {
          if (that.data.more == 0) {
            that.setData({
              'diaryinfo': res.data.data,
            })
          } else {
            that.setData({
              more: 0,
            })
            if (res.data.data.length) {

              var info = that.data.caseinfo.concat(res.data.data);
              that.setData({
                'caseinfo': info,
              })
            } else {
              wx.showToast({
                title: '已无更多...',
                icon: 'none',
                duration: 2000,
              });
            }
          }
        }
        // that.setData({
        //   diaryinfo: res.data.data,
        // })
      }
    })
  },

  // 一级服务产品类别列表1
  oneTypeServiceList: function(e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'shopp/goodsTypeApi/getPage',
      data: {
        'type': 2,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },
      method: 'POST',
      success: function(res) {
        console.log('一级类:', res)
        var firstName = res.data.data[1].name;
        var firstTypeId = res.data.data[1].id;
        var choosed = that.data.choosed;
        var choosedid = that.data.choosedid;
        choosed[0] = firstName;
        choosedid[0] = firstTypeId;
        wx.setStorageSync('firsttype', firstTypeId);
        that.setData({
          'firsttypelist': res.data.data.slice(1),
          'choosed': choosed,
          'choosedid': choosedid,
          'firsttype': firstTypeId,
        });
        that.loadTwoAndThreeType(firstTypeId);
      }
    })
  },

  // 加载二三级分类 2,3
  loadTwoAndThreeType: function(pid) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'shopp/goodsTypeApi/getAllSonsByParent',
      data: {
        'parentid': pid,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },
      method: 'POST',
      success: function(res) {
        console.log('二级三级分类:', res)
        var choosed = that.data.choosed;
        var choosed = that.data.choosed;
        var choosedid = that.data.choosedid;
        choosed[1] = res.data.data[0].name;
        choosed[2] = res.data.data[0].sons[0].name; // 名字赋值
        choosedid[1] = res.data.data[0].id;
        choosedid[2] = res.data.data[0].sons[0].id; // id存储
        that.setData({
          'childtypelist': res.data.data,
          // 'secondtype': res.data.data[0].id,
          // 'twoType': res.data.data[0].sons[0].id,
          'choosed': choosed,
          'choosedid': choosedid,
        })
      }
    })
  },

  // 点击一级分类
  clickTabOne: function(e) {
    var firsttype = e.currentTarget.dataset.firsttype;
    var firsttypename = e.currentTarget.dataset.firsttypename;
    // if (firsttype != this.data.firsttype) {
    var choosed = this.data.choosed;
    var choosedid = this.data.choosedid;
    choosed[0] = firsttypename; // 标签赋值
    choosedid[0] = firsttype; // id存储
    this.setData({
      'firsttype': firsttype,
      // 'currentTab': firsttype - 1,
      'choosed': choosed,
      'choosedid': choosedid,

    });
    this.loadTwoAndThreeType(firsttype);
    // } else {
    //   return false;
    // }
  },

  // 点击二级分类
  clickTabTwo: function(e) {
    console.log('点击二级分类', e);
    var secondtype = e.currentTarget.dataset.secondtype;
    var secondtypename = e.currentTarget.dataset.secondtypename;
    // var twoType = e.currentTarget.dataset.twotype;
    // if (secondtype != this.data.secondtype) {
    var choosed = this.data.choosed;
    choosed[1] = secondtype;
    this.setData({
      // 'twoType': twoType,
      'secondtype': secondtype,
      'choosed': choosed,
      project: secondtypename,
      xiangmu: true,
    });
    console.log('twotype666', this.data.twotype);
    this.loadDiary(); //加载日记
    // } else {
    //   return false;
    // }
  },

  // 点击三级分类
  clickTabThree: function(e) {
    console.log('点击三级分类:', e);
    var secondtype = e.currentTarget.dataset.secondtype;
    var thirdtype = e.currentTarget.dataset.thirdtype;
    var secondtypename = e.currentTarget.dataset.secondtypename;
    var thirdtypename = e.currentTarget.dataset.thirdtypename;
    if (thirdtype != this.data.thirdtype) {
      var choosed = this.data.choosed;
      var choosedid = this.data.choosedid;
      choosed[1] = secondtypename;
      choosed[2] = thirdtypename; // 赋值名字
      choosedid[1] = secondtype;
      choosedid[2] = thirdtype; // 存储id
      this.setData({
        'thirdtype': thirdtype,
        'choosed': choosed,
        'choosedid': choosedid,
        project: thirdtypename,
        // twoType: thirdtype,        
        xiangmu: true,
      });
      console.log('---cesji di ---', this.data.firsttype)
      this.loadDiary(); //加载日记
    } else {
      return false;
    }
  },

  // 获取所有城市列表
  getAllCity: function(e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/city/getCityListForDoctor',
      data: {
        type: 2,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('全部已开通城市:', res)
        that.setData({
          'opencitylist': res.data.data,
          // 'opencityid': res.data.data[0].cityId,
          // 'cityid': res.data.data[0].cityId,
        });
      }
    })
  },

  // 点击城市
  bindOpenCity: function(e) {
    console.log('--点击城市---', e)
    var opencityid = e.currentTarget.dataset.opencityid;
    var opencityname = e.currentTarget.dataset.opencityname;
    console.log('opencityid', this.data.opencityid);
    if (this.data.opencityid == "") {
      this.setData({
        'opencityid': opencityid,
        'opencityname': opencityname,
        iscity: true,
      });
      this.loadDiary(); //加载日记
    } else {
      if (opencityid != this.data.opencityid) {
        this.setData({
          'opencityid': opencityid,
          'opencityname': opencityname,
          iscity: true,
        });
        this.loadDiary(); //加载日记
      } else {
        this.setData({
          iscity: true,
        });
        this.loadDiary(); //加载日记
      }
    }
  },
  // 点击区
  bindArea: function(e) {
    var that = this;
    var openareaid = e.currentTarget.dataset.openareaid;
    var openareaname = e.currentTarget.dataset.openareaname;
    if (openareaid != that.data.openareaid) {
      that.setData({
        'openareaid': openareaid, ////11231
        'areaid': openareaid,
        'opencityname': openareaname,
        isShowCity2: false,
        'hidesort': true,
        'hidetype': false,
        'iscity': true,
      });
      that.loadDiary(); //加载日记
    } else {
      that.setData({
        isShowCity2: false,
        'hidecity': false,
        'hidesort': true,
        'hidetype': false,
      })
      return false;
    }
  },

  // 加载区
  getArea: function(e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/city/getCityList',
      data: {
        id: e[0],
        level: e[1],
        currentLevel: e[2],
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('当前市的区:', res)
        var openarealist = res.data.data;
        that.setData({
          'openarealist': openarealist,
          'openareaid': res.data.data[0].cityId,
          'areaid': res.data.data[0].cityId,
        })

      }
    })
  },

  // 点击切换
  clickTab: function(e) {
    console.log("变更", e)
    var currents = e.currentTarget.dataset.currents;
    var diary = e.currentTarget.dataset.name;
    if (this.data.currents === e.target.dataset.currents) {
      // return false;
    } else {
      this.setData({
        currentTabs: e.target.dataset.currents,
        noteType: e.target.dataset.notetype,
        diary: diary,
        riji: !this.data.riji
      })
      this.loadDiary(); //加载日记
    }
  },

  // 关注按钮
  bindFollow: function(e) {
    console.log('---ceshi--', e.currentTarget.dataset)
    var isLogin = wx.getStorageSync('isLogin');
    if (isLogin == 2) {
      wx.showModal({
        title: '提示',
        content: '请先登陆!',
        success: function(res) {
          if (res.confirm) {
            wx.redirectTo({
              url: '/pages/user-login/user-login',
            });
          } else {}
        }
      })
    } else { 
      var userid = e.currentTarget.dataset.userid;
      var isfollow = e.currentTarget.dataset.isfollow;
      var ziji = wx.getStorageSync('userid');
      console.log('关注:userid', userid);
      console.log('关注:isfollow', isfollow);      
      var that = this;
      if(userid == ziji){
        wx.showToast({
          title: '不能关注自己',
          icon: 'success',
          duration: 1000
        })

      } else if(isfollow == 1) {
        
          wx.request({
            url: app.globalData.baseUrl + 'doctor/member/attention',
            data: {
              userId: userid,
            },
            header: {
              'sign': app.globalData.sign,
              'token': app.globalData.token,
              'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
            },
            method: 'POST',
            success(res) {
              console.log('关注结果:', res)
              wx.showToast({
                title: '取消关注',
                icon: 'success',
                duration: 1000
              })
              that.loadDiary();
            }
          })
        } else if (isfollow == 2) {
          wx.request({
            url: app.globalData.baseUrl + 'doctor/member/attention',
            data: {
              userId: userid,
            },
            header: {
              'sign': app.globalData.sign,
              'token': app.globalData.token,
              'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
            },
            method: 'POST',
            success(res) {
              console.log('关注结果:', res)
              wx.showToast({
                title: '关注成功',
                icon: 'success',
                duration: 1000
              })
              that.loadDiary();
            }
          })
        } else {}
      

    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // this.getCase();
    this.setData({
      'more': 0,
      'pageNum': 1,
    })
    this.oneTypeServiceList();

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },
 

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    wx.showLoading({
      title: '加载中...',
    })
    this.setData({
      'more': 1,
      'pageNum': this.data.pageNum + 1,
    });
    console.log('页码:', this.data.pageNum)
    this.loadDiary();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  

  // 选择城，市项目，日记
  xuanzhong: function(e) {
    var that = this;
    // console.log('firsttype999', wx.getStorageSync('firsttype'));
    // that.setData({
    //   firsttype: wx.getStorageSync('firsttype')
    // })
    console.log(e)
    var sort = e.currentTarget.dataset.sort;
    console.log('e1', sort);
    console.log('e2', e.target.dataset.sort);
    if (this.data.sort === e.target.dataset.sort) {
      // return false;
    } else {
      this.setData({
        sort: e.target.dataset.sort
      })
    }
    if (e.target.dataset.sort == 1) {
      this.setData({
        iscity: !this.data.iscity,
        xiangmu: true,
        riji: true,
      })
    }

    if (e.target.dataset.sort == 2) {
      this.setData({
        xiangmu: !this.data.xiangmu,
        iscity: true,
        riji: true,
      })
    }
    if (e.target.dataset.sort == 3) {
      this.setData({
        riji: !this.data.riji,
        iscity: true,
        xiangmu: true
      })
    }
  }
})