// pages/user-my-movie/user-my-movie.js
var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    sheetTypeList:[],
    mySheetList:[],
    imgUrl: "https://tyc-bj.cn/yc/api/attach/"
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getSheetType();
    this.getSheetChange();
    console.log(wx.getStorageSync('userid'))
  },
  getSheetType: function(){
    const t = this;
    app.request({
      url: 'doctor/memberchange/getSheet',
      success: function (res) {
        t.setData({
          sheetTypeList: res.data.list
        });
      },
      fail: function (res) {
        //app.requestFail(res);
      }
    });
  },
  getSheetChange: function(){
    const t = this;
    app.request({
      url: 'doctor/memberchange/getSheetChange',
      data: {
        userId:wx.getStorageSync('userid')
      },
      success: function (res) {
        console.log(res.data);
        t.setData({
          mySheetList: res.data
        });
      },
      fail: function (res) {
        //app.requestFail(res);
      }
    });
  },
  navigateToDetail: function(e){
    let item = e.currentTarget.dataset.item;
    if(item.state == 1) {
      wx.navigateTo({
        url: `../user-my-movie-details/user-my-movie-details?changeNumber=${item.changeNumber}`,
      });
    }
    else{
      wx.showToast({
        title: '请先联系客服上传视频',
        icon: 'none',
        duration: 2000
      })
    }
  }
})