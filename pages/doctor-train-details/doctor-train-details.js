// pages/doctor-train-details/doctor-train-details.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgBase: app.globalData.imgUrl,
    datail: {},
    time: '',
    name: '',
    current: 1,       // 课程tab切换
    courseList: [],   // 课程视频列表
    page: 1,          // 当前页数
    pagesize: 10,     // 每页条数
    id: '',
    status: '200'         // 200是201否收藏
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    app.showLoading();
    const that = this;
    const obj = JSON.parse(options.obj);
    console.log(obj)
    const name = obj.classify;
    let param = {};
    param[name] = obj.id;
    this.setData({
      name,
      id: obj.id,

    })
    // 获取详情数据
    if ('activityId' == name) {
      this.getDetail('train/activity/getActivityDetail', param)
    } else if ('academicId' == name) {
      this.getDetail('train/academic/getAcademicDetail', param)
    } else if ('courseId' == name) {
      this.getDetail('train/course/getCourseDetail', param)
    } else if ('itemId' == name) {
      this.getDetail('train/item/getBrandItemDetail', param)
    }
  },

  getDetail(url, param) {
    const that = this;
    app.request({
      url: url,
      data: param,
      success: function(res) {
        const detail = res.data;
        let time = '';
        if (detail.activityDate) {
          time = app.timeData(detail.activityDate)
        }
        if (detail.createTime) {
          time = app.timeData(detail.createTime)
        }
        that.setData({
          detail,
          time
        })
        wx.hideLoading();
        that.videoContext = wx.createVideoContext('myVideo')
      },
      fail: function(res) {
        // app.requestFail(res);
      }
    });
  },
  //收藏
  collectTrain(e) {
    const collectType = e.currentTarget.dataset.type;
    const trainId = e.currentTarget.dataset.id;
    const that = this;
    const param = {
      trainId: this.data.id,
      collectType
    };
    app.request({
      url: "doctor/doctor/collectTrain",
      data: param,
      success: function(res) {
        that.setData({
          status: res.data
        })
        wx.showToast({
          title: res.message,
          icon: 'success',
          duration: 2000
        })
      

      },
      fail: function(res) {
        // app.requestFail(res);
      }
    });
  },
  // 课程计划
  coursePlain() {
    const that = this;
    const param = {
      courseId: this.data.id,
      page: this.data.page,
      pagesize: this.data.pagesize
    };
    app.request({
      url: "train/video/getCourseVideoList",
      data: param,
      success: function(res) {
        let data = res.data
        for (let i = 0; i < data.length; i++) {
          data[i].createTime = app.timeData(data[i].createTime)
        }
        that.setData({
          courseList: data
        })
      },
      fail: function(res) {
        // app.requestFail(res);
      }
    });
  },
  tabCurrent(e) {
    const current = e.currentTarget.dataset.index;
    current == '2' && this.coursePlain();
    this.setData({
      current
    })
  },
  //付费报名
  pay() {
    const consumerId = wx.getStorageSync('userid');
    const that = this;
    const param = {
      consumerId: consumerId,
      courseId: this.data.id,
      payType: 3
    };
    app.request({
      url: "train/course/generateCourseOrder",
      data: param,
      success: function(res) {
        res.code == 200 || that.postOrderid(res.orderId)
      },
      fail: function(res) {
        // app.requestFail(res);
      }
    });
  },

  //传订单id
  postOrderid(id) {
    let param = {
      orderid: id,
      payType: 2
    };
    app.request({
      url: "shopp/payment/requestPayment",
      data: param,
      success: function(res) {
        wx.requestPayment({
          'timeStamp': res.data.timeStamp,
          'nonceStr': res.data.nonceStr,
          'package': res.data.package,
          'signType': res.data.signType,
          'paySign': res.data.paySign,
          'success': function(res) {
            console.log(res)
            if (res.errMsg == "requestPayment:ok") {
              wx.reLaunch({
                url: '/pages/payResult/payResult'
              })
            }
          },
          'fail': function(res) {},
          'complete': function(res) {}
        })
      },
      fail: function(res) {}
    });
  },


  //听课
  entryVideo(e){
    if (this.data.detail.isBuy == 2 && this.data.detail.isFree == 1){
      app.showModal("请您先购买此课程")
      return
    }

    const path = e.currentTarget.dataset.path;
    wx.navigateTo({
      url: "../doctor-train-details-video/doctor-train-details-video?path=" + path 
    });
  },

  // 分享
  onShareAppMessage: function() {
    return {
      title: '分享好友获取美丽币！',
      path: '/pages/index/index',
      // imageUrl: '/pages/images/aa.jpg',
      success: function() {
        wx.request({
          url: app.globalData.baseUrl + 'doctor/memberTaskApi/addBeauty',
          data: {
            userId: wx.getStorageSync('userid'),
            type: 3
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('分享给美丽币给美丽值', res);
          }
        })
      }
    }

  }

})