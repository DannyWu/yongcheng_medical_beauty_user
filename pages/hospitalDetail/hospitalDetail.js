const app=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgBase: app.globalData.imgUrl,          // 线上图片
    localImg: app.globalData.staticimgUrl,  // 本地图片
    type: '',
    infor: {},
    siteId: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    this.setData({
      siteId: options.siteId,
      type: options.type
    })
    options.type == 1 && this.getHostailDetail();
    options.type == 2 && this.getdeviceDetail();
  },
  getHostailDetail(){
    const that = this;
    const param = {
      siteId: this.data.siteId
    }
    app.request({
      url: 'citycore/citycoreapi/siteDetail',
      data: param,
      success: function (res) {
        
        that.setData({
          infor: res.data,
          inforObj: res.data.sitePhotoList
        })
      },
      fail: function (res) {

      }
    });
  },
  getdeviceDetail() {
    const that = this;
    const param = {
      deviceId: this.data.siteId
    }
    app.request({
      url: 'citycore/deviceapi/deviceDetail',
      data: param,
      success: function (res) {
       
        that.setData({
          infor: res.data,
          inforObj: res.data.devicePhotoList
        })
      },
      fail: function (res) {

      }
    });
  }

})