// pages/user-my-order-details/user-my-order-details.js
var app = getApp();
Page({
 
  /**总价格
   * 页面的初始数据 
   */
  data: {
    orderid:"",//订单ID
    operation: "",//术前后详情数据//医美的详情数据
    imgUrl: "https://tyc-bj.cn/yc/api/attach/",//拼接图片路径
    orderType: "",//订单类型2医美1术前术后
    options:"",
  },
 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var orderid = options.id;//订单ID
    var orderType = options.orderType;//订单类型2医美1术前术后
    this.setData({
      orderid:orderid,
      orderType:orderType,
      options: options,
    })
    //获取订单详情判断
    if (orderType == 1){
      this.operation();// 获取用户订单详情  术前术后的
    }else{
      this.orderdetails();// 获取用户订单详情 医美的
    }
  },

  // 获取用户订单详情医美的
  orderdetails:function(){
    var that = this;
    // 获取用户订单详情
    wx.request({
      url: app.globalData.baseUrl + 'shopp/yimeiGoodsOrderApi/getOrderDetail',
      data: {
        orderid: that.data.orderid,//订单ID
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log("医美的订单详情", res.data.data)
        that.setData({
          operation:res.data.data
        })
      }
    }) 
  } ,

  // 获取用户订单详情医术前术后的
  operation : function () {
    var that = this;
    console.log("123qweqwe", that.data.orderid)
    // 获取用户订单详情
    wx.request({
      // url: app.globalData.baseUrl + '/shopp/goodsOrderApi/getOrderDetail/' + aa22adf7a20a4568b190cf73fc4c8093,
      url: app.globalData.baseUrl + '/shopp/goodsOrderApi/getOrderDetail/' + that.data.orderid,
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        // 'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
        'content-type': 'application/json'
      },
      success(res){
        console.log("术前术后订单详情", res.data.data)
        var operation = res.data.data
        var znum=0;
        for (var i = 0; i < res.data.data.goodsArray.length;i++){
          znum = znum + operation.goodsArray[i].number;
          
        }
        operation.znum=znum;
        that.setData({
          operation: operation
        })
      }
    })
  },
 
  //取消订单按钮 1是术前术后  2是医美
  qxorder: function (e) {
    console.log('取消id', e.currentTarget.dataset)
    var that = this;
    // var id = e.currentTarget.dataset.id;
    that.setData({
      id: e.currentTarget.dataset.sid
    })
    var ordertype = that.data.orderType;
    if (ordertype == 2) {
      //删除医美订单
      wx.request({
        url: app.globalData.baseUrl + 'shopp/yimeiGoodsOrderApi/cancelOrder',
        data: {
          id: that.data.id,
        },
        method: 'POST',
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
        },
        success(res) {
          console.log("删除", res)
          wx.showToast({
            title: "成功",
            icon: 'success',
            duration: 1000
          })
          that.onLoad(that.data.options); //从新加载订单

        }
      })
    } else if (ordertype == 1) {
      var that = this;
      console.log("---", that.data.id)
      //删除术前术后订单
      wx.request({
        url: app.globalData.baseUrl + 'shopp/goodsOrderApi/cancelOrder',
        data: {
          id: that.data.id,
        },
        method: 'POST',
        // method: 'GET', 
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
          // 'content-type': 'application/json'
        },
        success(res) {
          console.log("删除术前", res)
          wx.showToast({
            title: "成功",
            icon: 'success',
            duration: 1000
          })
         
          that.onLoad(that.data.options); //从新加载订单

        }
      })
    } else {

    }
  },
  //没发货查看物流提示
  log: function () {
    wx.showModal({
      title: '提示',
      content: '正在安排，请耐心等待，我们会尽快给您发货',
      success: function (res) {
        if (res.confirm) {
          console.log('用户点击确定')
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  //查看物流
  logistics: function (e) {
    var that = this;
    console.log('查看物流id', e.currentTarget.dataset)

    wx.request({
      url: app.globalData.baseUrl + 'shopp/goodsOrderApi/orderLogistics',
      data: {
        id: e.currentTarget.dataset.orderid
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log("查看物流结果", res)

      }
    })
  },
  //确认收货
  receipt: function (e) {
    var that = this;
    console.log('确认收货id', e.currentTarget.dataset)

    wx.request({
      url: app.globalData.baseUrl + 'shopp/goodsOrderApi/confirmCollectGoods',
      data: {
        id: e.currentTarget.dataset.orderid
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log("确认收货结果", res)
        wx.switchTab({
          url: '../user-my/user-my',
        })
        wx.showToast({
          title: '收货成功',
          icon: 'success',
          duration: 2000,

        })
      }
    })
  },

  //发起支付请求
  fkorder: function (e) {
    const { operation } = this.data;
    if (operation.ifPayment) {
      wx.navigateTo({
        url: `../user-shop-goods-order-pay/user-shop-goods-order-pay?res=${operation.orderId}&totalMoney=${operation.paymentMoney}&payType=1&orderType=1`,
      });
    } else {
      wx.navigateTo({
        url: `../user-shop-goods-balance-pay/user-shop-goods-balance-pay?res=${operation.orderId}&balance=${operation.useBalance}`,
      });
    }
  }
})