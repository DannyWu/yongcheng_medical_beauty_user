var app = getApp()
Page({
  data: {
    pagename: 2, // 首页为1 美容医生为2 为了判断定位和搜索框右侧的图标
    imgUrl: "https://tyc-bj.cn/yc/api/attach/",
    currentTab: 0,
    area: 0,
    // proId: 1,
    // cityId: 1,
    oneType: 1,
    distance: 1,
    sortType: '',
    pageSize: 5,
    pageNum: 1,
    doctorInfo: [],
    isShowCity: false, // 顶端搜索框旁边city
    isShowCity2: false, // 筛选医生city
    sort: 0, // 排序 1.城市 2.智能 3.筛选
    hidecity: true, // 是否隐藏城市列表
    hidearea: true, // 是否隐藏区域列表
    hidesort: true, // 是否隐藏只能排序
    areaname: '',
    areaid: 1, // 地区id (左上角显示的)
    provinceid: 1, // 省id
    openprovinceid: 1, // 已开通省id
    cityid: 2, // 市id
    areaid: '',
    level: 1, // 省市区等级
    currentLevel: 1, // 当前地区id的等级
    provincename: '', // 省名字
    hidetype: false, // 类型隐藏
    onetypeservicelist: [], // 服务产品分类列表
    servicesort: '', // 顶端服务产品分类排序 id
    keyword: '', // 搜索关键字
    serviceType: '', // 擅长服务项目 (一级分类) 名字
    distance: '', // 附近距离
    infolist: [],
    opencitylist: [],
    openarealist: [],
    opencityid: '', // 已开通城市id
    openareaid: '', // 已开通区id
    // opencityname: '全部城市', // 已开通的城市名称
    opencityname: '', // 已开通的城市名称
    intelligentsort: 1, // 智能排序
    intelligentname: '智能排序', // 智能排序名称
    isBindCity: false, // 是否点过城市
    isBindSory: false, // 是否点过智能排序
    isAddTopCss: false, // 是否加top 样式
    isHaveTopCss: false,
    areaId:"",
    isScreen:false,//筛选
    screening:true,//筛选弹出
    titleId: "", //职称id
    initial:"",//筛选的最小年龄
    end:"",//筛选的最大年龄
    searchPlaceholder: '请输入医生姓名',
    url:"https://tyc-bj.cn/yc/api/attach/images/doctor-top-jia.png",//搜索框右侧图片路径
  },

  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数
    this.loadOneTypeServiceList(); // 加载一级分类
    this.getAllCity();
    var that=this;
    that.zhicheng();
    var provinceid = wx.getStorageSync('provinceid');
    var provincename = wx.getStorageSync('provincename');

    var cityid = wx.getStorageSync('cityid');
    var cityname = wx.getStorageSync('cityname');
    console.log('已选省和市id:', provinceid, cityid);

    var opencityid = wx.getStorageSync('opencityid');
    var opencityname = wx.getStorageSync('opencityname');
    var openareaid = wx.getStorageSync('openareaid');
    var openareaname = wx.getStorageSync('openareaname');
    console.log('已选区名:', openareaname);
    console.log('已选市id:', openareaid);
    if (openareaname) {
      this.setData({
        'areaname': cityname,
        'cityid': cityid,
        'opencityid': openareaid,
        'opencityname': openareaname,
       
      })
    } else {
      this.setData({
        'areaname': cityname,
        'cityid': cityid,
        'opencityid': cityid,
        'opencityname': cityname,
       
      })
    };
    that.loadDoctorList();
    console.log('opencityid:', this.data.opencityid);

  },
//获取医生职务列表
zhicheng:function(){
  var that = this;
  wx.request({
    url: app.globalData.baseUrl + '/doctor/doctor/getJobList',
    header: {
      'sign': app.globalData.sign,
      'token': app.globalData.token,
      'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
    },
    method: 'POST',
    success: function (res) {
      console.log("医生职称:", res);
      that.setData({
        title: res.data.data
      });

    }
  })

},
  //滑动切换
  swiperTab: function(e) {
    var that = this;
    that.setData({
      area: e.detail.area
    });
  },
  //进入地图
  entryMap() {
    wx.navigateTo({
      url: '/pages/doctor-city-map/doctor-city-map'
    });
  },
  //点击切换
  clickArea: function(e) {
    var that = this;
    if (this.data.area === e.target.dataset.area) {
      return false;
    } else {
      that.setData({
        area: e.target.dataset.area
      })
    }
  },

  // 一级服务产品类别列表
  loadOneTypeServiceList: function(e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'shopp/goodsTypeApi/getPage',
      data: {
        'type': 2,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },
      method: 'POST',
      success: function(res) {
        console.log("医生页一级服务产品:", res);
        that.setData({
          'onetypeservicelist': res.data.data,
          'servicesort': res.data.data[0].id,
          // 'serviceType': res.data.data[0].id,
        });

      }
    })
  },

  // 处理服务产品分类切换
  handleServiceType: function(e) {
    var servicesort = e.currentTarget.dataset.servicesort;
    var index = e.currentTarget.dataset.index;

    this.setData({
      'serviceType': this.data.onetypeservicelist[index],
    });

    if (servicesort != this.data.servicesort) {
      this.setData({
        'servicesort': servicesort,
      });
      this.loadDoctorList();
    } else {
      return false;
    }
  },

  // 获取所有已开通的城市列表
  getAllCity: function(e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/city/getCityListForDoctor',
      data: {
        'type': 1,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('全部已开通城市:', res)
        if (!that.data.opencityid) {
          that.setData({
            'opencitylist': res.data.data,
            'opencityid': res.data.data[0].cityId,
          });
        } else {
          that.setData({
            'opencitylist': res.data.data,
          });
        }
        var area = [that.data.opencityid, 3, 2];
        that.getArea(area);
      }
    })
  },

  // 加载区
  getArea: function(e) {
    var that = this;

    wx.request({
      url: app.globalData.baseUrl + 'doctor/city/getAreaListByDoctorCity',
      data: {
        cityId: e[0],
       
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('当前市的区:', res)
        var openarealist = res.data.data;
        that.setData({
          'openarealist': openarealist,
          'openareaid': res.data.data[0].cityId,
          'areaid': res.data.data[0].cityId,
        });
        that.loadDoctorList();
      }
    })
  },

  // 医生列表加载
  loadDoctorList: function(e) {
    var that = this;
    var proId = wx.getStorageSync('provinceid');
    var areaId = that.data.openareaid;
    var cityId = that.data.opencityid;
    var oneTypeId = that.data.servicesort;
    var distance = that.data.distance;
    var intelligentsort = that.data.intelligentsort;
    var pageSize = that.data.pageSize;
    var pageNum = that.data.pageNum;
    var keyword = that.data.keyword;
    console.log('cityId', that.data.opencityid);
    console.log('areaId', that.data.openareaid);
    console.log('oneTypeId', that.data.servicesort);
    console.log('sortType', that.data.intelligentsort);
    console.log('pageSize', that.data.pageSize);
    console.log('pageNum', that.data.pageNum);
    console.log('keyName', that.data.keyword);
    console.log('titleID', that.data.titleId);


    var cityId = that.data.opencityid;
    console.log('cityId', cityId)
    var areaId ="";
    if (that.data.openareaid == that.data.opencityid || that.data.openareaid == 'undefined'){
      that.setData({
        areaId:"",
      });
    }else{
      that.setData({
        areaId: that.data.areaId,
      });
     
    }



    wx.request({
      url: app.globalData.baseUrl + 'doctor/doctor/getPage',
      data: {
        // 'proId': wx.getStorageSync('provinceid'), // 不用传
        'cityId': that.data.opencityid,
        'areaId': that.data.areaId,
        'oneTypeId': that.data.servicesort,
        // 'distance': that.data.distance, // 附近距离
        'sortType': that.data.intelligentsort, // 智能排序
        'pageSize': that.data.pageSize,
        'pageNum': that.data.pageNum,
        'keyName': that.data.keyword,
        'maxAge':that.data.end,
        'minAge': that.data.initial,
        'position': that.data.titleId,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('医生搜索结果:', res)
        that.setData({
          'infolist': res.data.data,
        })
      }
    })
  },

  // 医生页搜索
  indexSearch: function(e) {
    console.log('搜索医生')
    console.log("input:", e.detail.value)
    var that = this;
    var keyword = e.detail.value;
    this.setData({
      'keyword': keyword,
    })
    this.loadDoctorList();

  },

  // 选择城市(控制城市列表显隐)
  showCity: function(e) {
    var that = this;
    this.setData({
      isShowCity: !that.data.isShowCity,
      isShowCity2: false,
      isBindtype:true,
      isHaveTopCss: false,
      hidetype: !that.data.hidetype,
    })
    console.log('isHaveTopCss:', that.data.isHaveTopCss)
  },

  // 点击排序
  bindSort: function(e) {
    console.log('点击的sort:', e.currentTarget.dataset.sort)
    console.log('本地的sort:', this.data.sort)
    var sort = e.currentTarget.dataset.sort;
    console.log('sort:', sort)
    console.log('this.data.sort:',this.data.sort)
    if (sort != this.data.sort) {
      this.setData({
        'sort': sort,
        'isAddTopCss': true,
        'isHaveTopCss': true,
        
      });

      if (sort == 1) {

        console.log(111);
        
        this.setData({
          'isShowCity2': true,
          'hidesort': true,
          'hidetype': true,
          'isAddTopCss': true,
          'isBindCity': true,
          'isHaveTopCss': true,
        })
      } else if (sort == 2){
        console.log(222);
        
        this.setData({
          'hidetype': true,
          'isShowCity2': false,
          'hidesort': false,
          'isAddTopCss': true,
          'isBindSort': true,
          'isHaveTopCss': true,
        })
      }else{
        console.log(333);

        this.setData({
          'hidetype': true,
          'isShowCity2': false,
          'hidesort': true,
          'isAddTopCss': true,
          'isBindSort': false,
          'isHaveTopCss': true,
          'isScreen': false,
          'screening':false,
        })
      }
      this.loadDoctorList();
    } else {
     
      if (sort == 1) {
        console.log(333);
        
        this.setData({
          'hidetype': !this.data.hidetype,
          'isShowCity2': !this.data.isShowCity2,
          'isAddTopCss': !this.data.isAddTopCss,
          'isBindCity': true,
          'isHaveTopCss': !this.data.isHaveTopCss,
        })
      } else if (sort == 2){
        console.log(444);
        
        this.setData({
          'hidesort': !this.data.hidesort,
          'hidetype': !this.data.hidetype,
          'isAddTopCss': !this.data.isAddTopCss,
          'isBindSort': true,
          'isHaveTopCss': !this.data.isHaveTopCss,
        })
      }else{
        console.log(666);

        this.setData({
          'hidetype': !this.data.hidetype,
          'isAddTopCss': !this.data.isAddTopCss,
          'isHaveTopCss': !this.data.isHaveTopCss,
          'isScreen': true,
          'screening': !this.data.screening
        })
      }
    }
    console.log('isHaveTopCss',this.data.isHaveTopCss);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  // 离开页面关闭城市列表
  onHide: function() {
    this.setData({
      isShowCity: false,
      hidecity: true,
      hidearea: true,
      hidesort: true,
      hidetype: false,
    })
  },

  // 点击省
  bindProvince: function(e) {
    var provinceid = e.currentTarget.dataset.provinceid;
    var provincename = e.currentTarget.dataset.provincename;
    if (provinceid != this.data.provinceid) {
      this.setData({
        'provinceid': provinceid,
        'provincename': provincename,
      });
      console.log('省id', provinceid)
      var area = [provinceid, 2, 1];
      this.getCity(area);

    } else {
      return false;
    }
  },

  // 点击市
  bindCity: function(e) {
    var cityid = e.currentTarget.dataset.cityid;
    var cityname = e.currentTarget.dataset.cityname;
    if (cityid != this.data.cityid) {
      this.setData({
        'cityid': cityid,
        'areaname': cityname,
      });
      wx.setStorageSync('cityid', cityid);
      wx.setStorageSync('cityname', cityname);
      wx.setStorageSync('provinceid', this.data.provinceid);
      wx.setStorageSync('provincename', this.data.provincename);
      this.setData({
        isShowCity: false,
        'hidetype': false,
        hidesort: true,
      })
    } else {
      this.setData({
        isShowCity: false,
        'hidetype': false,
      })
      return false;
    }
  },

  // 点击区
  bindArea: function(e) {
    var openareaid = e.currentTarget.dataset.openareaid;
    var openareaname = e.currentTarget.dataset.openareaname;

    wx.setStorageSync('openareaid', openareaid);
    wx.setStorageSync('openareaname', openareaname);

    if (openareaid != this.data.openareaid) {
      this.setData({
        'openareaid': openareaid,
        'areaid': openareaid,
        'opencityname': openareaname,
        isShowCity2: false,
        'hidesort': true,
        'hidetype': false,
        'isHaveTopCss': false,
      });
      this.loadDoctorList();
    } else {
      this.setData({
        isShowCity2: false,
        'hidecity': false,
        'hidesort': true,
        'hidetype': false,
        'isHaveTopCss': false,
      })
      return false;
    }
  },

  // 点击已开通城市
  bindOpenCity: function(e) {
    var opencityid = e.currentTarget.dataset.opencityid;
    var opencityname = e.currentTarget.dataset.opencityname;

    wx.setStorageSync('opencityid', opencityid);
    wx.setStorageSync('opencityname', opencityname);

    if (opencityid != this.data.opencityid) {
      this.setData({
        'opencityid': opencityid,
        'cityid': opencityid,
        'opencityname': opencityname,
      });
      console.log('已开通市id', opencityid)
      var area = [opencityid, 3, 2];
      this.getArea(area);

    } else {
      return false;
    }
  },


  // 获取省
  getProvince: function(e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/city/getCityList',
      data: {
        id: e[0],
        level: e[1],
        currentLevel: e[2],
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('省列表:', res)
        that.setData({
          'provincelist': res.data.data,
        })
      }
    })
  },

  // 获取市
  getCity: function(e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/city/getCityList',
      data: {
        id: e[0],
        level: e[1],
        currentLevel: e[2],
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('当前省的所有市列表:', res)
        var citylist = res.data.data;
        that.setData({
          hidesort:true,
          isHaveTopCss: false,
          'citylist': citylist,
        })
        var cityid = wx.getStorageSync('cityid');
        if (cityid) {
          for (var i = 0; i < citylist.length; i++) {
            if (citylist[i]["cityId"] == cityid) {
              that.setData({
                areaname: citylist[i]["cityName"],
              })
            }
          }
          wx.setStorageSync('cityname', that.data.areaname);
        }
        that.loadDoctorList();
      }
    })
  },

  // 筛选城市
  bindChooseCity: function(e) {
    this.setData({
      hidetype: false,
      hidecity: true,
    })
  },

  // 筛选区域
  handleHideArea: function(e) {
    this.setData({
      hidearea: true,
      hidetype: false,
    })
  },

  // 筛选智能排序
  handleSort: function(e) {
    var intelligentsort = e.currentTarget.dataset.intelligentsort;
    var intelligentname = e.currentTarget.dataset.intelligentname;
    if (intelligentsort != this.data.intelligentsort) {
      this.setData({
        'intelligentsort': intelligentsort,
        'hidetype': false,
        'isShowCity2': false,
        'hidesort': true,
        'intelligentname': intelligentname,
        'isHaveTopCss': false,
      });
      this.loadDoctorList();
    } else {
      this.setData({
        'hidesort': !this.data.hidesort,
        'hidetype': !this.data.hidetype,
        'isHaveTopCss': false,
      })
      return false;
    }
  },
  //筛选重置
  reset:function(){
    this.setData({
      titleId:"",
      initial:'',
      end:"",
    })
  },
// 筛选排序
  screen:function(){
    this.setData({
      'hidetype': !this.data.hidetype,
      'isAddTopCss': !this.data.isAddTopCss,
      'isHaveTopCss': !this.data.isHaveTopCss,
      'isScreen': true,
      'screening': !this.data.screening
    })
    this.loadDoctorList();
  },
  //筛选点击
  dianji:function(e){
   
    this.setData({
      titleId: e.currentTarget.dataset.titleid,
    });
  },
  //筛选初始年龄
  initial:function(e){
    this.setData({
      initial: e.detail.value,
    });
  },
  //筛选结束年龄
  end:function(e){
    this.setData({
      end: e.detail.value,
    });
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var provinceid = wx.getStorageSync('provinceid');
    var cityid = wx.getStorageSync('cityid');
    var cityname = wx.getStorageSync('cityname');
    if (cityid) {
      this.setData({
        areaname: cityname,
        provinceid: provinceid,
        cityid: cityid,
        screening: true,
      })
      var province = ['', '', '']; // 初加载 默认请求全部省
      this.getProvince(province);
      var city = [provinceid, 2, 1]; // 初加载 请求已选省的市
      this.getCity(city);
    } else {
      this.loadaddress(); // 定位
    }
  },

  //获取当前位置
  loadaddress: function() {
    var that = this;
    wx.getLocation({
      type: 'wgs84',
      success(res) {
        console.log("定位", res)
        var latitude = res.latitude;
        var longitude = res.longitude;
        wx.setStorageSync('latitude', latitude)
        wx.setStorageSync('longitude', longitude)
        var location = latitude + "," + longitude;
        console.log("location", location)
        wx.request({
          url: app.globalData.baseUrl + 'doctor/city/getAreaByLocation',
          data: {
            'location': location
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('当前位置省id:', res.data.data.proId);
            console.log('当前位置市id:', res.data.data.cityId);
            var proId = res.data.data.proId;
            var cityId = res.data.data.cityId;
            that.setData({
              provinceid: proId,
              cityid: cityId,
            })
            wx.setStorageSync('provinceid', proId);
            wx.setStorageSync('cityid', cityId);
            var area = [proId, 2, 1];
            that.getCity(area);
          }
        })
      }
    })
  },

  // 占位
  goSearch: function(e) {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {
    this.setData({
      'isShowCity2': false,
      'hidetype': false,
    })
  },

})