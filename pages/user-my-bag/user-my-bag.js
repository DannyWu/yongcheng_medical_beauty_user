// pages/user-my-bag/user-my-bag.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    officialId:2,//文案id
    wallet: "",//钱包数据
    record:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.wallet();
    this.loadings();
  },
  //美丽任务
  jop2: function () {
    var userid = wx.getStorageSync('userid');
    wx.navigateTo({
      url: '../user-my-task/user-my-task?id=' + userid,
    })
  },
  //钱包,积分
  wallet:function(){
    var that = this;
    // var userid = wx.getStorageSync('userid');
    console.log('userId', wx.getStorageSync('userid'));
    console.log('sign', app.globalData.sign);
    console.log('token', app.globalData.token);    
    wx.request({
      url: app.globalData.baseUrl + 'doctor/wallet/getWalletByUser',
      data: {
        userId: wx.getStorageSync('userid'),//用户ID
        // officialId: that.data.officialId,//文案ID
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log("钱包", res.data.data)
        that.setData({
          wallet: res.data.data
        })
      }
    })
  },
  loadings: function (e) {
    var that = this;
    var id = 5;
    wx.request({
      url: app.globalData.baseUrl + 'system/officialapi/officialdetail', //个人中心设置中的文案信息详情展示
      data: {
        id: id
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log("文案信息详情", res)
        that.setData({
          record: res.data.data
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // this.wallet();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})