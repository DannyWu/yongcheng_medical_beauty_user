// pages/user-my-data/user-my-data.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: "",
    // dates: '1990-11-08',
    email: "",
    intro: "",
    myHeadImg: null, //本地的头像url
    userInfo: {}, //获取微信的 头像，昵称，性别
    hasUserInfo: false,
    conceal: !1,
    currentLevel: 1,
    level: 2, 
    array: ['未知', '男', '女'],
    index: 0,
    take: !0,
    
  }, 
  //保存提交
  addmydata: function() { 
    var that = this;
    var name = that.data.name;
    var birth = that.data.dates;
    var sex = that.data.index;
    var photo = that.data.avatarUrls;
    var provincename = that.data.provincename;
    var proId = that.data.provinceid;
    var areaId = that.data.cityid;
    var townId = that.data.districtid;

    var intro = that.data.intro;
    console.log('name', name);
    console.log('birth', birth);
    console.log('sex', sex);
    console.log('photo', photo);

    console.log('proId', proId);
    console.log('areaId', areaId);
    console.log('townId', areaId);

    console.log('intro', intro);

    if (photo == '') {
      wx.showToast({
        title: '请添加您的头像',
        icon: 'none',
        duration: 1000
      })
      return false;
    }
    if (name == undefined) {
      wx.showToast({
        title: '请输入您的真实姓名',
        icon: 'none',
        duration: 1000
      })
      return false;
    }
    if (birth == undefined) {
      wx.showToast({
        title: '请输入您的出生日期',
        icon: 'none',
        duration: 1000
      })
      return false;
    }

    if (sex == undefined) {
      wx.showToast({
        title: '请输入您的性别',
        icon: 'none',
        duration: 1000
      })
      return false;
    }
    if (provincename == undefined) {
      wx.showToast({
        title: '请输入您的所在城市',
        icon: 'none',
        duration: 1000
      })
      return false;
    }
    if (intro == undefined) {
      wx.showToast({
        title: '请输入您的个人简介',
        icon: 'none',
        duration: 1000
      })
      return false;
    }

    wx.request({
      url: app.globalData.baseUrl + 'doctor/member/finish',
      data: {
        name: name,
        birth: birth,
        sex: sex,
        photo: photo,
        proId: proId,
        areaId: areaId,
        townId: townId,
        intro: intro,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('完善个人信息:', res)
        // that.setData({
        //   'provincelist': res.data.data,
        // })
        if (res.data.status_code == 200) {
          wx.showToast({
            title: res.data.message,
            icon: 'success',
            duration: 2000
          })
          setTimeout(function() {
            wx.reLaunch({
              url: "../user-my/user-my"
            })
          }, 2000)
        }
      }
    })
  },
  // 上传图片
  changeHead: function() {
    var that = this;
    wx.chooseImage({
      count: 1,
      sizeType: ["original", "compressed"],
      sourceType: ["album", "camera"],
      success: function(e) {
        console.log(e);
        var a = e.tempFilePaths[0];
        wx.uploadFile({
          // url: app.globalData.baseUrl + 'doctor/product/uploadFile',
          url: app.globalData.baseUrl + 'doctor/member/uploadHead',
          filePath: a,
          name: "file",
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            "Content-Type": "multipart/form-data"
          },
          formData: {},
          success: function (res) {
            console.log('上传结果:', res);
            var path = JSON.parse(res.data).data[0].filePath;
            that.setData({
              avatarUrls: path
            })
          },
          fail: function (e) {
            console.log(e)
          }
        })
        that.setData({
          avatarUrl: a
        })
        console.log('666', that.data.avatarUrl)
      }

    })
  },
  getnameValue: function(e) {
    console.log("name", e.detail.value)
    this.setData({
      name: e.detail.value
    })
  },
  getcontentValue: function(e) {
    console.log("content", e.detail.value)
    this.setData({
      intro: e.detail.value
    })
  },
  bindPickerChange: function(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      index: e.detail.value
    })
  },
  // 选择城市
  updateaddress: function() {
    console.log(111);
    this.setData({
      'conceal': !0,
      'currentLevel': 1,
      'level': 2,
    })
    // 获取省
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/city/getCityList',
      data: {
  
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('省列表:', res)
        that.setData({
          'provincelist': res.data.data,
        })
      }
    })
  },
  bindProvince: function(e) {
    console.log('111---',e);
    var that = this;
    console.log('currentLevel', that.data.currentLevel);

    var id = e.currentTarget.dataset.provinceid;
    var name = e.currentTarget.dataset.provincename;
    var id = id;
    var level = that.data.level;
    var currentLevel = that.data.currentLevel;
    if (currentLevel == 1) {
      that.setData({
        'provinceid': id,
        'provincename': name,
      });
    } else if (currentLevel == 2) {
      that.setData({
        'cityid': id,
        'cityname': name,
      });
    } else if (currentLevel == 3) {
      that.setData({
        'districtid': id,
        'districtname': name,
      });
    }
    wx.request({
      url: app.globalData.baseUrl + 'doctor/city/getCityList',
      data: {
        id: id,
        level: level,
        currentLevel: currentLevel,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('列表:', res)
        that.setData({
          'provincelist': res.data.data,
          'level': level + 1,
          'currentLevel': currentLevel + 1,
        })

      }
    })
    console.log('currentLevel', that.data.currentLevel);
    if (that.data.currentLevel == 3) {
      that.setData({
        'conceal': !1,
        'currentLevel': 1,
        'level': 2,
      })
    }
  },
  // 日期选择按钮
  bindDateChanges: function(e) {
    console.log(e)    
    console.log(e.detail.value)
    this.setData({
      dates: e.detail.value
    })
  }, 
  // 日期选择按钮
  bindDateChange: function (e) {
    console.log(e)
    console.log(e.detail.value)
    this.setData({
      date: e.detail.value
    })
  }, 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      url: app.globalData.imgUrl
    })
    // this.loaddetail();

    var that = this; 
    wx.getUserInfo({
      success: function(res) {
        // console.log('1用户个人详情', res.userInfo)
        var userInfo = res.userInfo;
        that.setData({
          nickName: userInfo.nickName,
          avatarUrl: userInfo.avatarUrl,
          city: userInfo.city,
          gender: userInfo.gender,
        })
      } 
    })
    this.zilia();//会员详情
  },
  //会员详细资料
  zilia:function(){
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/member/personalDetail',
      data: {

      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('会员详细资料', res)
        var avatarUrls = res.data.data[0].headPic;
        if (avatarUrls.substr(0, 4) == 'http') {
          console.log("111:", 111);
          that.setData({
            take: !0
          })
        } else {
          console.log("222:", 222);
          that.setData({
            take: !1
          })
        }
        that.setData({
          user: res.data.data[0],
          name: res.data.data[0].name,
          avatarUrls: res.data.data[0].headPic,
          provincename: res.data.data[0].proName,
          cityname: res.data.data[0].areaName,
          districtname: res.data.data[0].townName,
          intro: res.data.data[0].intro, 
          dates: res.data.data[0].birth, 
          index: res.data.data[0].sex,
          provinceid: res.data.data[0].proId,                                       
          cityid: res.data.data[0].areaId,                                       
          districtid: res.data.data[0].townId,                                                                                     
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})