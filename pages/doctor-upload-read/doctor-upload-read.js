// pages/doctor-upload-read/doctor-upload-read.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgBase: app.globalData.imgUrl,
    contentStr: '',
    contentList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this;
    app.request({
      url: "system/officialapi/officialdetail",
      data: { id: 11 },
      success: function (res) {
        that.setData({
          contentList: res.data.contentList,
          contentStr: res.data.contentStr
        })
      },
      fail: function (res) {
        // app.requestFail(res);
      }
    });
  },

})