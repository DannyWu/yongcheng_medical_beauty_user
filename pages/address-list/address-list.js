// pages/address-list/address-list.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    carts:'',

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log('222', 222);    
    this.loadings();
  },
  compilelist: function(e) {
    console.log('点击收货人', e.currentTarget.dataset);
    var user = e.currentTarget.dataset
    app.globalData.consigneeid = user.id;
    app.globalData.consigneename = user.name;
    app.globalData.consigneephone = user.phone;
    app.globalData.consigneeaddress = user.address;    
    wx.navigateBack({
      url: '../user-shop-goods-order-1/user-shop-goods-order-1',
    })
  },
  loadings: function() {
    var that = this;
    var id = wx.getStorageSync('userid');
    // 获取用户收货人地址列表
    wx.request({
      url: app.globalData.baseUrl + 'shopp/orderConsigneeApi/getConsigneeArray',
      data: {

      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'GET',
      success(res) {
        console.log("收货人地址列表", res.data)
        that.setData({
          carts: res.data.data
        })
      }
    })
  },
  //删除收货人信息
  deleteClick: function(e) {
    var that = this;
    var id = e.currentTarget.dataset.id;
    wx.request({
      url: app.globalData.baseUrl + 'shopp/orderConsigneeApi/delConsignee',
      data: {
        id: id
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log("删除收货人信息", res.data)
        if (res.data.status_code == 200){
          wx.showToast({
            title: res.data.message,
            icon: 'success',
            duration: 1000
          })
          setTimeout(function () {
            that.loadings();
          }, 1000)
          
        }
      }
    })
  },
  //编辑收货人信息
  compile: function(e) {
    var that = this;
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../address-add/address-add?id=' + id,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    console.log('111', 111);
    this.loadings();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})