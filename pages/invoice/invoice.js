
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    show: false,
    choice: true,
    headType: '',          // 抬头类型
    headName: '',         // 抬头名称
    identityNumber: '',    // 识别号
    goodtype:'',//商品类型
    rise:false,   //抬头

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("ssss",options);
    if (options.goodsType==2){
      this.setData({
        rise: true,
      });
    } else if (options.goodsType == 1){
      this.setData({
        rise: false,
      });
    }
    if (wx.getStorageSync('invoice')){
      var invoice = wx.getStorageSync('invoice');
      this.setData({
        show: invoice.show,
        headType: invoice.headType,
        headName: invoice.headName,
        identityNumber: invoice.identityNumber,
      });
    }
  },
  //内容切换
  isShow(e) {
    const show = e.currentTarget.dataset.show;
    this.setData({
      show
    })
    console.log('kan',show);
    if (this.data.show==0){
      wx.setStorageSync('invoice', "");
    }
  },
  //选择抬头
  choiceType(e){
    const type = e.currentTarget.dataset.type;
    let headType; 
    this.setData({
      headType: type,
    })
  },
  //单位名称
  name(e){
    const headName = e.detail.value;
    this.setData({
      headName
    })
  },
  //识别号
  number(e) {
    const identityNumber = e.detail.value;
    this.setData({
      identityNumber
    })
  },
  //确认
  sure(){
    if( 1 == this.data.headType ){
      
      if (!app.validate(this.data.headName, 'required')) {
        app.showModal('请输入单位名称');
        return;
      }
      if (!app.validate(this.data.identityNumber, 'alphanumeric')) {
        app.showModal('请输入纳税人识别号');
        return;
      }
    }else{
      if (this.data.headType=="") {
        app.showModal('请选择发票抬头');
        return;
      }
    }
    const invoice = {
      show: this.data.show,
      headType: this.data.headType,
      headName: this.data.headName,
      identityNumber: this.data.identityNumber
    }
    wx.setStorageSync('invoice', invoice)
    wx.navigateBack();
  }
})