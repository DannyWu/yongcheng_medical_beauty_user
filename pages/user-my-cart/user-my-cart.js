const app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    imgBase: app.globalData.imgUrl,
    localImg: app.globalData.staticimgUrl,
    info: [],
    goodsnum: 1,
    // goodsid: 1,
    total_price: 0,
    isAllSelect: false,
    totalMoney: 0,
    carts: [],
    resultGoods: []
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getCartData();
    //this.getCartChoce();
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getCartData();
  },
  //获取购物车内勾选的商品
  // getCartChoce(){
  //   app.request({
  //     url: 'shopp/shoppcartApi/getCartCheckGoods',
  //     method: 'GET',
  //     success: function (res) {
  //       console.log(res)
  //     },
  //     fail: function (res) { }
  //   });
  // },

  //获取购物车数据
  getCartData: function() { 
    var that = this;
    app.request({
      url: 'shopp/shoppcartApi/getCartGoods',
      method: 'GET',
      success: function(res) {
        console.log(res.data);
        that.setData({
          carts: res.data
        })

        that.totalMony();
      },
      fail: function(res) {}
    });
  },
  // 减
  del: function(e) {
    const goodsid = e.currentTarget.dataset.goodsid;
    const data = this.data.carts;
    var that = this;
    console.log(data);
    for (let i = 0; i < data.length; i++) {
      if (goodsid == data[i].goodsid) {
        if (data[i].num > 1) {
          const param = {
            goodsid: goodsid,
            goodsnum: data[i].num - 1,
            'type': data[i].type
          };
          app.request({
            url: 'shopp/shoppcartApi/updateCartGoodsNum',
            data: param,
            success: function(res) {
              that.getCartData()
            },
            fail: function(res) {}
          });
        }
      }
    }
  },
  //加
  add: function(e) {
    const goodsid = e.currentTarget.dataset.goodsid;
    const data = this.data.carts;
    var that = this;
    console.log(data);
    for (let i = 0; i < data.length; i++) {
      if (goodsid == data[i].goodsid) {
        const param = {
          goodsid: goodsid,
          goodsnum: data[i].num + 1,
          'type': data[i].type
        };
        app.request({
          url: 'shopp/shoppcartApi/updateCartGoodsNum',
          method: 'POST',
          data: param,
          success: function(res) {
            that.getCartData()
          },
          fail: function(res) {}
        });
      }
    }
  },
  //删除购物车商品
  deleteGoods: function(e) {
    const goodsid = e.currentTarget.dataset.deleteid;
    var that = this;
    const param = {
      goodsid: goodsid
    };
    app.request({
      url: 'shopp/shoppcartApi/cartDelGoods',
      method: 'POST',
      data: param,
      success: function(res) {
        // console.log('deleteGoods',res)
        if (res.status_code == 200){
          wx.showToast({
            title: '删除商品成功',
            icon: 'success',
            duration: 1000
          })
          that.getCartData()
        }   
      },
      fail: function(res) {}
    });
  },
  //勾选事件处理函数  
  switchSelect: function(e) {
    // 获取item项的id，和数组的下标值  
    // var Allprice = 0,
    //   i = 0;
    const that = this;
    const goodsid = e.target.dataset.goodsid;
    let check = e.target.dataset.check;
    if (check == 0) {
      check = 1;
    } else {
      check = 0;
    }

    // index = parseInt(e.target.dataset.index);
    console.log(check)
    const param = {
      goodsid: goodsid,
      check: check
    };
    app.request({
      url: 'shopp/shoppcartApi/checkOutGoods',
      data: param,
      success: function(res) {
        that.getCartData()
      },
      fail: function(res) {}
    });

    // this.data.carts[index].isSelect = !this.data.carts[index].isSelect;
    //价钱统计
    // if (this.data.carts[index].isSelect) {
    //   this.data.totalMoney = this.data.totalMoney + this.data.carts[index].price;
    // } else {
    //   this.data.totalMoney = this.data.totalMoney - this.data.carts[index].price;
    // }
    //是否全选判断
    // for (i = 0; i < this.data.carts.length; i++) {
    //   Allprice = Allprice + this.data.carts[i].price;
    // }
    // if (Allprice == this.data.totalMoney) {
    //   this.data.isAllSelect = true;
    // } else {
    //   this.data.isAllSelect = false;
    // }
    // this.setData({
    //   carts: this.data.carts,
    //   totalMoney: this.data.totalMoney,
    //   isAllSelect: this.data.isAllSelect,
    // })
  },
  //计算总价钱
  totalMony() {
    const data = this.data.carts;
    let totalMoney = 0;
    for (let i = 0; i < data.length; i++) {
      if (data[i].check == 1) {
        totalMoney += data[i].goodsnum * data[i].price
      }
    }
    this.setData({
      totalMoney
    })
  },
  //全选
  allSelect: function(e) {
    //处理全选逻辑
    // let i = 0;
    // if (!this.data.isAllSelect) {
    //   for (i = 0; i < this.data.carts.length; i++) {
    //     this.data.carts[i].isSelect = true;
    //     this.data.totalMoney = this.data.totalMoney + this.data.carts[i].price;
    //   }
    // } else {
    //   for (i = 0; i < this.data.carts.length; i++) {
    //     this.data.carts[i].isSelect = false;
    //   }
    //   this.data.totalMoney = 0;
    // }
    // this.setData({
    //   carts: this.data.carts,
    //   isAllSelect: !this.data.isAllSelect,
    //   totalMoney: this.data.totalMoney,
    // })
  },
  //更改购物车内商品数量
  loadnum: function(id, newnum) {
    console.log('1', id);
    console.log('2', newnum);
    var that = this;
    var id = id;
    var goodsnum = newnum;
    wx.request({
      url: app.globalData.baseUrl + 'shopp/shoppcartApi/updateCartGoodsNum',
      data: {
        goodsid: id,
        goodsnum: goodsnum
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log("商品数量更改", res.data)
        that.setData({
          // sum: res.data
        })
      }
    })
  },
  //结算
  accounts: function(e) {
    var that = this;
    if (this.data.totalMoney == 0) {
      wx.showModal({
        title: '提示',
        content: '请选择商品',
        success(res) {}
      })
    } else {
      that.accountGoodsInfo();
    }
  },
  //获取结算的商品信息
  accountGoodsInfo() {
    const data = this.data.carts;
    let cartgoods = [];
    for (let i = 0; i < data.length; i++) {
      let item = {};
      if (data[i].check == 1) {
        item.goodsid = data[i].goodsid;
        item.num = data[i].goodsnum;
      }
      cartgoods.push(item)
    }
    console.log(cartgoods);
    const carts = JSON.stringify(cartgoods)
    console.log("aaaa",carts);
    wx.navigateTo({
      url: '../user-shop-goods-order-1/user-shop-goods-order-1?carts=' + carts
    })

  }
})