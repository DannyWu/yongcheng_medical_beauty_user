// pages/user-doctor-details/user-doctor-details.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrl: "https://tyc-bj.cn/yc/api/attach/",
    localImg: app.globalData.staticimgUrl, // 本地图片
    goodssort: '', // 医美商品类别
    casesort: '', // 案例类别
    diarysort: '', // 日记类别
    info: '',
    doctorid: '',
    onetypeservicelist: [], // 一级服务类产品列表
    goodsinfo: [], // 商品
    caseinfo: [], // 案例
    live: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log('医生id', options);
    this.ctx = wx.createLivePlayerContext('player')
    var doctorid = options.id;
    console.log('医生id', doctorid);

    this.setData({
      'doctorid': doctorid,
    });

    this.loadDoctorDetail(); // 加载个人信息
    this.loadDoctorGoodsInfo(); // 加载商品信息
    this.loadDoctorCaseInfo(); // 加载案例信息
    // this.loadOneTypeServiceList(); // 获取一级分类
    this.getDoctorLive();

  },

  // 一级服务产品类别列表
  loadOneTypeServiceList: function(e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'shopp/goodsTypeApi/getPage',
      data: {
        'type': 2,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },
      method: 'POST',
      success: function(res) {
        console.log("一级服务产品:", res);
        that.setData({
          'onetypeservicelist': res.data.data,
          'diarysort': res.data.data[0].id,
        })
        // that.loadDoctorDetail(); // 加载个人信息
        // that.loadDoctorGoodsInfo(); // 加载商品信息

      }
    })
  },

  // 加载医生个人信息
  loadDoctorDetail: function(e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/doctor/getDoctorHomeDetail',
      data: {
        'doctorId': that.data.doctorid,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('医生详情信息:', res);
        that.setData({
          info: res.data.data
        })
      }
    })
  },

  // 加载医生商品信息
  loadDoctorGoodsInfo: function(e) {
    var that = this;
    console.log('goodssort', this.data.goodssort);
    wx.request({
      url: app.globalData.baseUrl + 'doctor/doctor/getDoctorGoods',
      data: {
        'doctorId': that.data.doctorid,
        'typeId': that.data.goodssort,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('医生商品信息:', res);
        if (res.data.data.type.length) {
          if (that.data.goodssort) {
            that.setData({
              goodsinfo: res.data.data,
            });
          } else {
            that.setData({
              goodsinfo: res.data.data,
              goodssort: res.data.data.type[0].typeId,
            });
          }
        }
      }
    })
  },

  // 加载医生案例信息
  loadDoctorCaseInfo: function(e) {
    var that = this;
    // console.log('案例分类:', that.data.casesort);
    // console.log('医生id:', that.data.doctorid);
    wx.request({
      url: app.globalData.baseUrl + 'doctor/doctor/getDoctorExample',
      data: {
        'doctorId': that.data.doctorid,
        'typeId': that.data.casesort,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        // console.log('医生案例信息:', res);
        if (res.data.data.type.length) {
          if (that.data.casesort) {
            that.setData({
              caseinfo: res.data.data,
            });
          } else {
            that.setData({
              caseinfo: res.data.data,
              casesort: res.data.data.type[0].typeId,
            });
          }
        }
      }
    })
  },

  // 切换医美商品
  bindGoodsSort: function(e) {
    var goodssort = e.currentTarget.dataset.goodssort;
    if (goodssort != this.data.goodssort) {
      this.setData({
        'goodssort': goodssort,
      });
      this.loadDoctorGoodsInfo(); // 加载商品信息
    } else {
      return false;
    }
  },

  // 切换案例类型
  bindCaseSort: function(e) {
    var casesort = e.currentTarget.dataset.casesort;
    if (casesort != this.data.casesort) {
      this.setData({
        'casesort': casesort,
      });
      this.loadDoctorCaseInfo(); // 加载案例信息
    } else {
      return false;
    }
  },

  // 切换日记类型
  bindDiarySort: function(e) {
    var diarysort = e.currentTarget.dataset.diarysort;
    if (diarysort != this.data.diarysort) {
      this.setData({
        'diarysort': diarysort,
      });
    } else {
      return false;
    }
  },

  // 关注按钮
  bindFollow: function(e) {
    var isLogin = wx.getStorageSync('isLogin');
    if (isLogin == 2) {
      wx.showModal({
        title: '提示',
        content: '请先登陆!',
        success: function(res) {
          if (res.confirm) {
            wx.redirectTo({
              url: '/pages/user-login/user-login',
            });
          } else {}
        }
      })
    } else {
      var doctorid = e.currentTarget.dataset.doctorid;
      console.log('关注:doctorid', doctorid);
      var that = this;
      wx.request({
        url: app.globalData.baseUrl + 'doctor/member/attention',
        data: {
          userId: doctorid,
        },
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
        },
        method: 'POST',
        success(res) {
          console.log('关注医生结果:', res)
          that.loadDoctorDetail();
          wx.request({
            url: app.globalData.baseUrl + 'doctor/memberTaskApi/addBeauty',
            data: {
              userId: wx.getStorageSync('userid'),
              type: 5,
            },
            header: {
              'sign': app.globalData.sign,
              'token': app.globalData.token,
              'content-type': 'application/x-www-form-urlencoded; charset=utf - 8',
            },
            method: 'POST',
            success(res) {
              console.log('给美丽币给美丽值', res);
            }
          })
        }
      })
    }
  },

  // 获取医生直播
  getDoctorLive() {
    const that = this;
    const param = {
      doctorId: that.data.doctorid
    };

    app.request({
      url: 'train/onlineVideo/getDoctorLive',
      data: param,
      success: function(res) {
        console.log(res)
        that.setData({
          live: res.data
        })
      },
      fail: function(res) {
        // app.requestFail(res);
      }
    });
  },
  // 播放
  bindPlay() {
    this.ctx.play({
      success: res => {
        console.log('play success')
      },
      fail: res => {
        console.log('play fail')
      }
    })
  },
  // 跳转往期视频
  agoLive() {
    wx.navigateTo({
      url: "../user-doctor-video/user-doctor-video?doctorid=" + this.data.doctorid
    });
  },

  // 关注
  onShareAppMessage: function() {
    return {
      title: '分享好友获取美丽币！',
      path: '/pages/index/index',
      // imageUrl: '/pages/images/aa.jpg',
      success: function() {
        wx.request({
          url: app.globalData.baseUrl + 'doctor/memberTaskApi/addBeauty',
          data: {
            userId: wx.getStorageSync('userid'),
            type: 3
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('分享给美丽币给美丽值', res);
          }
        })
      }
    }
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.loadDoctorDetail(); // 加载个人信息
    this.loadDoctorGoodsInfo(); // 加载商品信息
    this.loadDoctorCaseInfo(); // 加载案例信息
    this.getDoctorLive();
  },
})