const app = getApp();
let moment = require('../../utils/moment.min.js');
Page({
  /**
   * 页面的初始数据
   */
  data: {
    ajaxLoading: true,
    weekArr: ["日", "一", "二", "三", "四", "五", "六"],
    currentDate: moment(),
    currentYearMonth: '',
    dayArray:[],
    selectedDateArr:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getUsableDay();
  },
  //获取可用日期
  getUsableDay() {
    wx.showLoading({
      title: '',
    });
    const that = this;
    let { currentDate} = that.data;
    const param = {
      doctorId: wx.getStorageSync('userid'),
      month: moment(currentDate).format('YYYY-MM')
    };
    app.request({
      url: 'doctor/scheduleApi/getDoctorTime',
      data: param,
      success: function(res) {
        wx.hideLoading();
        that.setData({
          selectedDateArr: res.data,
          ajaxLoading: false
        });
        //初始化
        that.getCalendarByCurrentDate();
      },
      fail: function(res) {
        // app.requestFail(res);
      }
    });
  },
  //获取下一个月份
  nextMonth() {
    let {currentDate} = this.data;
    let newCurrentDate = moment(currentDate).add(1, 'M');
    this.setData({
      currentDate: newCurrentDate
    });
    this.getUsableDay();
  },
  //获取上一个月份
  previousMonth() {
    let { currentDate } = this.data;
    if (moment(currentDate).isAfter(moment())){
      let newCurrentDate = moment(currentDate).subtract(1, 'M');
      this.setData({
        currentDate: newCurrentDate
      });
      this.getUsableDay();
    }
    else {
      return false
    }
  },

  getCalendarByCurrentDate(){
    let { selectedDateArr } = this.data;
    let currentDate = moment(this.data.currentDate);
    let currentYearmo = currentDate.format('YYYY年MM月');//2018年11月
    let weekForCurrentYearmo = moment(`${currentYearmo}1日`, 'YYYY年MM月DD日').day(); //1号星期几
    let daysForCurrentYearmo = moment(currentYearmo, 'YYYY年MM月').daysInMonth(); //30
    let dayArray = [];
    for (let i = weekForCurrentYearmo; i > 0 ; i--) {
      let date = moment(`${currentYearmo}1日`, 'YYYY年MM月DD日').subtract(i, 'days');
      dayArray.push({
        dateString: date.format('YYYY-MM-DD'),
        date,
        day: date.date(),
        isCurrentMonth: false,
        isBeforeToday: date.isBefore(moment()),
        isSelected: selectedDateArr.includes(date.format('YYYY-MM-DD'))
      });
    }
    for (let i = 1; i <= daysForCurrentYearmo; i++){
      let date = moment(`${currentYearmo}${i}日`, 'YYYY年MM月DD日');
      dayArray.push({
        dateString: date.format('YYYY-MM-DD'),
        date,
        day:i,
        isCurrentMonth: true,
        isBeforeToday: date.isBefore(moment()),
        isSelected: selectedDateArr.includes(date.format('YYYY-MM-DD'))
      });
    }
    this.setData({
      currentYearMonth: currentYearmo,
      dayArray
    });
  },

  //选择日期
  chooseDate(e) {
    const t = this;
    let item = e.currentTarget.dataset.item;
    let index = e.currentTarget.dataset.index;
    let dayArray = this.data.dayArray;

    if(item.isBeforeToday || !item.isCurrentMonth){
      return false;
    }
    else {
      item.isSelected = !item.isSelected;
      dayArray.splice(index,1,item);
      wx.showLoading({
        title: '',
      });
      app.request({
        url: 'doctor/scheduleApi/scheduleEdit',
        data: {
          doctorId: wx.getStorageSync('userid'),
          day: item.dateString
        },
        success: function (res) {
          wx.hideLoading();
          t.setData({
            dayArray: dayArray
          });
        },
        fail: function (res) {
          // app.requestFail(res);
        }
      });
    }
  }
})