// pages/doctor-add-review/doctor-add-review.js
const app = getApp();
Page({
  data: {
    imgBase: app.globalData.imgUrl, // 线上图片
    localImg: app.globalData.staticimgUrl, // 本地图片
    categoryName: '',
    categoryId: '',
    content: '',
    before: [],
    after: [],
    other: []
  },

  onLoad: function(options) {
    if (options.choosed) {
      const categoryName = options.choosed.split(",");
      const categoryId = options.choosedid.split(",");
      this.setData({
        categoryName,
        categoryId
      })
    }
  },
  // 内容
  getcontentValue: function(e) {
    this.setData({
      content: e.detail.value
    })
  },

  // 术前照片
  before(e) {
    const that = this;
    const index = e.currentTarget.dataset.index;
    wx.chooseImage({
      count: 1,
      sizeType: ["original", "compressed"],
      sourceType: ["album", "camera"],
      success: function(res) {
        let filePath = res.tempFilePaths[0];
        app.uploadFile({
          url: "system/commentApi/uploadCertFile",
          filePath: filePath,
          success: function(res) {
            let {
              before
            } = that.data;
            const path = JSON.parse(res.data).data[0].filePath;
            if (index) {
              before.splice(index - 1, 1, path);
            } else {
              before = before.concat(path);
            }
            that.setData({
              before
            })
          },
          fail: function(res) {
            // app.requestFail(res);
          }
        })
      }
    })
  },
  // 术后照片
  after(e) {
    const that = this;
    const index = e.currentTarget.dataset.index;
    wx.chooseImage({
      count: 1,
      sizeType: ["original", "compressed"],
      sourceType: ["album", "camera"],
      success: function(res) {
        const filePath = res.tempFilePaths[0];
        app.uploadFile({
          url: "system/commentApi/uploadCertFile",
          filePath: filePath,
          success: function(res) {
            let {
              after
            } = that.data;
            const path = JSON.parse(res.data).data[0].filePath;
            if (index) {
              after.splice(index - 1, 1, path);
            } else {
              after = after.concat(path);
            }
            that.setData({
              after
            })
          },
          fail: function(res) {
            // app.requestFail(res);
          }
        })
      }
    })
  },
  // 其他照片
  other(e) {
    const that = this;
    const index = e.currentTarget.dataset.index;
    wx.chooseImage({
      count: 1,
      sizeType: ["original", "compressed"],
      sourceType: ["album", "camera"],
      success: function(res) {
        const filePath = res.tempFilePaths[0];
        app.uploadFile({
          url: "system/commentApi/uploadCertFile",
          filePath: filePath,
          success: function(res) {
            let {
              other
            } = that.data;
            const path = JSON.parse(res.data).data[0].filePath;
            if (index) {
              other.splice(index - 1, 1, path);
            } else {
              other = other.concat(path);
            }
            that.setData({
              other
            })
          },
          fail: function(res) {
            // app.requestFail(res);
          }
        })
      }
    })
  },
  // 上传必看
  enrtyMustRead() {
    wx.navigateTo({
      url: "/pages/doctor-upload-read/doctor-upload-read"
    });
  },
  // 发布
  publish() {
    const {
      categoryId,
      content,
      before,
      after,
      other
    } = this.data;
    console.log(before)
    if (!app.validate(categoryId[2], 'required')) {
      app.showModal('请选择手术类别');
      return;
    }
    if (!app.validate(content, 'required')) {
      app.showModal('请添加内容');
      return;
    }
    if (before.length != 3) {
      app.showModal('请上传三张术前照片');
      return;
    }
    if (after.length != 3) {
      app.showModal('请上传三张术后照片');
      return;
    }

    const param = {
      serviceTypeId: categoryId[2],
      message: content,
      afterImgs: after,
      beforeImgs: before,
      otherImgs: other
    };
    app.request({
      url: "system/commentApi/insertComment",
      data: param,
      success: function(res) {
        console.log("---", res)
        if (res.status_code == 200) {


          wx.showLoading({
            title: '发布中',
            success: function () {
              wx.reLaunch({
                url: "/pages/doctor-review/doctor-review"
              });
            }
          })

          setTimeout(function () {
            wx.hideLoading()
          }, 2000)


          // setTimeout(() => {
          // wx.reLaunch({
          //   url: "/pages/doctor-review/doctor-review"
          // });
          // // }, 1500)
        }
      },
      fail: function(res) {
        app.requestFail(res);
      }
    });
  },
  entryCategory() {
    wx.navigateTo({
      url: "/pages/doctor-case-add-1/doctor-case-add-1?where=addreview"
    });
  }

})