// pages/doctor-my-case/doctor-my-case.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrl: "https://tyc-bj.cn/yc/api/attach/",
    exampleId: '',
    content: "",
    replyId: "",
    sort: 1,
    firsttype: 1, // 一级分类
    secondtype: 1, // 二级分类
    thirdtype: 1, // 三级分类
    currentTab: 0,
    choosed: [1, 2, 3],
    pageNum: 1,
    pageSize: 5,
    isshowadd: 0, // 控制是否显示添加按钮
    typelist: [],
    caseinfo: [],
    more: 0, // 是否为上拉加载
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log('options', options);
    var isshowadd = options.isShowAdd;
    this.setData({
      'isshowadd': isshowadd,
      'options': options,
    })
    this.oneTypeServiceList();
    var that = this;
    wx.getUserInfo({
      success: function(res) {
        console.log(res.userInfo)
        var userInfo = res.userInfo;
        that.setData({
          nickName: userInfo.nickName,
          avatarUrl: userInfo.avatarUrl,
        })
      }
    })
    // 调用案例评论接口
    // this.caseComment();
  },

  // 获取我的案例
  getCase: function(e) {
    // 医生端获取医生id 用户端不传
    var that = this;
    if (this.data.isshowadd == 1) {
      var doctorId = wx.getStorageSync('userid');
      wx.request({
        url: app.globalData.baseUrl + 'doctor/docExample/getExampleList',
        data: {
          doctorId: doctorId,
          oneTypeId: that.data.sort,
          pageNum: that.data.pageNum,
          pageSize: that.data.pageSize,
        },
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
        },
        method: 'POST',
        success(res) {
          console.log('用户端案例列表1', res);

          wx.hideLoading();
          if (res.data.data) {
            if (that.data.more == 0) {
              that.setData({
                'caseinfo': res.data.data,
              })
            } else {
              that.setData({
                more: 0,
              })
              if (res.data.data.length) {

                var info = that.data.caseinfo.concat(res.data.data);
                that.setData({
                  'caseinfo': info,
                })
              } else {
                wx.showToast({
                  title: '已无更多...',
                  icon: 'none',
                  duration: 2000,
                });
              }
            }
          }
        }
      })
    } else {
      wx.request({
        url: app.globalData.baseUrl + 'doctor/docExample/getExampleList',
        data: {
          oneTypeId: that.data.sort,
          pageNum: that.data.pageNum,
          pageSize: that.data.pageSize,
        },
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
        },
        method: 'POST',
        success(res) {
          console.log('用户端案例列表2', res);
          wx.hideLoading();
          if (res.data.data) {
            if (that.data.more == 0) {
              that.setData({
                'caseinfo': res.data.data,
              })
            } else {
              that.setData({
                more: 0,
              })
              if (res.data.data.length) {

                var info = that.data.caseinfo.concat(res.data.data);
                that.setData({
                  'caseinfo': info,
                })
              } else {
                wx.showToast({
                  title: '已无更多...',
                  icon: 'none',
                  duration: 2000,
                });
              }
            }
          }
        }
      })
    }
  },

  // 获取案例评论
  caseComment: function(e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/docExample/comment',
      data: {
        exampleId: that.data.exampleId,
        content: that.data.content,
        replyId: that.data.replyId
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('案例评论', res)
        // thst.setData({
        //   comment:res.data
        // })
      }
    })
  },

  // 切换案例排序
  caseSort: function(e) {
    var sort = e.currentTarget.dataset.sort;
    if (sort != this.data.sort) {
      this.setData({
        'sort': sort,
        'pageNum': 1,
      });
      this.getCase();
    } else {
      return false;
    }
  },

  // 删除案例
  deleteCase: function(e) {

  },

  // 一级服务产品类别列表
  oneTypeServiceList: function(e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'shopp/goodsTypeApi/getPage',
      data: {
        'type': 2,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },
      method: 'POST',
      success: function(res) {
        console.log('一级分类列表:', res)

        that.setData({
          'typelist': res.data.data,
          'sort': res.data.data[0].id,
        });
        // 获取案例
        that.getCase();
      }
    })
  },

  // 关注按钮
  bindFollow: function(e) {
    var isLogin = wx.getStorageSync('isLogin');
    if (isLogin == 2) {
      wx.showModal({
        title: '提示',
        content: '请先登陆!',
        success: function(res) {
          if (res.confirm) {
            wx.redirectTo({
              url: '/pages/user-login/user-login',
            });
          } else {}
        }
      })
    } else {
      var doctorid = e.currentTarget.dataset.doctorid;
      console.log('关注:doctorid', doctorid);
      var that = this;
        wx.request({
          url: app.globalData.baseUrl + 'doctor/member/attention',
          data: {
            userId: doctorid,
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('关注医生结果:', res)
            that.setData({
              pageNum: 1,
            })
            that.getCase();
            wx.showToast({
              title: '关注成功',
            })
          }
        })
      
      
      
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    // this.getCase();
    this.setData({
      'more': 0,
      'pageNum': 1,
    })
    this.oneTypeServiceList();
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    wx.showLoading({
      title: '加载中...',
    })
    this.setData({
      'more': 1,
      'pageNum': this.data.pageNum + 1,
    });
    console.log('页码:', this.data.pageNum)
    this.getCase();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
})