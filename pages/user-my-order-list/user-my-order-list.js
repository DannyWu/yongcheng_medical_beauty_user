// pages/user-my-order-list/user-my-order-list.js
var app = getApp();
Page({
 
  /** 
   * 页面的初始数据
   */
  data: { 

    pagenum: 1, // 页码 
    pagesize: 10, //行数 
    dingdan: "", //订单数据
    imgUrl: "https://tyc-bj.cn/yc/api/attach/", //拼接图片路径

    id: "", //取消订单ID
    type: "", //
    more: 0, //分页
    button:true,//完成按钮隐藏
    
  }, 
 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log('所有订单码', options)
    this.setData({
      type: options
    })
    this.quorder(options);
  },
  showButton:function(){
    
  },
  //区分订单
  quorder: function(options) {

    var url = '';
    if (options.type == 1) {
      url = 'shopp/orderApi/getPendingPayment'; //待付款
    } else if (options.type == 2) {
      url = 'shopp/orderApi/getWaitCompleted'; //待完成
    } else if (options.type == 3) {
      url = 'shopp/orderApi/getWaitEvaluate'; //待评价
    } else if (options.type == 4) {
      url = 'shopp/orderApi/getAfterSaleOrder'; //退换货
    } else {
      url = 'shopp/orderApi/getAllOrder'; //全部
    }
    this.order(url); // 获取用户所有订单
  },

  // 获取用户订单状态
  order: function(url) {
    var that = this;
    wx.request({
      // url: app.globalData.baseUrl + 'shopp/orderApi/getAllOrder', //仅为示例，并非真实的接口地址
      url: app.globalData.baseUrl + url, //仅为示例，并非真实的接口地址
      data: {
        pagenum: that.data.pagenum, // 页码
        pagesize: that.data.pagesize, //行数
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log("订单列表", res.data.data)
        if (res.data.data) {
          if (that.data.more == 0) {
            var dingdan = res.data.data;
            for (var i = 0; i < dingdan.length; i++) {
              var num = 0;
              for (var j = 0; j < dingdan[i].goodsArray.length; j++) {
                num = num + dingdan[i].goodsArray[j].num;
                // console.log("数量", dingdan[i].goodsArray[j].num);
              }
              dingdan[i].znum = num;
              console.log("总数量", num);

            }
            console.log("总数量", dingdan);
            that.setData({
              'dingdan': res.data.data,
              
            })
          } else {
            wx.hideLoading();
            that.setData({
              more: 0,
            })
            if (res.data.data.length) {
              var dingdan = res.data.data;
              var zjia = [];
              var znum = [];

              for (var i = 0; i < dingdan.length; i++) {
                var num = 0, unitPrice = 0;
                for (var j = 0; j < dingdan[i].goodsArray.length; j++) {
                  num = num + dingdan[i].goodsArray[j].num;
                  // console.log("数量", dingdan[i].goodsArray[j].num);
                }
                dingdan[i].znum = num;
                console.log("总数量", num);
                console.log("总价格", unitPrice);
              }
              console.log("总数量", dingdan);
              console.log("总价格", dingdan);

             
              var info = that.data.dingdan.concat(res.data.data);
              console.log('--info--', info)
              that.setData({
                'dingdan': info,
              })
              console.log('--dingdan--', that.data.dingdan)
            } else {
              wx.showToast({
                title: '已无更多...',
                icon: 'none',
                duration: 2000,
              });
            }
          }
        }

        //判断循环页

      }
    })
  },
  loadingChange: function() {

  },
  //待安排查看日程
  see: function() {
    wx.showModal({
      title: '提示',
      content: '暂无安排，请耐心等待，我们会尽快联系您给您安排',
      success: function(res) {
        if (res.confirm) {
          console.log('用户点击确定')
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    });
  },
  //没发货查看物流提示
  log:function(){
    wx.showModal({
      title: '提示',
      content: '正在安排，请耐心等待，我们会尽快给您发货',
      success: function (res) {
        if (res.confirm) {
          console.log('用户点击确定')
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  
  //确认收货
  receipt: function(e) {
    var that = this;
    console.log('确认收货id', e.currentTarget.dataset)

    wx.request({
      url: app.globalData.baseUrl + 'shopp/goodsOrderApi/confirmCollectGoods',
      data: {
        id: e.currentTarget.dataset.orderid
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log("确认收货结果", res)
        // that.onLoad(that.data.type);
        wx.switchTab({
          url: '../user-my/user-my',
        })
        wx.showToast({
          title: '收货成功',
          icon: 'success',
          duration: 2000,

        })
      }
    })
  },
  //取消订单按钮 1是术前术后  2是医美
  qxorder: function(e) {
    console.log('取消id', e.currentTarget.dataset)
    var that = this;
    // var id = e.currentTarget.dataset.id;
    that.setData({
      id: e.currentTarget.dataset.sid
    })
    var ordertype = e.currentTarget.dataset.ordertype;
    if (ordertype == 2) {
      //删除医美订单
      wx.request({
        url: app.globalData.baseUrl + 'shopp/yimeiGoodsOrderApi/cancelOrder',
        data: {
          id: that.data.id,
        },
        method: 'POST',
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
        },
        success(res) {
          console.log("删除", res)
          wx.showToast({
            title: "成功",
            icon: 'success',
            duration: 1000
          })
          that.onLoad(that.data.type); //从新加载订单
        }
      })
    } else if (ordertype == 1) {
      console.log("---", that.data.id)
      //删除术前术后订单
      wx.request({
        url: app.globalData.baseUrl + 'shopp/goodsOrderApi/cancelOrder',
        data: {
          id: that.data.id,
        },
        method: 'POST',
        // method: 'GET', 
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
          // 'content-type': 'application/json'
        },
        success(res) {
          console.log("删除术前", res)
          wx.showToast({
            title: "成功",
            icon: 'success',
            duration: 1000
          });
          that.onLoad(that.data.type); //从新加载订单
        }
      })
    } else {

    }

  },

  //订单跳转 1是术前术后  2是医美
  jop: function(e) {
    var that = this;
    var orderType = e.currentTarget.dataset.ordertype;
    var orderId = e.currentTarget.dataset.orderid;
    if (orderType == 1) {
      //术前术后
      wx.navigateTo({
        url: '../user-my-order-details/user-my-order-details?id=' + orderId + '&orderType=' + orderType,
      })
    } else {
      //医美
      wx.navigateTo({
        url: '../user-my-order-details-medical-1/user-my-order-details-medical-1?id=' + orderId + '&orderType=' + orderType,

      })
    }
  },
  //评价页
  ping: function (e) {
    var that = this;
    var orderType = e.currentTarget.dataset.ordertype;
    var orderId = e.currentTarget.dataset.orderid;  
    wx.navigateTo({
      url: '../user-my-order-list-ping/user-my-order-list-ping?id=' + orderId + '&orderType=' + orderType,
    })
     
  },
  //查看订单 1是术前术后  2是医美
  look: function(e) {
    var that = this;
    var orderType = e.currentTarget.dataset.ordertype;
    var orderId = e.currentTarget.dataset.orderid;
    if (orderType == 1) {
      console.log(1)
      wx.navigateTo({
        url: '../user-my-order-details/user-my-order-details?id=' + orderId + '&orderType=' + orderType,
      })
    } else {
      console.log(2)
      wx.navigateTo({
        url: '../user-my-order-details-medical-1/user-my-order-details-medical-1?id=' + orderId + '&orderType=' + orderType,

      })
    }
  },
  //发起支付请求
  fkorder: function(e) {
    var item = e.currentTarget.dataset.item;
    if (item.orderType == 1){
      //术前术后待支付
      if (item.ifPayment){
        wx.navigateTo({
          url: `../user-shop-goods-order-pay/user-shop-goods-order-pay?res=${item.orderId}&totalMoney=${item.paymentMoney}&payType=1&orderType=1`,
        });
      } else {
        wx.navigateTo({
          url: `../user-shop-goods-balance-pay/user-shop-goods-balance-pay?res=${item.orderId}&balance=${item.useBalance}`,
        });
      }
    }
    else{
      //医美
      if (item.orderStateCode == 2){
        //支付尾款
        wx.navigateTo({
          url: `../user-service-order-pay/user-service-order-pay?orderId=${item.orderId}&storePayment=${item.storePayment}`,
        })
      }
      else if (item.orderStateCode == 0) {
        //支付预付款
        if (item.ifPayment) {
          wx.navigateTo({
            url: `../user-shop-goods-order-pay/user-shop-goods-order-pay?res=${item.orderId}&totalMoney=${item.paymentMoney}&payType=1&orderType=1`,
          });
        } else {
          wx.navigateTo({
            url: `../user-shop-goods-balance-pay/user-shop-goods-balance-pay?res=${item.orderId}&balance=${item.useBalance}`,
          });
        }
      }
    }
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    wx.showLoading({
      title: '加载中...',
    })
    this.setData({
      'more': 1,
      'pagenum': this.data.pagenum + 1,
    });
    console.log('页码:', this.data.pagenum)
    this.onLoad(this.data.type);
  },
  onShow: function () {
    this.onLoad(this.data.type);
  }
})