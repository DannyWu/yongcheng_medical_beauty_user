// pages/user-shop-goods/user-shop-goods.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info: [],
    imgUrl: 'https://tyc-bj.cn/yc/api/attach/',
    pagenum: 1, // 页码
    pagesize: 10, //每页个数
    sort: 1, // 1.热门排序 2.好评排序 3.价格排序
    pricesort: 1, // 价格排序情况下:1.升序 2.降序
    typeid: 0, // 产品类别
    // isNav: 1, // 是否有导航分类
    goodsclass: 1,
    etype: 1,
    cityid: 0, // 城市id
    keyword: '', // 产品关键字
    typelist: [], // 类型列表数组
    more: 0, // 上拉加载
    ishottype: false, // 是否为点击热门分类进入页面
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log('options:', options);
    console.log('type:', this.data.etype);
    console.log('type:', options.type);
    if (options.type == 1) {
      if (options.typeid) {
        this.setData({
          'ishottype': true,
          'goodsclass': options.typeid,
        })
      }

      wx.setStorageSync('storeType', 1)
      this.setData({
        'etype': 1,
      })
    } else {
      wx.setStorageSync('storeType', 2)
      this.setData({
        'etype': 2,
      })
    }
    this.loadTypeList(options.type);
  },

  loadData: function(type) {
    console.log('type', type)
    var that = this;
    var url = '';
    if (type == 1) {
      url = app.globalData.baseUrl + 'shopp/yimeiGoodsApi/getPage';
    } else {
      url = app.globalData.baseUrl + 'shopp/goodsApi/getPage';
    }
    console.log('url::', url);
    console.log('pagenum:', that.data.pagenum);
    console.log('pagesize:', that.data.pagesize);
    console.log('pricesort:', that.data.pricesort);
    console.log('sort:', that.data.sort);
    console.log('goodsclass:', that.data.goodsclass);
    console.log('keyword:', that.data.keyword);
    console.log('cityid:', wx.getStorageSync('cityid'));
    wx.request({
      url: url,
      data: {
        'pagenum': that.data.pagenum,
        'pagesize': that.data.pagesize,
        'pricesort': that.data.pricesort,
        'sort': that.data.sort,
        'typeid': that.data.goodsclass,
        //  'cityid': 2,
        // 'cityid': wx.getStorageSync('cityid'),
        'keyword': that.data.keyword,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('术前术后商品数据:', res.data.data)

        if (that.data.more == 0) {
          that.setData({
            'info': res.data.data,
          })
        } else {
          wx.hideLoading();
          that.setData({
            more: 0,
          })
          if (res.data.data.length) {
            var info = that.data.info.concat(res.data.data);

            console.log('--info456--', info)

            that.setData({
              'info': info,
            })
          } else {
            wx.showToast({
              title: '已无更多...',
              icon: 'none',
              duration: 2000,
            });
          }
        }
      }

    })
  },

  // 加载分类列表
  loadTypeList: function(type) {

    if (type == 1) {
      this.oneTypeServiceList(); // 服务类
    } else {
      this.setData({
        'etype': 2,
      })
      this.oneTypeGoodsList(); // 实物类
    }
  },

  // 一级实物商品类别列表
  oneTypeGoodsList: function(e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'shopp/goodsTypeApi/getPage',
      data: {
        'type': 1,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },
      method: 'POST',
      success: function(res) {
        console.log('实物啊啊啊:', res)
        if (that.data.more == 0) {
          that.setData({
            'typelist': res.data.data,
            'goodsclass': res.data.data[0].id,
          });
        }
        that.loadData(that.data.etype); // 商品列表加载完成 请求数据
      }
    })
  },

  // 一级服务产品类别列表
  oneTypeServiceList: function(e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'shopp/goodsTypeApi/getPage',
      data: {
        'type': 2,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },
      method: 'POST',
      success: function(res) {
        console.log('服务啊啊啊:', res)
        if (that.data.more == 0) {
          if (that.data.ishottype) {
            that.setData({
              'typelist': res.data.data,
            });
          } else {
            that.setData({
              'typelist': res.data.data,
              'goodsclass': res.data.data[0].id,
            });
          }
        }
        that.loadData(that.data.etype); // 商品列表加载完成 请求数据
      }
    })
  },

  // 切换类别
  bindClass: function(e) {
    var goodsclass = e.currentTarget.dataset.goodsclass;

    if (goodsclass != this.data.goodsclass) {
      this.setData({
        'goodsclass': goodsclass,
        'pagenum': 1,
      });
      this.loadData(this.data.etype);
    } else {
      return false;
    }
  },

  // 点击切换排序
  bindSort: function(e) {
    var sort = e.currentTarget.dataset.sort;

    if (sort != this.data.sort) {

      console.log(222);
      this.setData({
        'sort': sort,
        'pagenum': 1,
      });
      this.loadData(this.data.etype);
    } else {
      console.log(111);

      // 如果为价格 改为升价/降价
      if (sort == 3) {
        if (this.data.pricesort == 1) {
          this.setData({
            'pricesort': 2,
            'pagenum': 1,
          });
        } else {
          this.setData({
            'pricesort': 1,
            'pagenum': 1,
          });
        }
        this.loadData(this.data.etype);
      }
    }
  },

  // 价格排序1.升序 2.降序
  bindPriceSort: function(e) {

    console.log('价格');
    var pricesort = e.currentTarget.dataset.pricesort;

    if (pricesort != this.data.pricesort) {
      this.setData({
        'pricesort': pricesort,
      });
      this.loadData(this.data.etype);
    } else {

    }
  },

  // 术前术后产品页搜索
  shopGoodsSelect: function(e) {
    console.log(e)
    var keyword = e.detail.value
    this.setData({
      'keyword': keyword
    });
    console.log(this.data.keyword)
    this.loadData(this.data.etype);
  },

  detailInfo: function(e) {
    console.log('点击:', e)
  },

  // 每次输入 重新赋值
  goSearch: function(e) {
    var keyword = e.detail.value
    this.setData({
      'keyword': keyword
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    console.log('下拉刷新');
    this.loadTypeList(this.data.etype);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    wx.showLoading({
      title: '加载中...',
    })
    this.setData({
      'more': 1,
      'pagenum': this.data.pagenum + 1,
    });
    console.log('页码:', this.data.pagenum)
    this.loadTypeList(this.data.etype);

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    return {
      title: '分享好友获取美丽币！',
      path: '/pages/index/index',
      // imageUrl: '/pages/images/aa.jpg',
      success: function() {
        wx.request({
          url: app.globalData.baseUrl + 'doctor/memberTaskApi/addBeauty',
          data: {
            userId: wx.getStorageSync('userid'),
            type: 3
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('分享给美丽币给美丽值', res);
          }
        })
      }
    }
  }
})