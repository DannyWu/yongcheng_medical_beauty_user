// pages/user-my-order-refund/user-my-order-refund.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderNum: "", //订单号
    orderId: "", //申请售后
    content: "", //内容
    currentTab: 0, //默认对号
    color: 1, //默认然色
    currentTab:"",//退款原因
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log('--a-', options)
    var orderNum = options.orderNum
    var orderType = options.orderType
    var orderId = options.orderId
    this.setData({
      orderNum: orderNum,
      orderId: orderId,
    })

  },

  // 保存信息
  save: function(e) {
    var that = this
    console.log('点击:', e);
    //内容
    this.setData({
      content: e.detail.value
    });
    console.log("内容:", that.data.content)
  },

  //退款申请
  apply: function(e) {
    var that = this;
    var content = that.data.content;
    if (content == "") {
      wx.showToast({
        title: '请描述问题',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if (that.data.currentTab==""){
      wx.showToast({
        title: '请选择退款原因',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    var orderType = that.data.orderId;
    console.log('术前术后的', that.data.orderId) //术前术后的
    console.log('问题描述', that.data.content) //问题描述
    console.log('退换原因', that.data.name) //退换原因
    wx.request({
      url: app.globalData.baseUrl + 'shopp/yimeiGoodsOrderApi/orderRefund', //仅为示例，并非真实的接口地址
      data: {
        id: that.data.orderId, //术前术后的
        describe: that.data.content, //问题描述
        problem: that.data.name, //退换原因
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log("申请退款", res)
        if (res.data.status_code == 200){   
          wx.showToast({
            title: "退款申请成功",
            icon: 'none',
            duration: 2000,
          })
          setTimeout(function () {
            wx.switchTab({
              url: '../user-my/user-my',
            })
          }, 2000)
        } else if (res.data.status_code == 500){
          wx.showToast({
            title:"此状态无法退款",
            icon: 'success',
            duration: 2000
          })
          setTimeout(function () {
            wx.navigateBack({
              url: '../user-my-order-list/user-my-order-list/',
            })
          }, 2000)
        } 
      }
    })
  },
  //默认然色
  cickTab: function(e) {
    console.log("-然色-", e)
    var that = this;

    if (this.data.color == e.target.dataset.current) {

    } else {
      that.setData({
        color: e.target.dataset.current
      })
    }
  },
  //对号切换
  clickTab: function(e) {
    console.log("-对号-", e)
    var that = this;

    if (this.data.currentTab === e.target.dataset.current) {

    } else {
      that.setData({
        currentTab: e.target.dataset.current,
        name: e.target.dataset.name,
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})