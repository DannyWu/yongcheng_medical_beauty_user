const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderid: '',
    payType: '', // 支付类型；1=商品订单支付；2=课程支付；3=充值支付
    totalPrice: '',
    orderType: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log("---lei---", options)
    this.setData({
      orderid: options.res, //订单id
      payType: options.payType, //商品类型 1=术前术后   2=医美产品
      totalPrice: options.totalMoney, //支付金额
      orderType: options.orderType, //支付类型  1=商品订单支付；2=课程支付；3=充值支付
        
    });
    this.invoice();
  },
  pay: function() {
    var that = this;
    var orderType = this.data.orderType;
    var orderId = this.data.orderid;
    const param = {
      orderid: this.data.orderid,
      payType: this.data.orderType,
    }
    app.request({
        url: "shopp/payment/requestPayment",
        data: param,
        success: function(res) {
          wx.removeStorageSync('invoice');
          wx.requestPayment({
              'timeStamp': res.data.timeStamp,
              'nonceStr': res.data.nonceStr,
              'package': res.data.package,
              'signType': res.data.signType,
              'paySign': res.data.paySign,
              'success': function(res) {

                if (that.data.orderType == 3) {
                  wx.navigateTo({
                    url: '../user-my-recharge-record/user-my-recharge-record',

                // if (that.data.payType == 3) {
                //   wx.switchTab({
                //     url: '../user-my/user-my',

                  })
                } else {
                  if (res.errMsg == "requestPayment:ok") {
                    wx.navigateTo({
                      url: '../user-shop-goods-order-2/user-shop-goods-order-2',
                    })
                    wx.request({
                      url: app.globalData.baseUrl + 'doctor/memberTaskApi/addBeauty',
                      data: {
                        userId: wx.getStorageSync('userid'),
                        type: 4,
                        orderType: orderType,
                        orderId: orderId
                      },
                      header: {
                        'sign': app.globalData.sign,
                        'token': app.globalData.token,
                        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
                      },
                      method: 'POST',
                      success(res) {
                        console.log('给美丽币给美丽值', res);
                      }
                    })
                  }
                }
              },
              'fail': function(res) {},
              'complete': function(res) {},
          })
      },
      fail: function(res) {
        // app.requestFail(res);
      }
    });

},
//发票信息
  invoice:function() {
   var that=this;
    if (wx.getStorageSync('invoice')){

    console.log(wx.getStorageSync('invoice')) ;
    var invoice=wx.getStorageSync('invoice');
      console.log('抬头名',invoice.headName);
      console.log('订单类型', that.data.payType);
      console.log('纳税人证号', invoice.identityNumber);
      const param = {
        orderNumber: that.data.orderid,//订单id
        orderType: that.data.payType,//订单类型
        headType: invoice.headType,//抬头类型
        headName: invoice.headName,//抬头名
        identityNumber: invoice.identityNumber ,//纳税人识别号
      };
      app.request({
        url: 'shopp/financeApi/insertInvoice',
        data: param,
        success: function (res) { 
           invoice="";
          wx.setStorageSync("invoice", invoice)
          console.log('发票', res);
          console.log('发票信息', wx.getStorageSync("invoice"));
        },
        fail: function (res) { }
      });
    }
    
  } 
})