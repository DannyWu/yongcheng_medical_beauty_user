// pages/user-my-movie-add/user-my-movie-add.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: { 
    pageNum:1, 
    pageSize:100,
    list:"",//列表数据
    bookId:"",//id
  }, 
 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.diarylist();
  },
  //用户个人日记本列表
  diarylist:function(){
    var that = this;
    var userId = wx.getStorageSync("userid");
    // console.log("123qwe",userId)
    wx.request({
      url: app.globalData.baseUrl + 'doctor/diaryBook/getDiaryBook',
      data: {
        // pageNum:1,
        pageNum: that.data.pageNum,
        // pageSize:10,
        pageSize: that.data.pageSize,
        userId:userId,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },                                                                                                                                                              
      method: 'POST',
      success: function (res) {
        console.log("用户日记本列表",res.data)
        that.setData({
          list:res.data.data,
          
        })
        
      }
    })
  },
  //创建日记本限制
  establish:function(){
    var isLogin = wx.getStorageSync('isLogin')
    if (!isLogin || isLogin == 2) {
      wx.showToast({
        title: '请登录!',
        icon: 'loading',
      })
      // 跳转到登入界面
      wx.redirectTo({
        url: '/pages/user-login/user-login',
      });
    }else{
      wx.navigateTo({
        url:'/pages/user-diary-book-add-1/user-diary-book-add-1'
      })
    }
  },
  //创建日记本限制
  diary:function(e){
    console.log('sd', e.currentTarget.dataset.earlyitem);
    var isLogin = wx.getStorageSync('isLogin')
    if (!isLogin || isLogin == 2) {
      wx.showToast({
        title: '请登录!',
        icon: 'loading',
      })
      // 跳转到登入界面
      wx.redirectTo({
        url: '/pages/user-login/user-login',
      });
    } else {
      var bookId = e.currentTarget.dataset.bookid;
      wx.navigateTo({
        url: "/pages/user-diary-add/user-diary-add?bookId=" + bookId + "&&earlyTime=" +e.currentTarget.dataset.earlyitem
      })
    }
  }
})