// pages/address-add/address-add.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    message: "",
    conceal: !1,
    currentLevel: 1,
    level: 2,
    isdefault: 0,
    newcheck: !1,
    name:"",
    mobile: "",
    provinceid: "",
    address: "",
    zipcode: "",
  },
  updateaddress: function() {
    console.log(111);
    this.setData({
      'conceal': !0,
      'currentLevel': 1,
      'level': 2,
    })
    // 获取省
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/city/getCityList',
      data: {

      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('省列表:', res)
        that.setData({
          'provincelist': res.data.data,
        })
      }
    })
  },
  bindProvince: function(e) {
    console.log(222);
    var that = this;
    console.log('currentLevel', that.data.currentLevel);

    var id = e.currentTarget.dataset.provinceid;
    var name = e.currentTarget.dataset.provincename;
    var id = id;
    var level = that.data.level;
    var currentLevel = that.data.currentLevel;
    if (currentLevel == 1) {
      that.setData({
        'provinceid': id,
        'provincename': name,
      });
    } else if (currentLevel == 2) {
      that.setData({
        'cityid': id,
        'cityname': name,
      });
    } else if (currentLevel == 3) {
      that.setData({
        'districtid': id,
        'districtname': name,
      });
    }
    wx.request({
      url: app.globalData.baseUrl + 'doctor/city/getCityList',
      data: {
        id: id,
        level: level,
        currentLevel: currentLevel,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('列表:', res)
        that.setData({
          'provincelist': res.data.data,
          'level': level + 1,
          'currentLevel': currentLevel + 1,
        })

      }
    })
    console.log('currentLevel', that.data.currentLevel);
    if (that.data.currentLevel == 3) {
      that.setData({
        'conceal': !1,
        'currentLevel': 1,
        'level': 2,
      })
    }
  },
  switchChange: function(e) {
    console.log('switch 发生 change 事件，携带值为', e.detail.value)
    if (e.detail.value == false) {
      console.log(0);
      this.setData({
        isdefault: 0,
      })
    } else {
      console.log(1);
      this.setData({
        isdefault: 1
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(e) {
    var that = this;
    console.log(e);
    if (e.id != null) {
      that.loadings(e)
    } else {
      return false;
    }
  },
  loadings: function(e) {
    var that = this;
    console.log("id", e.id);
    var id = e.id;
    this.setData({
      id: id
    })
    // 获取收货人信息
    wx.request({
      url: app.globalData.baseUrl + 'shopp/orderConsigneeApi/getConsignee/' + id,
      data: {

      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        // 'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
        'content-type': 'application/json'
      },
      method: 'GET',
      success(res) {
        console.log("收货人信息", res.data)
        if (res.data.status_code == 200) {
          that.setData({
            message: res.data.data,
            name: res.data.data.name,
            mobile: res.data.data.mobile,
            address: res.data.data.address,
            cityAddress: res.data.data.cityAddress,
            isdefault: res.data.data.isdefault,
            zipcode: res.data.data.zipCode,
            provincename: res.data.data.city,
            provinceid: res.data.data.provinceId,
            cityid: res.data.data.cityId,            
            countyid: res.data.data.countyId,                                    
          })
        }
        if (res.data.data.isdefault == 1) {
          that.setData({
            newcheck: !0
          })
        } else if (res.data.data.isdefault == 0) {
          that.setData({
            newcheck: !1
          })
        }
      }
    })
  },
  getnameValue: function(e) {
    console.log("name", e.detail.value)
    this.setData({
      name: e.detail.value
    })
  },
  getmobileValue: function(e) {
    console.log("mobile", e.detail.value)
    this.setData({
      mobile: e.detail.value
    })
  },
  getaddressValue: function(e) {
    console.log("address", e.detail.value)
    this.setData({
      address: e.detail.value
    })
  },
  getzipcodeValue: function(e) {
    console.log("zipcode", e.detail.value)
    this.setData({
      zipcode: e.detail.value
    })
  },
  addaddress: function() {
    var that = this;
    var id = that.data.id;
    if (id) {
      that.updateConsignee(id);
    } else {
      that.addConsignee();
    }
  },
  //编辑收货人信息
  updateConsignee: function(e) {
    var that = this;
    var name = that.data.name;
    var mobile = that.data.mobile;
    var address = that.data.address;
    var zipcode = that.data.zipcode;
    var provinceid = that.data.provinceid;
    var cityid = that.data.cityid;
    var countyid = that.data.countyid;
    var isdefault = that.data.isdefault;
    var id = that.data.id;
    console.log("address", address)
    console.log("cityid", cityid)
    console.log("countyid", countyid)
    console.log("id", id)
    console.log("isdefault", isdefault)
    console.log("mobile", mobile)
    console.log("name", name)
    console.log("provinceid", provinceid)
    console.log("zipcode", zipcode)
    if (name == "") {
      wx.showToast({
        title: '请添加您的姓名',
        icon: 'none',
        duration: 1000
      })
      return false;
    }
    if (mobile == "") {
      wx.showToast({
        title: '请添加您的电话',
        icon: 'none',
        duration: 1000
      })
      return false;
    }
    if (provinceid == "") {
      wx.showToast({
        title: '请选择您的地址',
        icon: 'none',
        duration: 1000
      })
      return false;
    }
    if (address == "") {
      wx.showToast({
        title: '请添加您的详细地址',
        icon: 'none',
        duration: 1000
      })
      return false;
    }
    if (zipcode == "") {
      wx.showToast({
        title: '请添加您的邮编',
        icon: 'none',
        duration: 1000
      })
      return false;
    }    
    wx.request({
      url: app.globalData.baseUrl + 'shopp/orderConsigneeApi/updateConsignee',
      data: {
        address: address,
        cityid: cityid,
        countyid: countyid,
        id: id,
        isdefault: isdefault,
        mobile: mobile,        
        name: name,
        provinceid: provinceid,      
        zipcode: zipcode,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log("编辑用户", res.data)
        if (res.data.status_code == 200) {
          wx.showToast({
            title: res.data.message,
            icon: 'success',
            duration: 2000
          })
          setTimeout(function() {
            wx.navigateTo({
              url: "../address-list/address-list"
            })
          }, 2000)
        }
      }
    })
  },
  addConsignee: function(e) {
    var that = this;
    var name = that.data.name;
    var mobile = that.data.mobile;
    var address = that.data.address;
    var zipcode = that.data.zipcode;
    var provinceid = that.data.provinceid;
    var cityid = that.data.cityid;
    var countyid = that.data.districtid;
    var isdefault = that.data.isdefault;
    console.log('name', name);
    console.log('mobile', mobile);
    console.log('address', address);
    console.log('zipcode', zipcode);
    console.log('provinceid', provinceid);
    if (name == undefined) {
      wx.showToast({
        title: '请添加您的姓名',
        icon: 'none',
        duration: 1000
      })
      return false;
    }
    if (mobile == undefined) {
      wx.showToast({
        title: '请添加您的电话',
        icon: 'none',
        duration: 1000
      })
      return false;
    }
    if (provinceid == undefined) {
      wx.showToast({
        title: '请选择您的地址',
        icon: 'none',
        duration: 1000
      })
      return false;
    }
    if (address == undefined) {
      wx.showToast({
        title: '请添加您的详细地址',
        icon: 'none',
        duration: 1000
      })
      return false;
    }
    if (zipcode == undefined) {
      wx.showToast({
        title: '请添加您的邮编',
        icon: 'none',
        duration: 1000
      })
      return false;
    }
    wx.request({
      url: app.globalData.baseUrl + 'shopp/orderConsigneeApi/addConsignee',
      data: {
        name: name,
        mobile: mobile,
        address: address,
        provinceid: provinceid,
        cityid: cityid,
        countyid: countyid,
        isdefault: isdefault,
        zipcode: zipcode,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log("添加用户", res.data)
        if (res.data.status_code == 200) {
          wx.showToast({
            title: res.data.message,
            icon: 'success',
            duration: 2000
          })
          setTimeout(function() {
            wx.navigateBack({
              url: "../address-list/address-list"
            })
          }, 2000)
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})