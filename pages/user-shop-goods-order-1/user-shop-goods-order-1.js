const app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    newPrice: "",
    imgBase: app.globalData.imgUrl,
    localImg: app.globalData.staticimgUrl,
    totalMoney: 0,
    calculateTotalMoney: 0,
    take: !1,
    takes: !0,
    carts: '',

    balance: '', // 余额
    checkedBalance: false,
    checkedcoin: false,
    maxCoin: '', // 美丽币 
    maxPrice: '', // 抵用钱数
    goodsList: [], // 订单列表数据
    totalPrice: '', // 订单总金额
    consigneeid: "", //收货人的信息
    newvalue: "",
    isinvoice: false,
    newchecked: !1,
    orderType: '' // 订单类型
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onShow: function() {
    var that = this;
    if (app.globalData.consigneeid != "") {
      that.setData({
        takes: !1,
        take: !0,
        consigneeid: app.globalData.consigneeid,
        consigneename: app.globalData.consigneename,
        consigneephone: app.globalData.consigneephone,
        consigneeaddress: app.globalData.consigneeaddress,
      })
    }
  },
  onLoad: function(options) {
    const carts = options.carts;
    this.setData({
      carts
    })
    this.getOrderList();
    this.beautifulCoin();
    this.getAddress(); 
    this.loaduser();  
  },
  //获取订单列表
  getOrderList() {
    const that = this;
    const param = {
      cartgoods: this.data.carts
    }
    app.request({
      url: 'shopp/shoppcartApi/settlement',
      data: param,
      success: function(res) {
        console.log("res:", res);
        that.setData({
          goodsList: res.data.goodsArray,
          totalPrice: res.data.totalPrice,
          calculateTotalMoney: res.data.totalPrice,
          orderType: 1
        });
        wx.setStorageSync('totalPrice', res.data.totalPrice);
      },
      fail: function(res) {}
    });
  },
  //获取收货地址列表
  getAddress() {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'shopp/orderConsigneeApi/getConsigneeArray',
      data: {},
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'GET',
      success(res) {
        console.log("收货人地址列表", res.data)
        console.log("收货人地址列表", res.data.data)
        var array = res.data.data;
        for (var kk = 0; kk < array.length; kk++) {
          if (array[kk].isdefault == 1) {
            that.setData({
              consigneeid: res.data.data[kk].id,
              consigneename: res.data.data[kk].name,
              consigneephone: res.data.data[kk].mobile,
              consigneeaddress: res.data.data[kk].address,
              take: !0,
              takes: !1,
            })
          }
        }
      }
    })
  },
  loaduser: function () {
    //获取用户id
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/member/detail',
      data: {},
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },
      method: 'POST',
      success: function (t) {
        console.log("个人中心", t);
        that.setData({
          core: t.data.data[0],
          name: t.data.data[0].name,
          beautyCoin: t.data.data[0].beautyCoin,
          headPic: t.data.data[0].headPic
        })
      }
    })
  },
  //获取美丽币
  beautifulCoin() {
    const that = this;
    app.request({
      url: "doctor/beautyCoin/getBeautyCoin",
      success: function(res) {
        console.log("获取美丽币", res);

        that.setData({
          maxCoin: res.data.maxCoin,
          maxPrice: res.data.maxPrice,
          newPrice: res.data.valueMoney,
          userUseCoin: res.data.userUseCoin,
          balance: res.data.currUser.totalMoney
        })

      },
      fail: function(res) {
        // app.requestFail(res);
      }
    });
  },
  //协议内容
  agreement: function() {
    var id = 9;
    wx.navigateTo({
      url: '../doctor-news-system/doctor-news-system?id=' + id,
    })
  },
  changeBanlanceChecked: function(e) {
    let {
      balance
    } = this.data;
    console.log("balance:", balance);
    console.log("checkedBalance:", this.data.checkedBalance);
    if (Number(balance) > 0) {
      let checked = !this.data.checkedBalance;
      console.log('实际支付金额', Number(this.data.totalPrice) - Number(balance));
      if (Number(this.data.totalPrice) - Number(balance) > 0) {
        this.setData({
          isinvoice: false,
          checkedBalance: checked
        });
      } else {
        this.setData({
          isinvoice: !this.data.isinvoice,
          checkedBalance: checked
        });
      }
      this.calculateTotalPrice();
    } else {
      wx.showToast({
        icon: 'none',
        title: '您的余额不足',
        duration: 1000
      });
    }
  },
  changeCoinChecked: function(e) {
    var beautyCoin = this.data.beautyCoin;
    if (beautyCoin <= 0) {
      wx.showToast({
        icon: 'none',
        title: '您的美丽币不足',
        duration: 1000
      });
      return false;
    }
    let {
      maxCoin
    } = this.data;
    // if (Number(maxCoin) > 0) {
    if (Number(maxCoin) > 0) {
      let checked = !this.data.checkedcoin;
      this.setData({
        checkedcoin: checked
      });
      this.calculateTotalPrice();
    } else {
      // wx.showToast({
      //   icon: 'none',
      //   title: '您的美丽币不足',
      //   duration: 1000
      // });
      app.showModal('美丽币抵扣超出今日最大抵扣');
    }
  },
  calculateTotalPrice: function() {
    let {
      totalPrice,
      balance,
      maxPrice,
      checkedcoin,
      checkedBalance
    } = this.data;
    let total = Number(totalPrice);
    if (checkedcoin) {
      // total = total - Number(maxPrice);
      //开始 
      var totals = total;
      total = total - Number(maxPrice); 
      if (total >= 0) {      
        this.setData({
          usecoin: maxPrice
        })
        total = total
      } else {      
        this.setData({
          usecoin: totals
        })
        total = 0
      }
      //结束
      if (checkedBalance) {
        if (total - Number(balance) > 0) {
          total = total - Number(balance);
        }
      }
    } else {
      if (checkedBalance) {
        if (total - Number(balance) > 0) {
          total = total - Number(balance);
        }
      }
    }
    this.setData({
      calculateTotalMoney: total
    });
  },
  //点击同意协议
  checkboxChange: function(e) {
    console.log(e.detail.value[0]);
    this.setData({
      newvalue: e.detail.value[0]
    })
  },
  //提交订单
  submitRoder: function() {
    const that = this;
    let {
      usecoin,
      checkedcoin,
      checkedBalance,
      balance,
      maxPrice,
      totalPrice,
      calculateTotalMoney
    } = this.data;
    //开始
    let goods = this.data.goodsList;
    for (var j = 0; j < goods.length; j++) {
      if (goods[j].stock - goods[j].num < 0) {
        app.showModal(goods[j].name + "库存不足，请修改数量!");
        return false;
      }
    }
    //结束
    let data = [];
    if (that.data.newvalue != 1) {
      wx.showToast({
        title: '请阅读协议后勾选下单',
        icon: 'none',
        duration: 1000
      })
      return false;
    }
    for (let i = 0; i < goods.length; i++) {
      let item = {};
      item.type = goods[i].goodsType;
      item.id = goods[i].goodsid;
      item.unitprice = goods[i].price;
      item.number = goods[i].num;
      data.push(item)
    }
    console.log('data', JSON.stringify(data));
    if (that.data.consigneeid == "") {
      wx.showToast({
        title: '请选择收货地址',
        icon: 'none',
        duration: 1500
      })
      return false;
    }
    console.log('consigneeid', this.data.consigneeid);
    console.log('goodslist', JSON.stringify(data));
    console.log('goodstotalprice', totalPrice);
    console.log('invoicetaxprice', 0);
    console.log('orderprice', totalPrice);
    console.log('packingprice', 0);
    console.log('paymentprice', that.data.calculateTotalMoney);
    console.log('transportprice', 0);
    console.log('usebalance', checkedBalance ? balance : 0);
    // console.log('usecoin', checkedcoin ? maxPrice : 0);
    console.log('usecoin', checkedcoin ? usecoin : 0);    

    const param = {
      consigneeid: this.data.consigneeid,
      goodslist: JSON.stringify(data),
      goodstotalprice: totalPrice,
      invoicetaxprice: 0,
      orderprice: totalPrice,
      packingprice: 0,
      paymentprice: totalPrice,
      transportprice: 0,
      usebalance: checkedBalance ? balance : 0,
      // usecoin: checkedcoin ? maxPrice : 0
      usecoin: checkedcoin ? usecoin : 0      
    }
    app.request({
      url: "shopp/goodsOrderApi/submitGoodsOrder",
      data: param,
      success: function(res) {

        // console.log("res1", res);

        console.log("生成订单", res);

        let {
          ifPayment,
          orderid
        } = res.data;
        if (ifPayment) {
          console.log("res", ifPayment);
          wx.redirectTo({
            url: '../user-shop-goods-order-pay/user-shop-goods-order-pay?res=' + orderid + '&totalMoney=' + calculateTotalMoney + "&payType=1" + '&orderType=1',
          });
        } else {
          console.log("res", ifPayment);
          let {
            actualBalance
          } = res.data;
          wx.redirectTo({
            url: `../user-shop-goods-balance-pay/user-shop-goods-balance-pay?res=${orderid}&balance=${actualBalance}&payType=1`,
          });
        }
        // that.invoice(res.data.orderid); 
      },
      fail: function(res) {
        // app.requestFail(res);
      }
    });
  },
  address() {
    var that = this;
    wx.navigateTo({
      url: '../address-list/address-list',
    })
  },
  // //发票信息
  // invoice(orderid) {
  //   if (this.data.invoice) {
  //     const param = {
  //       orderNumber: orderid,
  //       orderType: this.data.orderType,
  //       headType: this.data.invoice.headType,
  //       headName: this.data.invoice.headName || '',
  //       identityNumber: this.data.invoice.identityNumber || ''
  //     };
  //     app.request({
  //       url: 'shopp/financeApi/insertInvoice',
  //       data: param,
  //       success: function (res) { },
  //       fail: function (res) { }
  //     });
  //   }
  // } 
})