// pages/user-search/user-search.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrl: "https://tyc-bj.cn/yc/api/attach/",
    info: [],
    cityId: app.globalData.cityId,
    pageSize: 5,
    pageNum: 1,
    sort: 1,
    keyword: '请先商品输入名称进行查询',

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var keyword = options.keyword;
    if (keyword == "") {
      this.loadData(keyword);
      this.setData({
        keyword: '请先商品输入名称进行查询',
      });
    } else {
      this.loadData(keyword);
      this.setData({
        'keyword': keyword,
      })
    }
    // console.log('keyword:', this.data.keyword);
    // this.loadData(keyword);
  },

  //  加载数据
  loadData: function (keyword) {
    var that = this;
    var cityid = wx.getStorageSync('cityid');
    var latitude = wx.getStorageSync('latitude'); // 纬度
    var longitude = wx.getStorageSync('longitude'); // 经度 
    console.log('sort:::', cityid);
    
    wx.request({
      url: app.globalData.baseUrl + '/shopp/mallApi/searchGoods',
      data: {
        'keyword': keyword,
        'cityid': cityid,
        'pagesize': that.data.pageSize,
        'pagenum': that.data.pageNum,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('搜索结果:', res)
        that.setData({
          'info': res.data.data,
        })
      }
    })
  },

  // 取消按钮
  cancelSearch: function (e) {
    wx.navigateBack();
  },


  // 搜索框输入
  goSearch: function (e) {
    console.log(e);
    var keyword = e.detail.value;
    this.setData({
      keyword
    });
  },

  // 执行搜索
  indexSearch: function (e) {
    var keyword = this.data.keyword;
    console.log('啊啊啊啊啊', keyword)
    this.loadData(keyword);
  },
//页面跳转
  tzhuan:function(e){
    console.log(e);
    var id=e.currentTarget.dataset.id;
    var etype = e.currentTarget.dataset.etype;
    if (etype==1){
    wx.navigateTo({
      url: "../user-shop-goods-details/user-shop-goods-details?etype=2&id=" + id,
    });
    }else{
      wx.navigateTo({
        url: "../user-shop-goods/user-shop-goods?etype=1&id=" + id,
      });
    }
   
      },
        /**
           * 生命周期函数--监听页面初次渲染完成


  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})