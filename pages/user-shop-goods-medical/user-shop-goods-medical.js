// pages/user-shop-goods/user-shop-goods.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info: [],
    imgUrl: 'https://tyc-bj.cn/yc/api/attach/',
    pagenum: 1, // 页码
    pagesize: 50, //每页个数
    sort: 1, // 1.热门排序 2.好评排序 3.价格排序
    pricesort: 1, // 价格排序情况下:1.升序 2.降序
    typeid: 0, // 产品类别
    // isNav: 1, // 是否有导航分类
    // goodsclass: 0,
    etype: 1,

    keyword: '', // 产品关键字
    typelist: [], // 类型列表数组
    more: 0, // 上拉加载
    ishottype: false, // 是否为点击热门分类进入页面

    //我自己新添加的
    goodsclass: 0,
    alltypeid: "4799b11d4ca749d0a712aba0053c591d", //全部的id
    cityid: 0, // 城市id
    capacitysort: true,
    sorted: 1,
    sortname: "智能排序",
    district: "区域选择",
    currentTabs: 1,
    cityed: true,
    level: 2,
    currentLevel: 1,
    provinceid: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log('options:', options);
    // console.log('type:', this.data.etype);
    // console.log('type:', options.type);
    // if (options.type == 1) {
    //   if (options.typeid) {
    //     this.setData({
    //       'ishottype': true,
    //       'goodsclass': options.typeid,
    //     })
    //   }

    //   wx.setStorageSync('storeType', 1)
    //   this.setData({
    //     'etype': 1,
    //   })
    // } else {
    //   wx.setStorageSync('storeType', 2)
    //   this.setData({
    //     'etype': 2,
    //   })
    // }
    this.oneTypeServiceList();
    this.getProvince();
  },
  loadData: function () {
    var that = this;
    var goodsclass = that.data.goodsclass;
    var cityid = that.data.cityid;
    if (goodsclass == 0) {
      var typeid = that.data.alltypeid;
    } else {
      var typeid = that.data.goodsclass;
    }
    console.log('pagenum:', that.data.pagenum);
    console.log('pagesize:', that.data.pagesize);
    console.log('pricesort:', that.data.pricesort);
    console.log('sort:', that.data.sort);
    console.log('typeid:', typeid);
    console.log('keyword:', that.data.keyword);
    console.log('cityid:', cityid);

    wx.request({
      url: app.globalData.baseUrl + 'shopp/yimeiGoodsApi/getPage',
      data: {
        'pagenum': that.data.pagenum,
        'pagesize': that.data.pagesize,
        'pricesort': that.data.pricesort,
        'sort': that.data.sort,
        'typeid': typeid,
        'cityid': cityid,
        'keyword': that.data.keyword,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('医美/术前术后商品数据:', res.data.data)
        if (that.data.more == 0) {
          that.setData({
            'info': res.data.data,
          })
        } else {
          wx.hideLoading();
          that.setData({
            more: 0,
          })
          if (res.data.data.length) {

            var info = that.data.info.concat(res.data.data);
            that.setData({
              'info': info,
            })
          } else {
            wx.showToast({
              title: '已无更多...',
              icon: 'none',
              duration: 2000,
            });
          }
        }
      }
    })
  },
  // 一级服务产品类别列表
  oneTypeServiceList: function (e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'shopp/goodsTypeApi/getPage',
      data: {
        'type': 2,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },
      method: 'POST',
      success: function (res) {
        console.log('服务啊啊啊:', res)
        if (that.data.more == 0) {
          if (that.data.ishottype) {
            that.setData({
              'typelist': res.data.data,
            });
          } else {
            that.setData({
              'typelist': res.data.data,
            });
          }
        }
        that.loadData(that.data.etype); // 商品列表加载完成 请求数据
      }
    })
  },

  // 切换类别
  bindClass: function (e) {
    console.log("切换类别", e);
    console.log("切换类别", e.currentTarget.dataset.goodsclass);
    var goodsclass = e.currentTarget.dataset.goodsclass;
    console.log("goodsclass1", goodsclass);
    console.log("goodsclass2", this.data.goodsclass);
    if (goodsclass != this.data.goodsclass) {
      this.setData({
        'goodsclass': goodsclass,
        'pagenum': 1,
      });
      this.loadData();
    } else {
      return false;
    }
  },

  // 点击切换排序
  bindSort: function (e) {
    var sorted = e.currentTarget.dataset.sorted;
    this.setData({
      sorted: sorted
    })
    if (sorted == 1) {
      this.setData({
        cityed: !this.data.cityed,
      })
    }
    if (sorted == 2) {
      this.setData({
        capacitysort: false,
        cityed: true
      })
    }
  },
  // 智能排序选择
  clickTab: function (e) {
    var noteType = e.currentTarget.dataset.notetype;
    var sortname = e.currentTarget.dataset.sortname;
    console.log('智能排序选择', noteType);
    console.log('智能排序选择', noteType);
    if (noteType != this.data.sort) {
      if (noteType == 1) {
        this.setData({
          currentTabs: 1,
        })
      } else if (noteType == 2) {
        this.setData({
          currentTabs: 2
        })
      } else {
        this.setData({
          currentTabs: 3
        })
      }
      this.setData({
        sort: noteType,
        capacitysort: true,
        sortname: sortname,
      })
      this.loadData();
    } else {
      this.setData({
        capacitysort: true,
      })
      return false;
    }
  },
  // 获取省
  getProvince: function (e) {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/city/getCityList',
      data: {

      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('省列表:', res)
        that.setData({
          'provincelist': res.data.data,
        })
        if (that.data.provinceid == "") {
          that.setData({
            'provinceid': res.data.data[0].cityId
          })
          that.clickcitys();
        }
      }
    })
  },
  //获取市
  clickcity: function (e) {
    var that = this;
    var provinceid = e.currentTarget.dataset.provinceid;
    var level = that.data.level;
    var currentLevel = that.data.currentLevel;
    that.setData({
      provinceid: provinceid
    })
    wx.request({
      url: app.globalData.baseUrl + 'doctor/city/getCityList',
      data: {
        id: provinceid,
        level: level,
        currentLevel: currentLevel,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('市列表:', res.data.data)
        console.log('cityid:', that.data.cityid)
        that.setData({
          citylist: res.data.data
        })
      }
    })
  },
  clickcitys: function () {
    var that = this;
    var provinceid = that.data.provinceid;
    var level = that.data.level;
    var currentLevel = that.data.currentLevel;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/city/getCityList',
      data: {
        id: provinceid,
        level: level,
        currentLevel: currentLevel,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('市列表:', res.data.data)
        that.setData({
          citylist: res.data.data
        })
      }
    })
  },
  clickcityed: function (e) {
    var that = this;
    var cityid = e.currentTarget.dataset.cityid;
    var cityname = e.currentTarget.dataset.cityname;
    this.setData({
      cityid: cityid,
      district: cityname,
      cityed: true
    })
    this.loadData();
  },
  // 术前术后产品页搜索
  shopGoodsSelect: function (e) {
    console.log(e)
    var keyword = e.detail.value
    this.setData({
      'keyword': keyword
    });
    console.log(this.data.keyword)
    this.loadData(this.data.etype);
  },

  detailInfo: function (e) {
    console.log('点击:', e)
  },

  // 每次输入 重新赋值
  goSearch: function (e) {
    var keyword = e.detail.value
    this.setData({
      'keyword': keyword
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    console.log('下拉刷新');
    this.oneTypeServiceList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    wx.showLoading({
      title: '加载中...',
    })
    this.setData({
      'more': 1,
      'pagenum': this.data.pagenum + 1,
    });
    console.log('页码:', this.data.pagenum)
    this.oneTypeServiceList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})