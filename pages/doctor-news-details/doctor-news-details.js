// pages/doctor-news-details/doctor-news-details.js
var app = getApp();
var WxParse = require('../../wxParse/wxParse.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    newsId:"",//id
    diaryinfo:"",
    info:"",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("用户id:", options.id)
    var newsid = options.id;
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'system/newsapi/newsdetail',
      data: {
        'newsId': newsid,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('新闻详情', res)
        var article = res.data.data.newsContent;
        
        that.setData({
          diaryinfo: res.data.data,
        })
        WxParse.wxParse('article', 'html', article, that, 5);
      }
    })
    
  }
})