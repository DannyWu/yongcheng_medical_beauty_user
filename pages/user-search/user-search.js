// pages/user-search/user-search.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrl: "https://tyc-bj.cn/yc/api/attach/",
    info: [],
    cityId: app.globalData.cityId,
    pageSize: 5,
    pageNum: 1,
    sort: 1,
    keyword: '请先选择分类再输入名称进行查询',

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var keyword = options.keyword;

    if (keyword == "") {
      this.setData({
        keyword: '请先选择分类再输入名称进行查询',
      });
    } else {
      this.setData({
        'keyword': keyword,
      })
    }
    console.log('keyword:', this.data.keyword);
    this.loadData(keyword);
  },

  //  加载数据
  loadData: function(keyword) {
    var that = this;
    var cityid = wx.getStorageSync('cityid');
    var latitude = wx.getStorageSync('latitude'); // 纬度
    var longitude = wx.getStorageSync('longitude'); // 经度 
    console.log('sort:::', that.data.sort);
    console.log('经度纬度', longitude, latitude);
    if (that.data.sort !=3){
    wx.request({
      url: app.globalData.baseUrl + 'doctor/index/searchPage',
      data: {
        'longitude': longitude,
        'latitude': latitude,
        'keyword': keyword,
        'type': that.data.sort,
        'pageSize': that.data.pageSize,
        'pageNum': that.data.pageNum,
      },
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      method: 'POST',
      success(res) {
        console.log('搜索结果:', res)
        that.setData({
          'info': res.data.data,
        })
      }
    })
    }else{
      wx.request({
        url: app.globalData.baseUrl + '/shopp/mallApi/searchGoods',
        data: {
          'keyword': keyword,
          'cityid': cityid,
          'pagesize': that.data.pageSize,
          'pagenum': that.data.pageNum,
        },
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
        },
        method: 'POST',
        success(res) {
          console.log('搜索结果:', res.data.data)
          that.setData({
            'info': res.data.data,
          })
        }
      })
    }
  },

  // 切换分类 1.医生 2.日记 3.商城
  sortClick: function(e) {
    var sort = e.currentTarget.dataset.sort;
    if (sort != this.data.sort) {
      this.setData({
        'sort': sort,
      });
      this.loadData(this.data.keyword);
    } else {
      return false;
    }
  },

  // 取消按钮
  cancelSearch: function(e) {
    wx.navigateBack();
  },


  // 搜索框输入
  goSearch: function(e) {
    console.log(e);
    var keyword = e.detail.value;
    this.setData({
      keyword
    });
  },

  // 执行搜索
  indexSearch: function(e) {
    var keyword = this.data.keyword;
    console.log('输入信息', keyword)
    this.loadData(keyword);
  },
  // 日记关注按钮
  bindFollow: function (e) {
    var that=this;
    this.setData({
      keyword: e.target.dataset.username,
    });
    var isLogin = wx.getStorageSync('isLogin');
    if (isLogin == 2) {
      wx.showModal({
        title: '提示',
        content: '请先登陆!',
        success: function (res) {
          if (res.confirm) {
            wx.redirectTo({
              url: '/pages/user-login/user-login',
            });
          } else { }
        }
      })
    } else {
      var userid = e.currentTarget.dataset.userid;
      var isfollow = e.currentTarget.dataset.isfollow;
      var ziji = wx.getStorageSync('userid');
      console.log('关注:userid', userid);
      console.log('关注:isfollow', isfollow);
      var that = this;
      if (userid == ziji) {
        wx.showToast({
          title: '不能关注自己',
          icon: 'success',
          duration: 1000
        })

      } else if (isfollow == 1) {

        wx.request({
          url: app.globalData.baseUrl + 'doctor/member/attention',
          data: {
            userId: userid,
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('关注结果:', res)
            wx.showToast({
              title: '取消关注',
              icon: 'success',
              duration: 1000
            })
            that.onLoad(that.data);
          }
        })
      } else if (isfollow == 2) {
        wx.request({
          url: app.globalData.baseUrl + 'doctor/member/attention',
          data: {
            userId: userid,
          },
          header: {
            'sign': app.globalData.sign,
            'token': app.globalData.token,
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          method: 'POST',
          success(res) {
            console.log('关注结果:', res)
            wx.showToast({
              title: '关注成功',
              icon: 'success',
              duration: 1000
            })
            that.onLoad(that.data);
          }
        })
      } else { }


    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})