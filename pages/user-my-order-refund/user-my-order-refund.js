// pages/user-my-order-refund/user-my-order-refund.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderNum: "", //订单号
    orderId: "", //申请售后
    content: "", //内容
    currentTab: 0, //默认对号
    color: 1, //默认然色
    name: '', //退换货原因
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log('--a-', options)
    var orderNum = options.orderNum
    var orderType = options.orderType
    var orderId = options.orderId
    this.setData({
      orderNum: orderNum,
      orderId: orderId,
    })

  },

  // 保存信息
  save: function(e) {
    var that = this
    console.log('点击:', e);
    //内容
    this.setData({
      content: e.detail.value
    });
    console.log("内容:", that.data.content)
  },

  //申请售后
  apply: function(e) {
    var that = this;
    var orderType = that.data.orderId;
    var content = that.data.content;
    var name = that.data.name;    
    console.log("--id----", that.data.orderId)
    console.log("---内容---", that.data.content)
    console.log("--退款原因----", that.data.name)
    console.log("---退1换2---", that.data.color)
    if (name == "") {
      wx.showToast({
        title: '请选择退换原因',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    if (content == ""){
      wx.showToast({
        title: '请添写问题描述',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    wx.request({
      url: app.globalData.baseUrl + 'shopp/goodsOrderApi/submitExchangeGoods', //仅为示例，并非真实的接口地址
      data: {
        id: that.data.orderId, //术前术后的
        describe: that.data.content, //问题描述
        problem: that.data.name, //退换原因
        type: that.data.color, //1退货  2换货
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log("申请售后", res)
        wx.switchTab({
          url: '../user-my/user-my',
        })
        wx.showToast({
          title: '申请成功，等待审核',
          icon: 'success',
          duration: 2000,

        })
      }
    })


  },
  //默认然色
  cickTab: function(e) {
    console.log("-然色-", e)
    var that = this;

    if (this.data.color == e.target.dataset.current) {

    } else {
      that.setData({
        color: e.target.dataset.current
      })
    }
  },
  //对号切换
  clickTab: function(e) {
    console.log("-对号-", e)
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false
    } else {
      that.setData({
        currentTab: e.target.dataset.current,
        name: e.target.dataset.name
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})