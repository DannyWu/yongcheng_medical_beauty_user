// pages/user-my-consumption-record/user-my-consumption-record.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageSize: 10,
    pageNo: 1,
    record: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中...',
    })
    this.select();
  },
  //查询消费记录
  select: function () {
    var that = this;
    wx.request({
      url: app.globalData.baseUrl + 'doctor/beautyCoin/getListbeautyCoin',
      data: {
        'type': 1,
        'pageSize': that.data.pageSize,
        'pageNo': that.data.pageNo,
      },
      method: 'POST',
      header: {
        'sign': app.globalData.sign,
        'token': app.globalData.token,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8', // 默认值
      },
      success(res) {
        console.log("消费记录", res)
        var array = that.data.record.concat(res.data.data);
        that.setData({
          record: array
        })
        wx.hideLoading();
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottomss: function () {
    wx.showLoading({
      title: '加载中...',
    })
    this.setData({
      'pageNo': this.data.pageNo + 1,
    });
    this.select();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})