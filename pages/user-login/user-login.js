// pages/user-login/user-login.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    recommendUser: '', // 扫码结果
    codename: '获取验证码'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },
  getPhoneValue: function(e) {
    // console.log("phone", e.detail.value)
    this.setData({
      phone: e.detail.value
    })
  },
  getverifyCode: function(e) {
    // console.log("verifyCode", e.detail.value)
    this.setData({
      verifyCode: e.detail.value
    })
  },
  // 获取验证码
  getCode: function() {
    var phone = this.data.phone;
    var that = this;
    var myreg = /^(14[0-9]|13[0-9]|15[0-9]|17[0-9]|18[0-9]|19[0-8])\d{8}$$/;
    if (this.data.phone == "") {
      wx.showToast({
        title: '手机号不能为空',
        icon: 'none',
        duration: 1000
      })
      var num = 4;
      var timer = setInterval(function() {
        num--;
        if (num <= 0) {
          clearInterval(timer);
          that.setData({
            codename: '重新发送',
            disabled: false
          })
        } else {
          that.setData({
            codename: num + "s"
          })
        }
      }, 1000)
      return false;
    } else if (!myreg.test(this.data.phone)) {
      wx.showToast({
        title: '请输入正确的手机号',
        icon: 'none',
        duration: 1000
      })
      var num = 4;
      var timer = setInterval(function() {
        num--;
        if (num <= 0) {
          clearInterval(timer);
          that.setData({
            codename: '重新发送',
            disabled: false
          })
        } else {
          that.setData({
            codename: num + "s"
          })
        }
      }, 1000)
      return false;
    } else {
      wx.request({
        url: app.globalData.baseUrl + 'system/mobile/sendCode',
        data: {
          'mobile': phone,
          'isDebug': 2
        },
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
        },
        method: 'POST',
        success(res) {
          console.log("send", res.data)
          if (res.code == 200) {
            wx.showToast({
              title: res.data.message,
              icon: 'success',
              duration: 2000
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none',
              duration: 2000
            })
          }
          var num = 61;
          var timer = setInterval(function() {
            num--;
            if (num <= 0) {
              clearInterval(timer);
              that.setData({
                codename: '重新发送',
                disabled: false
              })
            } else {
              that.setData({
                codename: num + "s"
              })
            }
          }, 1000)
        }
      })
    }
  },
  // 获取验证码
  getVerificationCode() {
    this.getCode();
    var that = this
    that.setData({
      disabled: true
    })
  },
  // 扫码登入
  scanLogin() {
    const that = this;
    wx.scanCode({
      onlyFromCamera: true,
      success(res) {
        that.setData({
          recommendUser: res.result
        })

      }
    })
  },
  // 登入
  logined: function(e) {
    console.log("logined", e);
    console.log("loginedtoken", app.globalData.token);
    var that = this;
    var phone = that.data.phone;
    var verifyCode = that.data.verifyCode;
    var roleType = wx.getStorageSync('roleType');
    var userInfo = wx.getStorageSync('userInfo');
    var openid = wx.getStorageSync('openid');
    // var openid = '555555EMwzMt9jz66ku2ntjT3fqI'
    // console.log("phone", phone);
    // console.log("verifyCode", verifyCode);
    // console.log("roleType", 1);
    // console.log("userInfo", userInfo);
    // console.log("打印openid", openid);
    var myreg = /^(14[0-9]|13[0-9]|15[0-9]|17[0-9]|18[0-9]|19[0-8])\d{8}$$/;
    if (this.data.phone == "") {
      wx.showToast({
        title: '手机号不能为空',
        icon: 'none',
        duration: 1000
      })
      return false;
    } else if (!myreg.test(this.data.phone)) {
      wx.showToast({
        title: '请输入正确的手机号',
        icon: 'none',
        duration: 1000
      })
      return false;
    } else {
      // console.log("this。ok")
      wx.request({
        url: app.globalData.baseUrl + 'doctor/member/reg',
        data: {
          'mobile': phone,
          'verifyCode': verifyCode,
          'roleType': 1,
          'openId': openid,
          'avatarUrl': userInfo.avatarUrl,
          'nickName': userInfo.nickName,
          'gender': userInfo.gender,
          'city': userInfo.city,
          'recommendUser': that.data.recommendUser
        },
        header: {
          'sign': app.globalData.sign,
          'token': app.globalData.token,
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
        },
        method: 'POST',
        success(res) {
          console.log('会员登陆', res);
          if (res.data.status_code == 200) {
            wx.showToast({
              title: res.data.data[0].msg,
            })
            wx.setStorageSync("token", res.data.data[0].token);
            wx.setStorageSync("userid", res.data.data[0].userId);
            wx.setStorageSync("isLogin", 1);
            app.globalData.token = res.data.data[0].token;
            wx.setStorageSync("roleType", 1);
            console.log('perfect', res.data.data[0].perfect);
            if (res.data.data[0].perfect == 1) {
              // var photo = wx.getStorageSync("userInfo").avatarUrl;
              var photo = app.globalData.headportrait;
              var name = wx.getStorageSync("userInfo").nickName;
              wx.request({
                url: app.globalData.baseUrl + 'doctor/member/finish',
                data: {
                  'photo': photo,
                  'name': name,
                  'proId': 1,
                  'areaId': 2,
                  'townId': 8,
                },
                header: {
                  'sign': app.globalData.sign,
                  'token': wx.getStorageSync("token"),
                  'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
                },
                method: 'POST',
                success(res) {
                  console.log('会员个人1res', res);
                  // console.log('会员个人2res', res.data.status_code);
                  wx.switchTab({
                    url: "../index/index"
                  })
                },
              })
            } else {
              wx.switchTab({
                url: "../index/index"
              })
            }
            wx.request({
              url: app.globalData.baseUrl + 'doctor/memberTaskApi/addBeauty',
              data: {
                'type': 1,
                'userId': res.data.data[0].userId,
              },
              header: {
                'sign': app.globalData.sign,
                'token': app.globalData.token,
                'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
              },
              method: 'POST',
              success(res) {
                console.log('医生登陆美丽币', res);
              }
            })
          } else {
            var toast = res.data.message;
            wx.showToast({
              title: toast,
              icon: 'none',
            })
          }

        }
      })
    }
  }
})